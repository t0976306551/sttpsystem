<?php

namespace Tests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\RolePermissionSeeder;

trait AuthenticatesUsers
{
    private $username;
    private $password;

    protected function createAdmin()
    {
        $this->username = 'admin';
        $this->password = 'admin';

        $permission = [
            [
                'per_id' => 1,
                'per_name' => 'CREATE_USER'
            ],
        ];
        $role = [
            [
                'r_id' => 1,
                'r_name' => '總管理員'
            ]
        ];

        $rolePermissions = [
            [
                'rp_id' => 1,
                'r_id' => 1,
                'per_id' => 1
            ]
        ];
        $user = [
            'id' => 1,
            'username' => $this->username,
            'password' => Hash::make($this->password),
            'name' => 'admin',
            'r_id' => 1
        ];
        DB::table('permission')->insert($permission);
        DB::table('role')->insert($role);
        DB::table('role_permissions')->insert($rolePermissions);
        DB::table('users')->insert($user);
    }

    protected function createRolePermission()
    {
        $this->seed(PermissionSeeder::class);
        $this->seed(RolePermissionSeeder::class);
    }

    protected function login($username, $password)
    {
        $credentials = [
            'username' => $username,
            'password' => $password, // 假設密碼是 'password'，請根據實際情況更改
        ];
        $response = $this->postJson('/api/login', $credentials);
        $response->assertStatus(200);
        return $response->json('token');
    }
}
