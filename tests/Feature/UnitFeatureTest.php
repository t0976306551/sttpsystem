<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\AuthenticatesUsers;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\RolePermissionSeeder;
use Illuminate\Support\Facades\DB;

class UnitFeatureTest extends TestCase
{
    use RefreshDatabase, AuthenticatesUsers;

    private $username;
    private $password;
    private $token;

    public function setUp(): void
    {
        parent::setUp();

        $this->username = 'admin';
        $this->password = 'admin';
        $this->createAdmin();
        $this->token = $this->login($this->username, $this->password);
        $this->seed(PermissionSeeder::class);
        $this->seed(RolePermissionSeeder::class);
    }

    public function test_create_unit(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'DS',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        DB::table('department')->insert($departmentData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/unit', $unitData);
        $response->assertStatus(200);
        $this->assertEquals('Unit updated or created successfully', $response->json('message'));
    }

    public function test_create_unit_name_repeat(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'DS',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);

        $newUnitData = [
            'u_id' => 'TE',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/unit', $newUnitData);
        $response->assertStatus(409);
        $this->assertEquals('Unit with the same name already exists', $response->json('message'));
    }

    public function test_create_unit_department_id_not_found(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'DS',
            'u_name' => '測試單位',
            'd_id' => 'DA'
        ];
        DB::table('department')->insert($departmentData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/unit', $unitData);
        $response->assertStatus(500);
        $this->assertEquals('Department not found with ID: DA', $response->json('message'));
    }

    public function test_create_unit_rule_error(): void
    {
        $unitData = [
            'u_id' => 10,
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/unit', $unitData);
        $response->assertStatus(422);
        $this->assertEquals(false, $response->json('success'));
    }

    public function test_update_unit(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'DS',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);

        $newUnitData = [
            'u_id' => 'DS',
            'u_name' => '測試單位DS',
            'd_id' => 'DE'
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/unit/DS', $newUnitData);
        $response->assertStatus(200);
        $this->assertEquals('Unit updated or created successfully', $response->json('message'));
    }

    public function test_get_all_unit(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            [
                'u_id' => 'D1',
                'u_name' => '測試單位01',
                'd_id' => 'DE'
            ],
            [
                'u_id' => 'D2',
                'u_name' => '測試單位02',
                'd_id' => 'DE'
            ],
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        $response = $this->get('/api/unit');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        foreach ($responseData as $item) {
            $this->assertArrayHasKey('u_id', $item);
            $this->assertArrayHasKey('u_name', $item);
        }
    }

    public function test_get_unit_by_id(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            [
                'u_id' => 'D1',
                'u_name' => '測試單位01',
                'd_id' => 'DE'
            ],
            [
                'u_id' => 'D2',
                'u_name' => '測試單位02',
                'd_id' => 'DE'
            ],
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        $response = $this->get('/api/unit/D1');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        $this->assertArrayHasKey('u_id', $responseData);
        $this->assertArrayHasKey('u_name', $responseData);
    }

    public function test_delete_unit(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            [
                'u_id' => 'D1',
                'u_name' => '測試單位01',
                'd_id' => 'DE'
            ],
            [
                'u_id' => 'D2',
                'u_name' => '測試單位02',
                'd_id' => 'DE'
            ],
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/unit/D1');
        $response->assertStatus(200);
        $this->assertEquals('Unit delete successfully', $response->json('message'));
    }

    public function test_delete_unit_id_is_not_found(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            [
                'u_id' => 'D1',
                'u_name' => '測試單位01',
                'd_id' => 'DE'
            ],
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/unit/D2');
        $response->assertStatus(500);
        $this->assertEquals('Unit not found with ID: D2', $response->json('message'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
