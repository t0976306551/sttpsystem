<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\AuthenticatesUsers;

class AuthFeatureTest extends TestCase
{
    use RefreshDatabase, AuthenticatesUsers;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_login(): void
    {
        $this->createAdmin();
        $loginData = [
            'username' => 'admin',
            'password' => 'admin'
        ];
        $response = $this->post('/api/login', $loginData);
        $response->assertStatus(200);
        $this->assertEquals("Login successfully", $response->json('message'));
        $this->assertEquals(true, $response->json('success'));
        $this->assertArrayHasKey('token', $response->json());
    }

    public function test_login_username_or_password_error(): void
    {
        $this->createAdmin();
        $loginData = [
            'username' => 'admin2',
            'password' => 'admin2'
        ];
        $response = $this->post('/api/login', $loginData);
        $response->assertStatus(401);
        $this->assertEquals("invalid credentials", $response->json('message'));
        $this->assertEquals(false, $response->json('success'));
    }

    public function test_login_rule_error(): void
    {
        $this->createAdmin();
        $loginData = [
            'username' => 123456,
            'password' => 'admin2'
        ];
        $response = $this->withoutMiddleware()->post('/api/login', $loginData);
        $response->assertStatus(422);
        $this->assertEquals(false, $response->json('success'));
    }

    public function test_logout(): void
    {
        $this->createAdmin();
        $token = $this->login('admin', 'admin');
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('/api/auth/logout');
        $response->assertStatus(200);
        $this->assertEquals("Logout successfully", $response->json('message'));
    }

    public function test_logout_error(): void
    {
        $response = $this->post('/api/auth/logout');
        $response->assertStatus(401);
        $this->assertEquals("Authorization Token not found", $response->json('status'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
