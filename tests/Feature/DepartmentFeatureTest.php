<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\AuthenticatesUsers;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\RolePermissionSeeder;
use Illuminate\Support\Facades\DB;

class DepartmentFeatureTest extends TestCase
{
    use RefreshDatabase, AuthenticatesUsers;

    private $username;
    private $password;
    private $token;

    public function setUp(): void
    {
        parent::setUp();

        $this->username = 'admin';
        $this->password = 'admin';
        $this->createAdmin();
        $this->token = $this->login($this->username, $this->password);
        $this->seed(PermissionSeeder::class);
        $this->seed(RolePermissionSeeder::class);
    }

    public function test_create_department(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/department', $departmentData);
        $response->assertStatus(200);
        $this->assertEquals('Department updated or created successfully', $response->json('message'));
    }

    public function test_create_department_name_repeat(): void
    {
        $originDepartment = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        DB::table('department')->insert($originDepartment);

        $departmentData = [
            'd_id' => 'DA',
            'd_name' => '測試部門'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/department', $departmentData);
        $response->assertStatus(409);
        $this->assertEquals('Department with the same name already exists', $response->json('message'));
    }

    public function test_create_department_rule_error(): void
    {
        $departmentData = [
            'd_id' => 10,
            'd_name' => '測試部門'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/department', $departmentData);
        $response->assertStatus(422);
        $this->assertEquals(false, $response->json('success'));
    }

    public function test_update_department(): void
    {
        $originDepartment = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        DB::table('department')->insert($originDepartment);
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門2'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/department/DE', $departmentData);
        $response->assertStatus(200);
        $this->assertEquals('Department updated or created successfully', $response->json('message'));
    }

    public function test_get_department_all(): void
    {
        $departmentData = [
            [
                'd_id' => 'DE',
                'd_name' => '測試部門'
            ],
            [
                'd_id' => 'DA',
                'd_name' => '測試部門2'
            ]
        ];
        DB::table('department')->insert($departmentData);
        $response = $this->get('/api/department');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        foreach ($responseData as $item) {
            $this->assertArrayHasKey('d_id', $item);
            $this->assertArrayHasKey('d_name', $item);
        }
    }

    public function test_get_department_by_id(): void
    {
        $departmentData = [
            [
                'd_id' => 'DE',
                'd_name' => '測試部門'
            ],
            [
                'd_id' => 'DA',
                'd_name' => '測試部門2'
            ]
        ];
        DB::table('department')->insert($departmentData);
        $response = $this->get('/api/department/DE');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        $this->assertArrayHasKey('d_id', $responseData);
        $this->assertArrayHasKey('d_name', $responseData);
    }

    public function test_delete_department(): void
    {
        $departmentData = [
            [
                'd_id' => 'DE',
                'd_name' => '測試部門'
            ],
            [
                'd_id' => 'DA',
                'd_name' => '測試部門2'
            ]
        ];
        DB::table('department')->insert($departmentData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/department/DE');
        $response->assertStatus(200);
        $this->assertEquals('Department delete successfully', $response->json('message'));
    }

    public function test_delete_department_id_not_found(): void
    {
        $departmentData = [
            [
                'd_id' => 'DE',
                'd_name' => '測試部門'
            ],
            [
                'd_id' => 'DA',
                'd_name' => '測試部門2'
            ]
        ];
        DB::table('department')->insert($departmentData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/department/DD');
        $response->assertStatus(500);
        $this->assertEquals("Department not found with ID: DD", $response->json('message'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
