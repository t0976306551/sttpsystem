<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Tests\AuthenticatesUsers;

class UserFeatureTest extends TestCase
{
    use RefreshDatabase, AuthenticatesUsers;

    private $permission;
    private $role;
    private $rolePermissions;
    private $user;
    private $token;
    private $username;
    private $password;

    public function setUp(): void
    {
        parent::setUp();
        $this->username = 'admin';
        $this->password = 'admin';
        $this->user = [
            'id' => 2,
            'username' => 'admin2',
            'password' => Hash::make('admin2'),
            'name' => 'admin2',
            'r_id' => 1
        ];
        $this->createAdmin();
        $this->token = $this->login($this->username, $this->password);
    }

    public function test_create_user(): void
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/user', $this->user);
        $response->assertStatus(200);
    }

    public function test_create_user_name_repeat(): void
    {
        DB::table('users')->insert($this->user);
        $this->user = [
            'id' => 3,
            'username' => 'admin2',
            'password' => Hash::make('admin2'),
            'name' => 'admin2',
            'r_id' => 1
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/user', $this->user);
        $response->assertStatus(409);
        $this->assertEquals('User with the same username already exists', $response->json('message'));
    }

    public function test_create_user_id_not_found(): void
    {
        DB::table('users')->insert($this->user);
        $this->user = [
            'username' => 'admin3',
            'password' => Hash::make('admin3'),
            'name' => 'admin3',
            'r_id' => 1
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/user/3', $this->user);
        $response->assertStatus(500);
        $this->assertEquals("User not found with ID: 3", $response->json('message'));
    }

    public function test_get_user_all(): void
    {
        DB::table('users')->insert($this->user);
        $response = $this->get('/api/user');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        foreach ($responseData as $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('username', $item);
            $this->assertArrayHasKey('name', $item);
            $this->assertArrayHasKey('r_id', $item);
        }
    }

    public function test_get_user_by_id(): void
    {
        $response = $this->get('/api/user/1');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        $this->assertArrayHasKey('id', $responseData);
        $this->assertArrayHasKey('username', $responseData);
        $this->assertArrayHasKey('name', $responseData);
        $this->assertArrayHasKey('r_id', $responseData);
    }

    public function test_update_user(): void
    {
        DB::table('users')->insert($this->user);
        $updateUserData = [
            'username' => 'admin2',
            'password' => 'admin2',
            'name' => 'adminNew',
            'r_id' => 1
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/user/2', $updateUserData);
        $response->assertStatus(200);
        $this->assertEquals("User updated or created successfully", $response->json('message'));
    }

    public function test_update_password(): void
    {
        DB::table('users')->insert($this->user);
        $updatePasswordData = [
            'newPassword' => 'testtest',
            'oldPassword' => 'admin2'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/user/updatePassword/2', $updatePasswordData);
        $response->assertStatus(200);
        $this->assertEquals("User password update successfully", $response->json('message'));
    }

    public function test_update_password_old_password_error(): void
    {
        DB::table('users')->insert($this->user);
        $updatePasswordData = [
            'newPassword' => 'testtest',
            'oldPassword' => 'admin3'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/user/updatePassword/2', $updatePasswordData);
        $response->assertStatus(422);
        $this->assertEquals("User Incorrect Password", $response->json('message'));
    }

    public function test_update_password_new_password_error(): void
    {
        DB::table('users')->insert($this->user);
        $updatePasswordData = [
            'newPassword' => '',
            'oldPassword' => 'admin2'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/user/updatePassword/2', $updatePasswordData);
        $response->assertStatus(302);
    }

    public function test_delete_user(): void
    {
        DB::table('users')->insert($this->user);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/user/2');
        $response->assertStatus(200);
        $this->assertEquals('User delete successfully', $response->json('message'));
    }

    public function test_delete_user_id_not_found(): void
    {
        DB::table('users')->insert($this->user);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/user/100');
        $response->assertStatus(500);
        $this->assertEquals('User not found with ID: 100', $response->json('message'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
