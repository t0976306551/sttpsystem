<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;

class PositionFeatureTest extends TestCase
{
    use RefreshDatabase, AuthenticatesUsers;

    private $username;
    private $password;
    private $token;

    public function setUp(): void
    {
        parent::setUp();

        $this->username = 'admin';
        $this->password = 'admin';
        $this->createAdmin();
        $this->createRolePermission();
        $this->token = $this->login($this->username, $this->password);
    }

    public function test_create_position(): void
    {
        $positionData = [
            'p_id' => 'AM',
            'p_name' => '測試職位'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/position', $positionData);
        $response->assertStatus(200);
        $this->assertEquals('Position updated or created successfully', $response->json('message'));
    }

    public function test_create_position_name_repeat(): void
    {
        $positionData = [
            'p_id' => 'AM',
            'p_name' => '測試職位'
        ];
        DB::table('position')->insert($positionData);
        $newPositionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/position', $newPositionData);
        $response->assertStatus(409);
        $this->assertEquals('Position with the same name already exists', $response->json('message'));
    }

    public function test_create_position_rule_error(): void
    {
        $positionData = [
            'p_id' => 10,
            'p_name' => '測試職位'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/position', $positionData);
        $response->assertStatus(422);
        $this->assertEquals(false, $response->json('success'));
    }

    public function test_update_position(): void
    {
        $positionData = [
            'p_id' => 'AM',
            'p_name' => '測試職位'
        ];
        DB::table('position')->insert($positionData);
        $newPositionData = [
            'p_id' => 'AM',
            'p_name' => '測試職位001'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/position/AM', $newPositionData);
        $response->assertStatus(200);
        $this->assertEquals('Position updated or created successfully', $response->json('message'));
    }

    public function test_get_all_position(): void
    {
        $positionData = [
            [
                'p_id' => 'AM',
                'p_name' => '測試職位01'
            ],
            [
                'p_id' => 'FE',
                'p_name' => '測試職位02'
            ]
        ];
        DB::table('position')->insert($positionData);
        $response = $this->get('/api/position');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        foreach ($responseData as $item) {
            $this->assertArrayHasKey('p_id', $item);
            $this->assertArrayHasKey('p_name', $item);
        }
    }

    public function test_get_position_by_id(): void
    {
        $positionData = [
            'p_id' => 'AM',
            'p_name' => '測試職位'
        ];
        DB::table('position')->insert($positionData);
        $response = $this->get('/api/position/AM');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        $this->assertArrayHasKey('p_id', $responseData);
        $this->assertArrayHasKey('p_name', $responseData);
    }

    public function test_delete_position(): void
    {
        $positionData = [
            'p_id' => 'AM',
            'p_name' => '測試職位'
        ];
        DB::table('position')->insert($positionData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/position/AM');
        $response->assertStatus(200);
        $this->assertEquals('Position delete successfully', $response->json('message'));
    }

    public function test_delete_position_id_is_not_found(): void
    {
        $positionData = [
            'p_id' => 'AM',
            'p_name' => '測試職位'
        ];
        DB::table('position')->insert($positionData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/position/D2');
        $response->assertStatus(500);
        $this->assertEquals('Position not found with ID: D2', $response->json('message'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
