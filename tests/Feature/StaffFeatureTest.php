<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;

class StaffFeatureTest extends TestCase
{
    use RefreshDatabase, AuthenticatesUsers;
    private $username;
    private $password;
    private $token;

    public function setUp(): void
    {
        parent::setUp();

        $this->username = 'admin';
        $this->password = 'admin';
        $this->createAdmin();
        $this->createRolePermission();
        $this->token = $this->login($this->username, $this->password);
    }

    public function test_create_staff(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        $supervisorLevel = [
            'sl_id' => 'E1',
            'sl_name' => '基層員工'
        ];
        $sttpData = [
            'sttp_id' => 'DESDFE',
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE',
        ];

        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        DB::table('sttp')->insert($sttpData);
        DB::table('supervisor_level')->insert($supervisorLevel);

        $staffData = [
            's_id' => 'Y2023-1001',
            's_name' => '王陽明',
            's_gender' => '男',
            's_phone' => '0912345678',
            'sl_id' => 'E1',
            'sttp_id' => 'DESDFE'
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/staff', $staffData);
        $response->assertStatus(200);
        $this->assertEquals('Staff updated or created successfully', $response->json('message'));
    }
}
