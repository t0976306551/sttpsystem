<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;

class SttpFeatureTest extends TestCase
{
    use RefreshDatabase, AuthenticatesUsers;
    private $username;
    private $password;
    private $token;

    public function setUp(): void
    {
        parent::setUp();

        $this->username = 'admin';
        $this->password = 'admin';
        $this->createAdmin();
        $this->createRolePermission();
        $this->token = $this->login($this->username, $this->password);
    }

    public function test_create_sttp(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        $sttpData = [
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/sttp', $sttpData);
        $response->assertStatus(200);
        $this->assertEquals('Sttp updated or created successfully', $response->json('message'));
    }

    public function test_create_sttp_name_repeat(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        $sttpData = [
            'sttp_id' => 'DESDFE',
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE',
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        DB::table('sttp')->insert($sttpData);
        $newSttpData = [
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/sttp', $newSttpData);
        $response->assertStatus(409);
        $this->assertEquals('Sttp with the same name already exists', $response->json('message'));
    }

    public function test_create_sttp_rule_error(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        $sttpData = [
            'd_id' => 11,
            'u_id' => 'SD',
            'p_id' => 'FE'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->post('/api/sttp', $sttpData);
        $response->assertStatus(422);
        $this->assertEquals(false, $response->json('success'));
    }

    public function test_set_sttp_id_is_not_found(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        $sttpData = [
            'sttp_id' => 'DESDFE',
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE',
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        DB::table('sttp')->insert($sttpData);
        $newSttpData = [
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE'
        ];
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->put('/api/sttp/DESDFA', $newSttpData);
        $response->assertStatus(500);
        $this->assertEquals('Sttp not found with ID: DESDFA', $response->json('message'));
    }

    public function test_get_all_sttp(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        $sttpData = [
            'sttp_id' => 'DESDFE',
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE',
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        DB::table('sttp')->insert($sttpData);
        $response = $this->get('/api/sttp');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        foreach ($responseData as $item) {
            $this->assertArrayHasKey('sttp_id', $item);
            $this->assertArrayHasKey('d_id', $item);
            $this->assertArrayHasKey('u_id', $item);
            $this->assertArrayHasKey('p_id', $item);
        }
    }

    public function test_get_sttp_by_id(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        $sttpData = [
            'sttp_id' => 'DESDFE',
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE',
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        DB::table('sttp')->insert($sttpData);
        $response = $this->get('/api/sttp/DESDFE');
        $response->assertStatus(200);
        $responseData = $response->json('data');
        $this->assertArrayHasKey('sttp_id', $responseData);
        $this->assertArrayHasKey('d_id', $responseData);
        $this->assertArrayHasKey('u_id', $responseData);
        $this->assertArrayHasKey('p_id', $responseData);
    }

    public function test_delete_sttp(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        $sttpData = [
            'sttp_id' => 'DESDFE',
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE',
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        DB::table('sttp')->insert($sttpData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/sttp/DESDFE');
        $response->assertStatus(200);
        $this->assertEquals('Sttp delete successfully', $response->json('message'));
    }

    public function test_delete_sttp_id_is_not_found(): void
    {
        $departmentData = [
            'd_id' => 'DE',
            'd_name' => '測試部門'
        ];
        $unitData = [
            'u_id' => 'SD',
            'u_name' => '測試單位',
            'd_id' => 'DE'
        ];
        $positionData = [
            'p_id' => 'FE',
            'p_name' => '測試職位'
        ];
        $sttpData = [
            'sttp_id' => 'DESDFE',
            'd_id' => 'DE',
            'u_id' => 'SD',
            'p_id' => 'FE',
        ];
        DB::table('department')->insert($departmentData);
        DB::table('unit')->insert($unitData);
        DB::table('position')->insert($positionData);
        DB::table('sttp')->insert($sttpData);
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->token,
        ])->delete('/api/sttp/DESDFA');
        $response->assertStatus(500);
        $this->assertEquals('Sttp not found with ID: DESDFA', $response->json('message'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
