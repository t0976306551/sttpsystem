<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sttp_learn_and_experience', function (Blueprint $table) {
            $table->increments('sle_id', 25)->comment('Sttp學經歷id');
            $table->string('sttp_id', 10)->comment('Stpp id');
            $table->string('education', 10)->comment('學歷');
            $table->string('experience', 10)->comment('經歷');
            $table->timestamps();
            $table->foreign('sttp_id')->references('sttp_id')->on('sttp')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sttp_learn_and_experience');
    }
};
