<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('training_unit', function (Blueprint $table) {
            $table->increments('tu_id')->comment('訓練單位id');
            $table->string('tu_name', 50)->comment('訓練單位名稱');
            $table->string('tu_location', 50)->comment('訓練單位地點');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('training_unit');
    }
};
