<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->string('s_id', 25)->primary()->comment('員工編號(唯一)');
            $table->string('s_name', 10)->nullable()->comment('員工姓名');
            $table->string('s_gender', 10)->nullable()->comment('員工性別');
            $table->string('s_phone', 20)->nullable()->comment('員工電話');
            $table->string('sl_id', 10)->nullable()->comment('主管等級');
            $table->string('sttp_id', 10)->nullable()->comment('stpp編號');
            $table->timestamps();

            $table->foreign('sl_id')->references('sl_id')->on('supervisor_level')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sttp_id')->references('sttp_id')->on('sttp')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('staff');
    }
};
