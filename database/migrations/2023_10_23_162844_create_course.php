<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('course', function (Blueprint $table) {
            $table->increments('c_id')->comment('課程id');
            $table->string('c_name', 50)->comment('課程名稱');
            $table->unsignedInteger('ct_id')->comment('課程分類id');
            $table->integer('hours')->comment('時數');
            $table->string('t_name', 50)->comment('講師姓名');
            $table->string('t_unit', 50)->comment('講師單位');
            $table->string('t_position', 50)->comment('講師職位');
            $table->timestamps();

            $table->foreign('ct_id')->references('ct_id')->on('course_type')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('course');
    }
};
