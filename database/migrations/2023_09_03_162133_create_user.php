<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->comment('使用者id');
            $table->string('username', 20)->comment('使用者帳號');
            $table->string('password')->comment('使用者密碼');
            $table->string('name', 20)->comment('使用者名稱');
            $table->unsignedInteger('r_id')->comment('角色id');
            $table->timestamps();

            $table->foreign('r_id')->references('r_id')->on('role')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
