<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('training_and_certificate_results', function (Blueprint $table) {
            $table->increments('tac_id')->comment('訓練and證照成績id');
            $table->unsignedInteger('c_id')->comment('班別對照表id');
            $table->string('s_id')->comment('職員對照表id');
            $table->integer('academic_score')->comment('術科成績');
            $table->integer('practical_score')->comment('學科成績');
            $table->timestamps();


            $table->foreign('c_id')->references('c_id')->on('classes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('s_id')->references('s_id')->on('staff')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('training_and_certificate_results');
    }
};
