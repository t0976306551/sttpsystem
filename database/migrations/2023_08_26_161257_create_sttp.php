<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sttp', function (Blueprint $table) {
            $table->string('sttp_id', 25)->primary()->comment('sttp_id');
            $table->string('d_id', 5)->comment('部門id');
            $table->string('u_id', 5)->nullable()->comment('單位id');
            $table->string('p_id', 5)->nullable()->comment('職位id');
            $table->timestamps();

            $table->foreign('d_id')->references('d_id')->on('department')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('u_id')->references('u_id')->on('unit')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('p_id')->references('p_id')->on('position')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sttp');
    }
};
