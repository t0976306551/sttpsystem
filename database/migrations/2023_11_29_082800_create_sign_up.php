<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sign_up', function (Blueprint $table) {
            $table->increments('su_id')->comment('報名資料表id');
            $table->unsignedInteger('c_id')->nullable()->comment('班別對照表id');
            $table->string('s_id')->nullable()->comment('職員對照表id');
            $table->timestamps();

            $table->foreign('c_id')->references('c_id')->on('classes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('s_id')->references('s_id')->on('staff')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sign_up');
    }
};
