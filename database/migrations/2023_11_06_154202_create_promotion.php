<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('promotion', function (Blueprint $table) {
            $table->increments('pr_id')->comment('升遷id');
            $table->string('sttp_id', 30)->comment('sttp_id');
            $table->string('pr_sttp', 30)->comment('可升遷id之sttp');
            $table->timestamps();

            $table->foreign('sttp_id')->references('sttp_id')->on('sttp')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pr_sttp')->references('sttp_id')->on('sttp')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('promotion');
    }
};
