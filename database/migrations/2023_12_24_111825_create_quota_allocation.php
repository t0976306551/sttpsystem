<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quota_allocation', function (Blueprint $table) {
            $table->increments('qa_id')->comment('名額分配id');
            $table->string('u_id')->comment('單位id');
            $table->unsignedInteger('cs_id')->comment('班次id');
            $table->integer('quota')->nullable()->comment('名額');
            $table->timestamps();

            $table->foreign('u_id')->references('u_id')->on('unit')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cs_id')->references('cs_id')->on('class_schedule')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quota_allocation');
    }
};
