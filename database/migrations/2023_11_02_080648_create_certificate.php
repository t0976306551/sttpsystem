<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('certificate', function (Blueprint $table) {
            $table->increments('cer_id')->comment('證照id');
            $table->string('cer_name', 50)->comment('證照名稱');
            $table->integer('effective_years')->comment('證照有效年限');
            $table->string('cer_authority', 50)->comment('發照機關');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('certificate');
    }
};
