<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('unit', function (Blueprint $table) {
            $table->string('u_id', 5)->primary()->comment('單位id');
            $table->string('u_name', 20)->comment('單位名稱');
            $table->string('d_id', 5)->nullable()->comment('部門外來鍵');
            $table->timestamps();

            $table->foreign('d_id')->references('d_id')->on('department')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('unit');
    }
};
