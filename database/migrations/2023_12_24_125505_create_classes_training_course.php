<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('classes_training_course', function (Blueprint $table) {
            $table->increments('ctc')->comment('班別訓練課程對照表id');
            $table->unsignedInteger('c_id')->comment('班別id');
            $table->unsignedInteger('tc_id')->comment('訓練課程id');
            $table->timestamps();

            $table->foreign('c_id')->references('c_id')->on('classes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tc_id')->references('tc_id')->on('training_courses')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('classes_training_course');
    }
};
