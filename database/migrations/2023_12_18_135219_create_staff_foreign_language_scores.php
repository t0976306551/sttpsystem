<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('staff_foreign_language_scores', function (Blueprint $table) {
            $table->increments('sfls_id')->comment('員工外語成績id');
            $table->string('s_id')->comment('職員對照表id');
            $table->unsignedInteger('fl_id')->comment('外語對照表id');
            $table->date('test_date')->comment('測驗日期');
            $table->integer('pass_score')->nullable()->comment('及格成績');
            $table->integer('listening_score')->nullable()->comment('聽力成績');
            $table->integer('grammar_score')->nullable()->comment('語法成績');
            $table->integer('vocabulary_score')->nullable()->comment('字彙成績');
            $table->integer('session_score')->nullable()->comment('會話成績');
            $table->integer('toefl_score')->nullable()->comment('托福成績');
            $table->integer('essay_score')->nullable()->comment('作文成績');
            $table->integer('total_score')->comment('總成績');
            $table->boolean('is_passed')->comment('是否通過');
            $table->timestamps();

            $table->foreign('s_id')->references('s_id')->on('staff')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fl_id')->references('fl_id')->on('foreign_language')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('staff_foreign_language_scores');
    }
};
