<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('class_schedule', function (Blueprint $table) {
            $table->increments('cs_id')->comment('班次對照表id');
            $table->string('training_year', 50)->comment('訓練年度');
            $table->integer('term')->comment('期別');
            $table->string('ch_name', 50)->comment('班主任姓名');
            $table->string('contacts_name', 50)->comment('聯絡人姓名');
            $table->string('contacts_phone', 50)->comment('聯絡人電話');
            $table->date('start_date')->comment('開訓日期');
            $table->date('end_date')->comment('結訓日期');
            $table->integer('plan_trainees_number')->comment('擬受訓人數');
            $table->date('deadline_date')->comment('報名期限');
            $table->integer('budget')->comment('預算費用');
            $table->string('remarks', 255)->comment('備註');
            $table->unsignedInteger('o_id')->comment('主辦單位id');
            $table->unsignedInteger('tu_id')->comment('訓練單位id');
            $table->timestamps();

            $table->foreign('o_id')->references('o_id')->on('organizer')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tu_id')->references('tu_id')->on('training_unit')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('class_schedule');
    }
};
