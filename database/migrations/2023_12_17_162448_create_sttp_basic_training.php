<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sttp_basic_training', function (Blueprint $table) {
            $table->increments('sbt_id')->comment('Sttp基本訓練id');
            $table->string('sttp_id')->comment('sttp對照表id');
            $table->unsignedInteger('c_id')->comment('班別對照表id');
            $table->string('elective_type', 10)->comment('必選修');
            $table->string('credit_conditions', 255)->comment('抵免條件');
            $table->string('classes_type', 10)->comment('類別');
            $table->timestamps();

            $table->foreign('sttp_id')->references('sttp_id')->on('sttp')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('c_id')->references('c_id')->on('classes')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sttp_basic_training');
    }
};
