<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('flat_tone', function (Blueprint $table) {
            $table->increments('ft_id')->comment('平調id');
            $table->string('sttp_id', 30)->comment('sttp_id');
            $table->string('ft_sttp', 30)->comment('可平調之sttp');
            $table->timestamps();

            $table->foreign('sttp_id')->references('sttp_id')->on('sttp')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('ft_sttp')->references('sttp_id')->on('sttp')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('flat_tone');
    }
};
