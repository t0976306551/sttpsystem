<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('course_class_schedule', function (Blueprint $table) {
            $table->increments('ccs_id')->comment('課程and班次id');
            $table->unsignedInteger('c_id')->comment('課程id');
            $table->unsignedInteger('cs_id')->comment('班次對照表id');

            $table->timestamps();

            $table->foreign('c_id')->references('c_id')->on('course')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cs_id')->references('cs_id')->on('class_schedule')->onDelete('cascade')->onUpdate('cascade');



        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('course_class_schedule');
    }
};
