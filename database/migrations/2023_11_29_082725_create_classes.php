<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('c_id')->comment('班別id');
            $table->unsignedInteger('cs_id')->nullable()->comment('班次對照表id');
            $table->unsignedInteger('cer_id')->nullable()->comment('證照id');
            $table->string('c_name')->nullable()->comment('班別名稱');
            $table->integer('training_day')->nullable()->comment('訓練日數');
            $table->integer('training_hours')->nullable()->comment('訓練時數');
            $table->integer('class_number')->nullable()->comment('每班人數');
            $table->integer('academic_standards')->nullable()->comment('學科及格標準');
            $table->integer('practical_standards')->nullable()->comment('術科及格標準');
            $table->integer('traing_active_years')->nullable()->comment('訓練有效年限');
            $table->string('remarks')->nullable()->comment('備註');
            $table->timestamps();

            $table->foreign('cs_id')->references('cs_id')->on('class_schedule')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cer_id')->references('cer_id')->on('certificate')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('classes');
    }
};
