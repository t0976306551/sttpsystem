<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //訓練課程對照表
        Schema::create('training_courses', function (Blueprint $table) {
            $table->increments('tc_id')->comment('訓練課程id');
            $table->string('tc_name', 50)->comment('訓練課程名稱');
            $table->integer('hours')->comment('時數');
            $table->string('t_name', 50)->comment('講師姓名');
            $table->string('t_unit', 50)->comment('講師單位');
            $table->string('t_position', 50)->comment('講師職位');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('training_courses');
    }
};
