<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('role_permissions', function (Blueprint $table) {
            $table->increments('rp_id')->comment('角色權限id');
            $table->unsignedInteger('r_id')->comment('角色id');
            $table->unsignedInteger('per_id')->comment('權限id');

            $table->timestamps();

            $table->foreign('r_id')->references('r_id')->on('role')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('per_id')->references('per_id')->on('permission')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('role_permissions');
    }
};
