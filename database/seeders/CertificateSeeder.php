<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CertificateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'cer_name' => 'TQC+ 程式語言Python 3',
                'effective_years' => 0,
                'cer_authority' => '財團法人中華民國電腦技能基金會',
            ],
            [
                'cer_name' => 'TQC+  程式語言C',
                'effective_years' => 0,
                'cer_authority' => '財團法人中華民國電腦技能基金會',
            ],
            [
                'cer_name' => 'TQC+ 網頁程式設計HTML5',
                'effective_years' => 0,
                'cer_authority' => '財團法人中華民國電腦技能基金會',
            ],
            [
                'cer_name' => 'TQC+ 基礎行動裝置應用程式設計Android 9',
                'effective_years' => 0,
                'cer_authority' => '財團法人中華民國電腦技能基金會',
            ],
        ];

        foreach ($datas as $data) {
            DB::table('certificate')->insert([
                'cer_name' => $data['cer_name'],
                'effective_years' => $data['effective_years'],
                'cer_authority' => $data['cer_authority'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
