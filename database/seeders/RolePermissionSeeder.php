<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Role;
use App\Models\Permissions;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        // $roles = Role::all();
        $permissions = Permissions::all();
        foreach ($permissions as $permission) {
            DB::table('role_permissions')->insert([
                'r_id' => 1,
                'per_id' => $permission->per_id,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
