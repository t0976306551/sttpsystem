<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'sttp_id' => 'DESDFE',
                'd_id' => 'DE',
                'u_id' => 'SD',
                'p_id' => 'FE'
            ],
            [
                'sttp_id' => 'SLDSSR',
                'd_id' => 'SL',
                'u_id' => 'DS',
                'p_id' => 'SR'
            ],
            [
                'sttp_id' => 'MGHRRS',
                'd_id' => 'MG',
                'u_id' => 'HR',
                'p_id' => 'RS'
            ],
            [
                'sttp_id' => 'MFMPAM',
                'd_id' => 'MF',
                'u_id' => 'MP',
                'p_id' => 'AM'
            ],
        ];

        foreach ($datas as $data) {
            DB::table('sttp')->insert([
                'sttp_id' => $data['sttp_id'],
                'd_id' => $data['d_id'],
                'u_id' => $data['u_id'],
                'p_id' => $data['p_id'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
