<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SttpLearnAndExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'sttp_id' => 'DESDFE',
                'education' => '學士',
                'experience' => '3年'
            ],
            [
                'sttp_id' => 'MFMPAM',
                'education' => '學士',
                'experience' => '2年'
            ],
            [
                'sttp_id' => 'MGHRRS',
                'education' => '學士',
                'experience' => '1年'
            ],
            [
                'sttp_id' => 'SLDSSR',
                'education' => '碩士',
                'experience' => '5年'
            ]
        ];

        foreach ($datas as $data) {
            DB::table('sttp_learn_and_experience')->insert([
                'sttp_id' => $data['sttp_id'],
                'education' => $data['education'],
                'experience' => $data['experience'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
