<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            '總管理員',
            '一般管理員',
            '使用者'
        ];

        foreach ($datas as $data) {
            DB::table('role')->insert([
                'r_name' => $data,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
