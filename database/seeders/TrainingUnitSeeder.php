<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrainingUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'tu_name' => '程式設計培訓中心',
                'tu_location' => '台北市信義區忠孝東路五段1號'
            ],
            [
                'tu_name' => '數據分析學院',
                'tu_location' => '新竹市東區光復路二段101號'
            ],
        ];

        foreach ($datas as $data) {
            DB::table('training_unit')->insert([
                'tu_name' => $data['tu_name'],
                'tu_location' => $data['tu_location'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
