<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                's_id' => 'Y2023-1001',
                's_name' => '王小明',
                's_gender' => '男',
                's_phone' => '0912345678',
                'sl_id' => 'E1',
                'sttp_id' => 'DESDFE',
            ],
            [
                's_id' => 'Y2023-1002',
                's_name' => '王怡芳',
                's_gender' => '女',
                's_phone' => '0987654321',
                'sl_id' => 'E2',
                'sttp_id' => 'MFMPAM',
            ]
        ];

        foreach ($datas as $data) {
            DB::table('staff')->insert([
                's_id' => $data['s_id'],
                's_name' => $data['s_name'],
                's_gender' => $data['s_gender'],
                's_phone' => $data['s_phone'],
                'sl_id' => $data['sl_id'],
                'sttp_id' => $data['sttp_id'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
