<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseClassScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'c_id' => 1,
                'cs_id' => 1,
            ],
            [
                'c_id' => 2,
                'cs_id' => 1,
            ],
        ];

        foreach ($datas as $data) {
            DB::table('course_class_schedule')->insert([
                'c_id' => $data['c_id'],
                'cs_id' => $data['cs_id'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
