<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'c_name' => 'TQC+ 程式語言Python',
                'ct_id' => '1',
                'hours' => 20,
                't_name' => '王大牛',
                't_unit' => '資訊科技部',
                't_position' => 'Python資深工程師',
            ],
            [
                'c_name' => 'TQC+ 程式語言C',
                'ct_id' => '1',
                'hours' => 20,
                't_name' => '陳曉明',
                't_unit' => '資訊科技部',
                't_position' => '專案管理組長',
            ],
        ];

        foreach ($datas as $data) {
            DB::table('course')->insert([
                'c_name' => $data['c_name'],
                'ct_id' => $data['ct_id'],
                'hours' => $data['hours'],
                't_name' => $data['t_name'],
                't_unit' => $data['t_unit'],
                't_position' => $data['t_position'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
