<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrganizerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'o_name' => '軟體工程師協會'
            ],
            [
                'o_name' => '科技產業聯盟'
            ],
        ];

        foreach ($datas as $data) {
            DB::table('organizer')->insert([
                'o_name' => $data['o_name'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
