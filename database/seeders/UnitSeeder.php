<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'u_id' => 'HR',
                'u_name' => '人力資源',
                'd_id' => 'MG'
            ],
            [
                'u_id' => 'SD',
                'u_name' => '軟體開發',
                'd_id' => 'DE'
            ],
            [
                'u_id' => 'DS',
                'u_name' => '國內銷售',
                'd_id' => 'SL'
            ],
            [
                'u_id' => 'MP',
                'u_name' => '製造單位',
                'd_id' => 'MF'
            ],
            [
                'u_id' => 'NA',
                'u_name' => '無隸屬任何部門',
                'd_id' => null,
            ]
        ];

        foreach ($datas as $data) {
            DB::table('unit')->insert([
                'u_id' => $data['u_id'],
                'u_name' => $data['u_name'],
                'd_id' => $data['d_id'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
