<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupervisorLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'sl_id' => 'M2',
                'sl_name' => '高階主管'
            ],
            [
                'sl_id' => 'M1',
                'sl_name' => '主管'
            ],
            [
                'sl_id' => 'E3',
                'sl_name' => '中級員工'
            ],
            [
                'sl_id' => 'E2',
                'sl_name' => '初級員工'
            ],
            [
                'sl_id' => 'E1',
                'sl_name' => '基層員工'
            ]
        ];
        foreach ($datas as $data) {
            DB::table('supervisor_level')->insert([
                'sl_id' => $data['sl_id'],
                'sl_name' => $data['sl_name'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
