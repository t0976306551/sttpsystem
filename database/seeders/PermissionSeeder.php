<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            'CREATE_USER',
            'UPDATE_USER',
            'DELETE_USER',
            'CREATE_ROLE',
            'UPDATE_ROLE',
            'DELETE_ROLE',
            'CREATE_ROLE_PERMISSION',
            'UPDATE_ROLE_PERMISSION',
            'DELETE_ROLE_PERMISSION',
            'CREATE_COURSE_SHARE',
            'UPDATE_COURSE_SHARE',
            'DELETE_COURSE_SHARE',
            'CREATE_DEPARTMENT',
            'UPDATE_DEPARTMENT',
            'DELETE_DEPARTMENT',
            'CREATE_POSITION',
            'UPDATE_POSITION',
            'DELETE_POSITION',
            'CREATE_UNIT',
            'UPDATE_UNIT',
            'DELETE_UNIT',
            'CREATE_STTP',
            'UPDATE_STTP',
            'DELETE_STTP',
            'CREATE_STPPLEARN_EXPERIENCE',
            'UPDATE_STPPLEARN_EXPERIENCE',
            'DELETE_STPPLEARN_EXPERIENCE'
        ];

        foreach ($datas as $data) {
            DB::table('permission')->insert([
                'per_name' => $data,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
