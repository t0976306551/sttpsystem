<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'type' => '程式語言',

            ],
            [
                'type' => '軟體工程',
            ],
        ];

        foreach ($datas as $data) {
            DB::table('course_type')->insert([
                'type' => $data['type'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
