<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'd_id' => 'MG',
                'd_name' => '管理部'
            ],
            [
                'd_id' => 'DE',
                'd_name' => '研發部'
            ],
            [
                'd_id' => 'SL',
                'd_name' => '銷售部'
            ],
            [
                'd_id' => 'MF',
                'd_name' => '製造部'
            ],
        ];

        foreach ($datas as $data) {
            DB::table('department')->insert([
                'd_id' => $data['d_id'],
                'd_name' => $data['d_name'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
