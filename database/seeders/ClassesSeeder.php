<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'cs_id' => 1,
                'cer_id' => 1,
                'c_name' => 'Python-1',
                'training_day' => 5,
                'training_hours' => 20,
                'class_number' => 10,
                'academic_standards' => 70,
                'practical_standards' => 70,
                'traing_active_years' => 0,
                'remarks' => '無',
            ],
            [
                'cs_id' => 1,
                'cer_id' => 1,
                'c_name' => 'Python-2',
                'training_day' => 5,
                'training_hours' => 20,
                'class_number' => 10,
                'academic_standards' => 70,
                'practical_standards' => 70,
                'traing_active_years' => 0,
                'remarks' => '無',
            ],
            [
                'cs_id' => 2,
                'cer_id' => 2,
                'c_name' => 'C-1',
                'training_day' => 5,
                'training_hours' => 20,
                'class_number' => 10,
                'academic_standards' => 80,
                'practical_standards' => 80,
                'traing_active_years' => 0,
                'remarks' => '無',
            ],
            [
                'cs_id' => 2,
                'cer_id' => 2,
                'c_name' => 'C-2',
                'training_day' => 5,
                'training_hours' => 20,
                'class_number' => 10,
                'academic_standards' => 80,
                'practical_standards' => 80,
                'traing_active_years' => 0,
                'remarks' => '無',
            ],
        ];

        foreach ($datas as $data) {
            DB::table('classes')->insert([
                'cs_id' => $data['cs_id'],
                'cer_id' => $data['cer_id'],
                'c_name' => $data['c_name'],
                'training_day' => $data['training_day'],
                'training_hours' => $data['training_hours'],
                'class_number' => $data['class_number'],
                'academic_standards' => $data['academic_standards'],
                'practical_standards' => $data['practical_standards'],
                'traing_active_years' => $data['traing_active_years'],
                'remarks' => $data['remarks'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
