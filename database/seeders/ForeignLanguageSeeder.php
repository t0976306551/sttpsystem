<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ForeignLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'fl_name' => '全民英檢-初',
            ],
            [
                'fl_name' => '全民英檢-中',
            ],
            [
                'fl_name' => '全民英檢-中高',
            ],
            [
                'fl_name' => '全民英檢-高',
            ],
            [
                'fl_name' => '全民英檢-優',
            ],
            [
                'fl_name' => '多益TOEIC',
            ],
            [
                'fl_name' => '托福TOEFL-Primary',
            ],
            [
                'fl_name' => '托福TOEFL-Junior',
            ],
            [
                'fl_name' => '托福TOEFL-ITP',
            ],
            [
                'fl_name' => '托福TOEFL-iBT',
            ]
        ];

        foreach ($datas as $data) {
            DB::table('foreign_language')->insert([
                'fl_name' => $data['fl_name'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
