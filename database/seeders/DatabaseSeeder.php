<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(RolePermissionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(UnitSeeder::class);
        $this->call(PositionSeeder::class);
        $this->call(StppSeeder::class);
        $this->call(SupervisorLevelSeeder::class);
        $this->call(SttpLearnAndExperienceSeeder::class);
        $this->call(StaffSeeder::class);
        $this->call(CourseTypeSeeder::class);
        $this->call(CourseSeeder::class);
        $this->call(OrganizerSeeder::class);
        $this->call(TrainingUnitSeeder::class);
        $this->call(ClassScheduleSeeder::class);
        $this->call(CourseClassScheduleSeeder::class);
        $this->call(ForeignLanguageSeeder::class);
        $this->call(CertificateSeeder::class);
        $this->call(ClassesSeeder::class);
    }
}
