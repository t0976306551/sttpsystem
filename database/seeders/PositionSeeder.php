<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'p_id' => 'RS',
                'p_name' => '人才招募專員',
            ],
            [
                'p_id' => 'FE',
                'p_name' => '前端工程師',
            ],
            [
                'p_id' => 'SR',
                'p_name' => '銷售代表',
            ],
            [
                'p_id' => 'AM',
                'p_name' => '裝配工',
            ],
        ];

        foreach ($datas as $data) {
            DB::table('position')->insert([
                'p_id' => $data['p_id'],
                'p_name' => $data['p_name'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
