<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $datas = [
            [
                'training_year' => '112',
                'term' => 1,
                'ch_name' => '張家輝',
                'contacts_name' => '謝承辦',
                'contacts_phone' => '0911111111',
                'start_date' => '2023-12-01',
                'end_date' => '2023-12-31',
                'plan_trainees_number' => 20,
                'deadline_date' => '2023-11-10',
                'budget' => 100000,
                'remarks' => '無備註',
                'o_id' => 1,
                'tu_id' => 1,
            ],
            [
                'training_year' => '112',
                'term' => 1,
                'ch_name' => '陳輝煌',
                'contacts_name' => '林承辦',
                'contacts_phone' => '0922222222',
                'start_date' => '2023-11-01',
                'end_date' => '2023-11-30',
                'plan_trainees_number' => 20,
                'deadline_date' => '2023-10-10',
                'budget' => 200000,
                'remarks' => '無備註',
                'o_id' => 2,
                'tu_id' => 2,
            ],
        ];

        foreach ($datas as $data) {
            DB::table('class_schedule')->insert([
                'training_year' => $data['training_year'],
                'term' => $data['term'],
                'ch_name' => $data['ch_name'],
                'contacts_name' => $data['contacts_name'],
                'contacts_phone' => $data['contacts_phone'],
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date'],
                'plan_trainees_number' => $data['plan_trainees_number'],
                'deadline_date' => $data['deadline_date'],
                'budget' => $data['budget'],
                'remarks' => $data['remarks'],
                'o_id' => $data['o_id'],
                'tu_id' => $data['tu_id'],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
