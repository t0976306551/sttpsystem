@servers(['web' => 'localhost'])

@task('deploy', ['on' => 'web'])
git pull origin main
composer install --no-interaction --prefer-dist
@endtask
