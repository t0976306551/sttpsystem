<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    use HasFactory;
    //證照對照表
    protected $table = 'certificate';
    protected $primaryKey = 'cer_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'cer_name',
        'effective_years',
        'cer_authority'
    ];

    public function classes()
    {
        return $this->hasMany(Classes::class, 'cer_id', 'cer_id');
    }
}
