<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassesTrainingCourse extends Model
{
    use HasFactory;
    //班別訓練課程對照表
    protected $table = 'classes_training_course';
    protected $primaryKey = 'ctc_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'c_id',
        'tc_id'
    ];

    public function classes()
    {
        return $this->belongsTo(Classes::class, 'c_id', 'c_id');
    }

    public function trainingCourse()
    {
        return $this->belongsTo(TrainingCourses::class, 'tc_id', 'tc_id');
    }
}
