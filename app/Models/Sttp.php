<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sttp extends Model
{
    use HasFactory;

    //STTP對照表
    protected $table = 'sttp';
    protected $primaryKey = 'sttp_id';
    public $timestamps = true;
    public $incrementing = false;
    protected $fillable = [
        'sttp_id',
        'd_id',
        'u_id',
        'p_id',
    ];

    public function department()
    {
        return $this->belongsTo(Department::class, 'd_id', 'd_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'u_id', 'u_id');
    }

    public function position()
    {
        return $this->belongsTo(Position::class, 'p_id', 'p_id');
    }

    public function staff()
    {
        return $this->hasMany(Staff::class, 'sttp_id', 'sttp_id');
    }

    public function flatToneBySttpId()
    {
        return $this->hasMany(FlatTone::class, 'sttp_id', 'sttp_id');
    }

    public function sttpByFtSttp()
    {
        return $this->hasMany(FlatTone::class, 'sttp_id', 'ft_sttp');
    }

    public function promotion()
    {
        return $this->hasMany(Promotion::class, 'sttp_id', 'sttp_id');
    }

    public function sttpByPromotion()
    {
        return $this->hasMany(Promotion::class, 'sttp_id', 'pr_sttp');
    }

    public function sttpLearnAndExperience()
    {
        return $this->hasMany(SttpLearnAndExperience::class, 'sttp_id', 'sttp_id');
    }

    public function sttpBasicTraining()
    {
        return $this->hasMany(SttpBasicTraining::class, 'sttp_id', 'sttp_id');
    }
}
