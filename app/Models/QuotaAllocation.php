<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotaAllocation extends Model
{
    use HasFactory;

    //名額分配
    protected $table = 'quota_allocation';
    protected $primaryKey = 'qa_id';
    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'u_id',
        'cs_id',
        'quota'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'u_id', 'u_id');
    }

    public function classSchedule()
    {
        return $this->belongsTo(ClassSchedule::class, 'cs_id', 'cs_id');
    }
}
