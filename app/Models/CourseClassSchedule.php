<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseClassSchedule extends Model
{
    use HasFactory;
    //開班課程對照表
    protected $table = 'course_class_schedule';
    protected $primaryKey = 'ccs_id';
    public $timestamps = true;
    protected $fillable = [
        'c_id',
        'cs_id',
    ];
    public function course()
    {
        return $this->belongsTo(Course::class, 'c_id', 'c_id');
    }

    public function classSchedule()
    {
        return $this->belongsTo(ClassSchedule::class, 'cs_id', 'cs_id');
    }
}
