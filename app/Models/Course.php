<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    //課程對照表
    protected $table = 'course';
    protected $primaryKey = 'c_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'c_name',
        'ct_id',
        'hours',
        't_name',
        't_unit',
        't_position'
    ];


    public function courseClassSchedule()
    {
        return $this->hasMany(CourseClassSchedule::class, 'c_id', 'c_id');
    }

    public function courseType()
    {
        return $this->belongsTo(CourseType::class, 'ct_id', 'ct_id');
    }
}
