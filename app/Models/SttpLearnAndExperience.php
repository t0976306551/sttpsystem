<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SttpLearnAndExperience extends Model
{
    use HasFactory;

    //STTP學經歷基本資料
    protected $table = 'sttp_learn_and_experience';
    protected $primaryKey = 'sle_id';
    public $timestamps = true;
    protected $fillable = [
        'sttp_id',
        'education',
        'experience'
    ];

    public function sttp()
    {
        return $this->belongsTo(Sttp::class, 'sttp_id', 'sttp_id');
    }
}
