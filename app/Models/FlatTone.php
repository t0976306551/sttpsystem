<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlatTone extends Model
{
    use HasFactory;

    //平調對照表
    protected $table = 'flat_tone';
    protected $primaryKey = 'ft_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'sttp_id',
        'ft_sttp'
    ];

    public function sttp()
    {
        return $this->belongsTo(Sttp::class, 'sttp_id', 'sttp_id');
    }

    public function sttpByflatTone()
    {
        return $this->belongsTo(Sttp::class, 'ft_sttp', 'sttp_id');
    }
}
