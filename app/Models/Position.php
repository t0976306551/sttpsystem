<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;

    //職位對照表
    protected $table = 'position';
    protected $primaryKey = 'p_id';
    public $timestamps = true;
    public $incrementing = false;
    protected $fillable = [
        'p_id',
        'p_name',
    ];

    public function sttp()
    {
        return $this->hasMany(Sttp::class, 'p_id', 'p_id');
    }
}
