<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffForeignLanguageScores extends Model
{
    use HasFactory;
    //員工外語成績對照表
    protected $table = 'staff_foreign_language_scores';
    protected $primaryKey = 'sfls_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        's_id',
        'fl_id',
        'test_date',
        'pass_score',
        'listening_score',
        'grammar_score',
        'vocabulary_score',
        'session_score',
        'toefl_score',
        'essay_score',
        'total_score',
        'is_passed'
    ];

    public function staff()
    {
        return $this->belongsTo(Staff::class, 's_id', 's_id');
    }

    public function foreignLanguage()
    {
        return $this->belongsTo(ForeignLanguage::class, 'fl_id', 'fl_id');
    }
}
