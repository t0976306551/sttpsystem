<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingCourses extends Model
{
    use HasFactory;
    //訓練課程對照表
    protected $table = 'training_courses';
    protected $primaryKey = 'tc_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'tc_name',
        'hours',
        't_name',
        't_unit',
        't_position'
    ];


    public function classesTrainingCourse()
    {
        return $this->hasMany(ClassesTrainingCourse::class, 'tc_id', 'tc_id');
    }
}
