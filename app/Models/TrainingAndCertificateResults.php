<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingAndCertificateResults extends Model
{
    use HasFactory;
    protected $table = 'training_and_certificate_results';
    protected $primaryKey = 'tac_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'c_id',
        's_id',
        'academic_score',
        'practical_score'
    ];

    public function classes()
    {
        return $this->belongsTo(Classes::class, 'c_id', 'c_id');
    }

    public function staff()
    {
        return $this->belongsTo(Position::class, 's_id', 's_id');
    }
}
