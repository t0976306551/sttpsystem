<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePermissions extends Model
{
    use HasFactory;

    protected $table = 'role_permissions';
    protected $primaryKey = 'rp_id';
    public $timestamps = true;
    protected $fillable = [
        'r_id',
        'per_id',
    ];
    public function role()
    {
        return $this->belongsTo(Role::class, 'r_id', 'r_id');
    }

    public function permission()
    {
        return $this->belongsTo(Permissions::class, 'per_id', 'per_id');
    }
}
