<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'role';
    protected $primaryKey = 'r_id';
    public $timestamps = true;
    protected $fillable = [
        'r_name'
    ];
    public function user()
    {
        return $this->hasMany(User::class, 'r_id', 'r_id');
    }

    public function rolePermission()
    {
        return $this->hasMany(RolePermissions::class, 'r_id', 'r_id');
    }
}
