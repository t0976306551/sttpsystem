<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForeignLanguage extends Model
{
    use HasFactory;

    //外語對照表
    protected $table = 'foreign_language';
    protected $primaryKey = 'fl_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'fl_name',
    ];

    public function staffForeignLanguageScores()
    {
        return $this->hasMany(StaffForeignLanguageScores::class, 'fl_id', 'fl_id');
    }
}
