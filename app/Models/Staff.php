<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;

    //職員對照表
    protected $table = 'staff';
    protected $primaryKey = 's_id';
    public $timestamps = true;
    public $incrementing = false;
    protected $fillable = [
        's_id',
        's_name',
        's_phone',
        's_gender',
        'sl_id',
        'sttp_id'
    ];

    public function sttp()
    {
        return $this->belongsTo(Sttp::class, 'sttp_id', 'sttp_id');
    }

    public function supervisorLevel()
    {
        return $this->belongsTo(SupervisorLevel::class, 'sl_id', 'sl_id');
    }

    public function signUp()
    {
        return $this->hasMany(SignUp::class, 's_id', 's_id');
    }

    public function trainingAndCertificateResults()
    {
        return $this->hasMany(TrainingAndCertificateResults::class, 's_id', 's_id');
    }

    public function staffForeignLanguageScores()
    {
        return $this->hasMany(StaffForeignLanguageScores::class, 's_id', 's_id');
    }
}
