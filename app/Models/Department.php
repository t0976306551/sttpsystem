<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    //部門對照表
    protected $table = 'department';
    protected $primaryKey = 'd_id';
    public $timestamps = true;
    public $incrementing = false;
    protected $fillable = [
        'd_id',
        'd_name'
    ];
    public function sttp()
    {
        return $this->hasMany(Sttp::class, 'd_id', 'd_id');
    }

    public function unit()
    {
        return $this->hasMany(Unit::class, 'd_id', 'd_id');
    }
}
