<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    use HasFactory;

    protected $table = 'permission';
    protected $primaryKey = 'per_id';
    public $timestamps = true;
    protected $fillable = [
        'per_name',
    ];

    public function rolePermission()
    {
        return $this->hasMany(RolePermissions::class, 'per_id', 'per_id');
    }
}
