<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;
    //班別對照表
    protected $table = 'classes';
    protected $primaryKey = 'c_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'cs_id',
        'cer_id',
        'c_name',
        'training_day',
        'training_hours',
        'class_number',
        'academic_standards',
        'practical_standards',
        'traing_active_years',
        'remarks'
    ];

    public function classSchedule()
    {
        return $this->belongsTo(ClassSchedule::class, 'cs_id', 'cd_id');
    }

    public function certificate()
    {
        return $this->belongsTo(Certificate::class, 'cer_id', 'cer_id');
    }

    public function trainingAndCertificateResults()
    {
        return $this->hasMany(TrainingAndCertificateResults::class, 'c_id', 'c_id');
    }

    public function sttpBasicTraining()
    {
        return $this->hasMany(SttpBasicTraining::class, 'c_id', 'c_id');
    }

    public function classesTrainingCourse()
    {
        return $this->hasMany(ClassesTrainingCourse::class, 'c_id', 'c_id');
    }
}
