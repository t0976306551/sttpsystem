<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    //單位對照表
    protected $table = 'unit';
    protected $primaryKey = 'u_id';
    public $timestamps = true;
    public $incrementing = false;
    protected $fillable = [
        'u_id',
        'u_name',
        'd_id'
    ];

    public function sttp()
    {
        return $this->hasMany(Sttp::class, 'u_id', 'u_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'd_id', 'd_id');
    }

    public function quotaAllocation()
    {
        return $this->hasMany(QuotaAllocation::class, 'u_id', 'u_id');
    }
}
