<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SttpBasicTraining extends Model
{
    use HasFactory;
    //Sttp 基本訓練表
    protected $table = 'sttp_basic_training';
    protected $primaryKey = 'sbt_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'sttp_id',
        'c_id',
        'elective_type',
        'credit_conditions',
        'classes_type'
    ];

    public function sttp()
    {
        return $this->belongsTo(Sttp::class, 'sttp_id', 'sttp_id');
    }

    public function classes()
    {
        return $this->belongsTo(Classes::class, 'c_id', 'c_id');
    }
}
