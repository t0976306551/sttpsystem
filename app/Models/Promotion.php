<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    use HasFactory;

    //升遷對照表
    protected $table = 'promotion';
    protected $primaryKey = 'pr_id';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'sttp_id',
        'pr_sttp'
    ];

    public function sttp()
    {
        return $this->belongsTo(Sttp::class, 'sttp_id', 'sttp_id');
    }

    public function sttpByPromotion()
    {
        return $this->belongsTo(Sttp::class, 'pr_sttp', 'sttp_id');
    }
}
