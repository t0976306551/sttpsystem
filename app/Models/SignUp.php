<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SignUp extends Model
{
    use HasFactory;

    //報名資料表
    protected $table = 'sign_up';
    protected $primaryKey = 'su_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'c_id',
        's_id',
    ];

    public function staff()
    {
        return $this->belongsTo(Staff::class, 's_id', 's_id');
    }

    public function classes()
    {
        return $this->belongsTo(Classes::class, 'c_id', 'c_id');
    }
}
