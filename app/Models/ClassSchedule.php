<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassSchedule extends Model
{
    use HasFactory;
    //班次對照表
    protected $table = 'class_schedule';
    protected $primaryKey = 'cs_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'training_year',
        'term',
        'ch_name',
        'contacts_name',
        'contacts_phone',
        'start_date',
        'end_date',
        'plan_trainees_number',
        'deadline_date',
        'budget',
        'remarks',
        'o_id',
        'tu_id'
    ];

    public function organizer()
    {
        return $this->belongsTo(Organizer::class, 'o_id', 'o_id');
    }

    public function trainingUnit()
    {
        return $this->belongsTo(TrainingUnit::class, 'tu_id', 'tu_id');
    }

    public function courseClassSchedule()
    {
        return $this->hasMany(CourseClassSchedule::class, 'cs_id', 'cs_id');
    }

    public function signUp()
    {
        return $this->hasMany(SignUp::class, 'cs_id', 'cs_id');
    }

    public function quotaAllocation()
    {
        return $this->hasMany(QuotaAllocation::class, 'cs_id', 'cs_id');
    }
}
