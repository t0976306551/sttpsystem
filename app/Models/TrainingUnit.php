<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingUnit extends Model
{
    use HasFactory;
    //訓練單位對照表
    protected $table = 'training_unit';
    protected $primaryKey = 'tu_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'tu_name',
        'tu_location'
    ];

    public function classSchedule()
    {
        return $this->hasMany(classSchedule::class, 'tu_id', 'tu_id');
    }
}
