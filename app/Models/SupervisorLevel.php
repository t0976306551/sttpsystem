<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupervisorLevel extends Model
{
    use HasFactory;
    protected $table = 'supervisor_level';
    protected $primaryKey = 'sl_id';
    public $timestamps = true;
    public $incrementing = false;
    protected $fillable = [
        'sl_id',
        'sl_name'
    ];

    public function staff()
    {
        return $this->hasMany(Staff::class, 'sl_id', 'sl_id');
    }
}
