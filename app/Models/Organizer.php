<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    use HasFactory;
    //主辦單位對照表
    protected $table = 'organizer';
    protected $primaryKey = 'o_id';
    public $timestamps = true;
    public $incrementing = true;
    protected $fillable = [
        'o_name'
    ];

    public function classSchedule()
    {
        return $this->hasMany(classSchedule::class, 'o_id', 'o_id');
    }
}
