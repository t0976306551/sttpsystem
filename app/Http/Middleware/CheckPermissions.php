<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Role;
use App\Models\Permissions;
use App\Models\RolePermissions;
use Illuminate\Support\Facades\Auth;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, $permission): Response
    {
        $user = Auth::user();


        $getRole = Role::find($user->r_id);
        if(!$getRole){
            return response()->json(['status' => 'Role is found'], 401);
        }

        $getRoleAllPermission = RolePermissions::where('r_id',$user->r_id)->get();
        $checkPermission = false;
        foreach($getRoleAllPermission as $role_permission){
            $permissionData = Permissions::find($role_permission->per_id);
            if($permissionData->per_name == $permission){
                $checkPermission = true;
                break;
            }
        }

        if($checkPermission){
            return $next($request);
        }

        return response()->json(['status' => '該權限不能操作此功能'], 401);


    }
}
