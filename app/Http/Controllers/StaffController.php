<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\StaffService;
use Illuminate\Validation\ValidationException;

class StaffController extends Controller
{
    protected $staffService;


    /**
     * @OA\Schema(
     *     schema="Staff",
     *     @OA\Property(property="s_id", type="string"),
     *     @OA\Property(property="s_name", type="string"),
     *     @OA\Property(property="s_gender", type="string"),
     *     @OA\Property(property="s_phone", type="string"),
     *     @OA\Property(property="sl_id", type="string"),
     *     @OA\Property(property="sttp_id", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(StaffService $staffService)
    {
        $this->staffService = $staffService;
    }

    /**
     * @OA\Get(
     *     path="/api/staff/{id}",
     *     summary="根據ID取得職員資訊",
     *     description="根據提供的ID取得職員資訊",
     *     tags={"Staffs"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="職員ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得職員資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Staff"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋職員發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/staff",
     *     summary="返回所有職員資訊",
     *     description="返回所有職員的資訊",
     *     tags={"Staffs"},
     *     @OA\Parameter(
     *         name="sl_id",
     *         in="query",
     *         description="supervisorLevel ID",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="sttp_id",
     *         in="query",
     *         description="Sttp ID",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有職員資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/Staff")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋職員發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $sl_id = $request->query('sl_id') ?? null;
        $sttp_id = $request->query('sttp_id') ?? null;
        $data = [
            'sl_id' => $sl_id,
            'sttp_id' => $sttp_id
        ];
        $result = $this->staffService->get($data, $id);
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Post(
     *     path="/api/staff",
     *     summary="創建職員",
     *     description="創建職員",
     *     tags={"Staffs"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建職員",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"s_id", "s_name", "s_gender", "s_phone", "sl_id", "sttp_id"},
     *                 @OA\Property(property="s_id", type="string", example="s_id"),
     *                 @OA\Property(property="s_name", type="string", example="王小明"),
     *                 @OA\Property(property="s_gender", type="string", example="男"),
     *                 @OA\Property(property="s_phone", type="string", example="0900000000"),
     *                 @OA\Property(property="sl_id", type="string", example="sl_id"),
     *                 @OA\Property(property="sttp_id", type="string", example="sttp_id"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Staff updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Staff"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該職員已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Staff with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/staff/{id}",
     *     summary="修改職員",
     *     description="修改職員",
     *     tags={"Staffs"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="職員的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改職員資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"s_id", "s_name", "s_gender", "s_phone", "sl_id", "sttp_id"},
     *                 @OA\Property(property="s_id", type="string", example="s_id"),
     *                 @OA\Property(property="s_name", type="string", example="王小明"),
     *                 @OA\Property(property="s_gender", type="string", example="男"),
     *                 @OA\Property(property="s_phone", type="string", example="0900000000"),
     *                 @OA\Property(property="sl_id", type="string", example="sl_id"),
     *                 @OA\Property(property="sttp_id", type="string", example="sttp_id"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Staff updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Staff"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該職員已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Staff with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Staff not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function create(Request $request)
    {
        try {
            $validatedData = $request->validate([
                's_id' => 'required|string|max:25',
                's_name' => 'required|string|max:10',
                's_phone' => 'required|string|max:20',
                's_gender' => 'required|string|max:10',
                'sl_id' => 'required|string|max:10',
                'sttp_id' => 'required|string|max:10',
            ]);
            $result = $this->staffService->create($validatedData);
            return response()->json($result, $result['status']);
        } catch (ValidationException $e) {
            return response()->json(['success' => false, 'errors' => $e->validator->errors()], 422);
        }
    }


    public function update(Request $request)
    {
        try {
            $id = $request->route('id') ?? null;
            $validatedData = $request->validate([
                's_id' => 'required|string|max:25',
                's_name' => 'required|string|max:10',
                's_phone' => 'required|string|max:20',
                's_gender' => 'required|string|max:10',
                'sl_id' => 'required|string|max:10',
                'sttp_id' => 'required|string|max:10',
            ]);
            $result = $this->staffService->update($validatedData, $id);
            return response()->json($result, $result['status']);
        } catch (ValidationException $e) {
            return response()->json(['success' => false, 'errors' => $e->validator->errors()], 422);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/staff/{id}",
     *     summary="根據ID刪除職員",
     *     description="根據提供的ID刪除職員",
     *     tags={"Staffs"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="職員的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Staff delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Staff ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->staffService->delete($id);
        return response()->json($result, $result['status']);
    }
}
