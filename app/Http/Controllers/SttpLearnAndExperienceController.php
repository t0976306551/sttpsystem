<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SttpLearnAndExperienceService;

class SttpLearnAndExperienceController extends Controller
{
    protected $sttpLearnAndExperienceService;
    /**
     * @OA\Schema(
     *     schema="SttpLearnAndExperience",
     *     @OA\Property(property="sle_id", type="integer"),
     *     @OA\Property(property="sttp_id", type="string"),
     *     @OA\Property(property="education", type="string"),
     *     @OA\Property(property="experience", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */
    public function __construct(SttpLearnAndExperienceService $sttpLearnAndExperienceService)
    {
        $this->sttpLearnAndExperienceService = $sttpLearnAndExperienceService;
    }

    /**
     * @OA\Get(
     *     path="/api/sle/{id}",
     *     summary="根據ID取得Sttp學經歷資訊",
     *     description="根據提供的ID取得Sttp學經歷資訊",
     *     tags={"SttpLearnAndExperiences"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp學經歷ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得Sttp學經歷資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/SttpLearnAndExperience"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋Sttp學經歷發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/sle",
     *     summary="返回所有Sttp學經歷資訊",
     *     description="返回所有Sttp學經歷的資訊",
     *     tags={"SttpLearnAndExperiences"},
     *     @OA\Parameter(
     *         name="sttp_id",
     *         in="query",
     *         description="Sttp ID",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有Sttp學經歷資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/SttpLearnAndExperience")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋Sttp學經歷發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $sttp = $request->query('sttp_id') ?? null;
        $data = [
            'sttp_id' => $sttp
        ];
        $result = $this->sttpLearnAndExperienceService->get($id, $data);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/sle",
     *     summary="創建Sttp學經歷",
     *     description="創建Sttp學經歷",
     *     tags={"SttpLearnAndExperiences"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建Sttp學經歷",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"d_id", "u_id", "cs_id", "p_id"},
     *                 @OA\Property(property="sttp_id", type="string", example="sttp_id (Sttp代號)"),
     *                 @OA\Property(property="education", type="string", example="education (學歷)"),
     *                 @OA\Property(property="experience", type="string", example="experience (經歷)"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Sttp learn and experience updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/SttpLearnAndExperience"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該Sttp已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp learn and experience with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     *  * @OA\PUT(
     *     path="/api/sle/{id}",
     *     summary="修改Sttp學經歷",
     *     description="修改Sttp學經歷",
     *     tags={"SttpLearnAndExperiences"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp學經歷 ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改Sttp學經歷資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"sttp_id", "education", "experience"},
     *                 @OA\Property(property="sttp_id", type="string", example="sttp_id (Sttp代號)"),
     *                 @OA\Property(property="education", type="string", example="education (學歷)"),
     *                 @OA\Property(property="experience", type="string", example="experience (經歷)"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Sttp learn and experience updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/SttpLearnAndExperience"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該Sttp學經歷已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp learn and experience with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp learn and experience not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */


    public function set(Request $request)
    {
        $id = $request->route('id');
        $validatedData = $request->validate([
            'sttp_id' => 'required|string',
            'education' => 'required|string|max:10',
            'experience' => 'required|string|max:10',
        ]);
        $result = $this->sttpLearnAndExperienceService->set($validatedData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/sle/{id}",
     *     summary="根據ID刪除Sttp學經歷",
     *     description="根據提供的ID刪除Sttp學經歷",
     *     tags={"SttpLearnAndExperiences"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp學經歷 ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Sttp learn and experience delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp learn and experience ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->sttpLearnAndExperienceService->delete($id);
        return response()->json($result);
    }
}
