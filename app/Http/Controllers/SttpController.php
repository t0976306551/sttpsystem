<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SttpService;
use Illuminate\Validation\ValidationException;

class SttpController extends Controller
{
    protected $sttpService;
    /**
     * @OA\Schema(
     *     schema="Sttp",
     *     @OA\Property(property="sttp_id", type="string"),
     *     @OA\Property(property="d_id", type="string"),
     *     @OA\Property(property="u_id", type="string"),
     *     @OA\Property(property="p_id", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     *     @OA\Property(property="fullTitle", type="string"),
     *     @OA\Property(property="department", ref="#/components/schemas/Department"),
     *     @OA\Property(property="unit", ref="#/components/schemas/Unit"),
     *     @OA\Property(property="position", ref="#/components/schemas/Position"),
     * ),
     * @OA\Schema(
     *     schema="SetSttp",
     *     @OA\Property(property="sttp_id", type="string"),
     *     @OA\Property(property="d_id", type="string"),
     *     @OA\Property(property="u_id", type="string"),
     *     @OA\Property(property="p_id", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     *
     */

    public function __construct(SttpService $sttpService)
    {
        $this->sttpService = $sttpService;
    }

    /**
     * @OA\Get(
     *     path="/api/sttp/{id}",
     *     summary="根據ID取得Sttp資訊",
     *     description="根據提供的ID取得Sttp資訊",
     *     tags={"Sttps"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得Sttp資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Sttp"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋Sttp發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/sttp",
     *     summary="返回所有Sttp資訊",
     *     description="返回所有Sttp的資訊",
     *     tags={"Sttps"},
     *     @OA\Parameter(
     *         name="d_id",
     *         in="query",
     *         description="Department ID",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="u_id",
     *         in="query",
     *         description="Unit ID",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="p_id",
     *         in="query",
     *         description="Position ID",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有Sttp資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/Sttp")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋Sttp發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $department = $request->query('d_id') ?? null;
        $unit = $request->query('u_id') ?? null;
        $position = $request->query('p_id') ?? null;
        $data = [
            'd_id' => $department,
            'u_id' => $unit,
            'p_id' => $position
        ];
        $result = $this->sttpService->get($id, $data);
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Post(
     *     path="/api/sttp",
     *     summary="創建Sttp",
     *     description="創建Sttp",
     *     tags={"Sttps"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建Sttp",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *               required={"d_id"},
     *                 @OA\Property(property="d_id", type="string", nullable=true, example="d_id (部門代號)"),
     *                 @OA\Property(property="u_id", type="string", nullable=true, example="u_id (單位代號)"),
     *                 @OA\Property(property="p_id", type="string", nullable=true, example="p_id (職位代號)")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Sttp updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/SetSttp"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該Sttp已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     *  * @OA\PUT(
     *     path="/api/sttp/{id}",
     *     summary="修改Sttp",
     *     description="修改Sttp",
     *     tags={"Sttps"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改Sttp資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"d_id"},
     *                 @OA\Property(property="d_id", type="string", nullable=true, example="d_id (部門代號)"),
     *                 @OA\Property(property="u_id", type="string", nullable=true, example="u_id (單位代號)"),
     *                 @OA\Property(property="p_id", type="string", nullable=true, example="p_id (職位代號)")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Sttp updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/SetSttp"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該Sttp已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function set(Request $request)
    {
        try {
            $id = $request->route('id');
            $validatedData = $request->validate([
                'd_id' => 'required|string',
                'u_id' => 'nullable|string',
                'p_id' => 'nullable|string',
            ]);
            $result = $this->sttpService->set($validatedData, $id);
            return response()->json($result, $result['status']);
        } catch (ValidationException $e) {
            return response()->json(['success' => false, 'errors' => $e->validator->errors()], 422);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/sttp/{id}",
     *     summary="根據ID刪除課股",
     *     description="根據提供的ID刪除課股",
     *     tags={"Sttps"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Sttp delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->sttpService->delete($id);
        return response()->json($result, $result['status']);
    }
}
