<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CourseService;

class CourseController extends Controller
{
    protected $courseService;

    /**
     * @OA\Schema(
     *     schema="Course",
     *     @OA\Property(property="c_id", type="integer"),
     *     @OA\Property(property="c_name", type="string"),
     *     @OA\Property(property="ct_id", type="integer"),
     *     @OA\Property(property="hours", type="integer"),
     *     @OA\Property(property="t_name", type="string"),
     *     @OA\Property(property="t_unit", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    /**
     * @OA\Get(
     *     path="/api/course/{id}",
     *     summary="根據ID取得課程資訊",
     *     description="根據提供的ID取得課程資訊",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="課程ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Course"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/course",
     *     summary="返回所有課程資訊",
     *     description="返回所有課程的資訊",
     *     tags={"Courses"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/Course")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->courseService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/course/getCourseByType/{id}",
     *     summary="根據Type ID取得課程資訊",
     *     description="根據提供的根據Type ID取得課程資訊",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="課程分類ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Course"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function getCourseByType(Request $request)
    {
        $id = $request->route('id');
        $result = $this->courseService->getCourseByType($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/course",
     *     summary="創建課程",
     *     description="創建課程",
     *     tags={"Courses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建課程",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"c_name", "ct_id", "hours", "t_name", "t_unit"},
     *                 @OA\Property(property="c_name", type="string", example="c_name"),
     *                 @OA\Property(property="ct_id", type="integer", example="type"),
     *                 @OA\Property(property="hours", type="integer", example="hours"),
     *                 @OA\Property(property="t_name", type="string", example="t_name"),
     *                 @OA\Property(property="t_unit", type="string", example="t_unit"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Organizer updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Course"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該課程已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Course with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/course/{id}",
     *     summary="修改課程",
     *     description="修改課程",
     *     tags={"Courses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="課程ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改課程資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"c_name", "ct_id", "hours", "t_name", "t_unit"},
     *                 @OA\Property(property="c_name", type="string", example="c_name"),
     *                 @OA\Property(property="ct_id", type="integer", example="ct_id"),
     *                 @OA\Property(property="hours", type="integer", example="hours"),
     *                 @OA\Property(property="t_name", type="string", example="t_name"),
     *                 @OA\Property(property="t_unit", type="string", example="t_unit"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Course updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Course"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該課程已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Course with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Course not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function set(Request $request)
    {
        $id = $request->route('id');
        $validateData = $request->validate([
            'c_name' => 'required|string|max:50',
            "type" => 'required|string|max:50',
            'hours' => 'required|integer|max:10',
            't_name' => 'required|string|max:50',
            't_unit' => 'required|string|max:50',
            't_position' => 'required|string|max:50'
        ]);
        $result = $this->courseService->set($validateData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/course/{id}",
     *     summary="根據ID刪除課程",
     *     description="根據提供的ID刪除課程",
     *     tags={"Courses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="課程ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Course delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Course ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->courseService->delete($id);
        return response()->json($result);
    }
}
