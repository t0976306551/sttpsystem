<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PromotionService;

class PromotionController extends Controller
{
    protected $promotionService;
    /**
     * @OA\Schema(
     *     schema="Promotion",
     *     @OA\Property(property="pr_id", type="integer"),
     *     @OA\Property(property="sttp_id", type="string"),
     *     @OA\Property(property="pr_sttp", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */
    public function __construct(PromotionService $promotionService)
    {
        $this->promotionService = $promotionService;
    }

    /**
     * @OA\Get(
     *     path="/api/promotion/getPromotionBySttpId/{id}",
     *     summary="根據ID取得所有關於升遷Sttp資訊",
     *     description="根據ID取得所有關於升遷Sttp的資訊",
     *     tags={"Promotions"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有關於升遷Sttp資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/Promotion")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋所有關於升遷Sttp資訊發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function getPromotionBySttpId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->promotionService->getPromotionBySttpId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/promotion/createPromotion",
     *     summary="創建升遷Sttp",
     *     description="創建升遷Sttp",
     *     tags={"Promotions"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建升遷Sttp",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"sttp_id"},
     *                 @OA\Property(property="sttp_id", type="string", example="DESDFE"),
     *                 @OA\Property(property="pr_sttp", type="array",
     *                   @OA\Items(type="string",example="MFMPAM"),
     *                ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Promotion updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Promotion"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該升遷Sttp已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Promotion with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function createPromotion(Request $request)
    {
        $validateData = $request->validate([
            'sttp_id' => 'required|string|max:10',
            'pr_sttp.*' => 'required|string'
        ]);
        $result = $this->promotionService->createPromotion($validateData);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/promotion/deletePromotion/{id}",
     *     summary="根據ID刪除升遷Sttp",
     *     description="根據提供的ID刪除升遷Sttp",
     *     tags={"Promotions"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="升遷Sttp ID（選填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="sttp_id",
     *         in="query",
     *         description="Sttp ID（選填）",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *    @OA\Parameter(
     *         name="pr_sttp",
     *         in="query",
     *         description="pr_sttp（選填）",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Promotion delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Promotion ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function deletePromotion(Request $request)
    {
        $id = $request->route('id');
        $sttp_id = $request->query('sttp_id') ?? null;
        $pr_sttp = $request->query('pr_sttp') ?? null;
        $data = [
            'sttp_id' => $sttp_id,
            'pr_sttp' => $pr_sttp
        ];
        $result = $this->promotionService->deletePromotion($id, $data);
        return response()->json($result);
    }
}
