<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClassesService;

class ClassesController extends Controller
{
    protected $classesService;

    /**
     * @OA\Schema(
     *     schema="Classes",
     *     @OA\Property(property="c_id", type="integer"),
     *     @OA\Property(property="cs_id", type="integer"),
     *     @OA\Property(property="cer_id", type="integer"),
     *     @OA\Property(property="c_name", type="string"),
     *     @OA\Property(property="training_day", type="integer"),
     *     @OA\Property(property="training_hours", type="integer"),
     *     @OA\Property(property="class_number", type="integer"),
     *     @OA\Property(property="academic_standards", type="integer"),
     *     @OA\Property(property="practical_standards", type="integer"),
     *     @OA\Property(property="traing_active_years", type="integer"),
     *     @OA\Property(property="remarks", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(ClassesService $classesService)
    {
        $this->classesService = $classesService;
    }

    /**
     * @OA\Get(
     *     path="/api/classes/{id}",
     *     summary="根據ID取得班別資訊",
     *     description="根據提供的ID取得班別資訊",
     *     tags={"Classes"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得班別資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Classes"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班別發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/classes",
     *     summary="返回所有班別資訊",
     *     description="返回所有班別的資訊",
     *     tags={"Classes"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有班別資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/Classes")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班別發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classesService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/classes/getClassesByClassScheduleId/{id}",
     *     summary="根據班次ID取得班別資訊",
     *     description="根據提供班次的ID取得班別資訊",
     *     tags={"Classes"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別 ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得班別資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Classes"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班別發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function getClassesByClassScheduleId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classesService->getClassesByClassScheduleId($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/classes/getClassesByCertificateId/{id}",
     *     summary="根據證照ID取得班別資訊",
     *     description="根據提供證照的ID取得班別資訊",
     *     tags={"Classes"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="證照 ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得班別資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Classes"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班別發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function getClassesByCertificateId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classesService->getClassesByCertificateId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/classes",
     *     summary="創建班別",
     *     description="創建班別",
     *     tags={"Classes"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建班別",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"cs_id", "cer_id", "c_name", "training_day", "training_hours", "class_number", "academic_standards", "practical_standards", "traing_active_years", "remarks"},
     *                 @OA\Property(property="cs_id", type="integer", example="cs_id"),
     *                 @OA\Property(property="cer_id", type="integer", example="cer_id"),
     *                 @OA\Property(property="c_name", type="string", example="c_name"),
     *                 @OA\Property(property="training_day", type="integer", example="training_day"),
     *                 @OA\Property(property="training_hours", type="integer", example="training_hours"),
     *                 @OA\Property(property="class_number", type="integer", example="class_number"),
     *                 @OA\Property(property="academic_standards", type="integer", example="academic_standards"),
     *                 @OA\Property(property="practical_standards", type="integer", example="practical_standards"),
     *                 @OA\Property(property="traing_active_years", type="integer", example="traing_active_years"),
     *                 @OA\Property(property="remarks", type="string", example="remarks"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Classes updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Classes"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該班別已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Classes with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/classes/{id}",
     *     summary="修改班別",
     *     description="修改班別",
     *     tags={"Classes"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改班別資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"cs_id", "cer_id", "c_name", "training_day", "training_hours", "class_number", "academic_standards", "practical_standards", "traing_active_years", "remarks"},
     *                 @OA\Property(property="cs_id", type="integer", example="cs_id"),
     *                 @OA\Property(property="cer_id", type="integer", example="cer_id"),
     *                 @OA\Property(property="c_name", type="string", example="c_name"),
     *                 @OA\Property(property="training_day", type="integer", example="training_day"),
     *                 @OA\Property(property="training_hours", type="integer", example="training_hours"),
     *                 @OA\Property(property="class_number", type="integer", example="class_number"),
     *                 @OA\Property(property="academic_standards", type="integer", example="academic_standards"),
     *                 @OA\Property(property="practical_standards", type="integer", example="practical_standards"),
     *                 @OA\Property(property="traing_active_years", type="integer", example="traing_active_years"),
     *                 @OA\Property(property="remarks", type="string", example="remarks"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Classes updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Classes"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該班別已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Classes with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Classes not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function set(Request $request)
    {
        $id = $request->route('id');
        $validateData = $request->validate([
            'cs_id' => 'required|integer',
            'cer_id' => 'required|integer',
            'c_name' => 'required|string|max:50',
            'training_day' => 'required|integer',
            'training_hours' => 'required|integer',
            'class_number' => 'required|integer',
            'academic_standards' => 'required|integer',
            'practical_standards' => 'required|integer',
            'traing_active_years' => 'required|integer',
            'remarks' => 'required|string|max:50',

        ]);
        $result = $this->classesService->set($validateData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/classes/{id}",
     *     summary="根據ID刪除班別",
     *     description="根據提供的ID刪除班別",
     *     tags={"Classes"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Classes delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Classes ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classesService->delete([], $id);
        return response()->json($result);
    }
}
