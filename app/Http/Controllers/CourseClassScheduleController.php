<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CourseClassScheduleService;

class CourseClassScheduleController extends Controller
{
    protected $courseClassScheduleService;
    /**
     * @OA\Schema(
     *     schema="CourseClassSchedule",
     *     @OA\Property(property="ccs_id", type="integer"),
     *     @OA\Property(property="c_id", type="integer"),
     *     @OA\Property(property="cs_id", type="integer"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * ),
     * @OA\Schema(
     *     schema="CourseClassScheduleByCourse",
     *     @OA\Property(property="ccs_id", type="integer"),
     *     @OA\Property(property="c_id", type="integer"),
     *     @OA\Property(property="cs_id", type="integer"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     *     @OA\Property(property="department", ref="#/components/schemas/Course"),
     * ),
     * @OA\Schema(
     *     schema="CourseClassScheduleByClassSchedule",
     *     @OA\Property(property="ccs_id", type="integer"),
     *     @OA\Property(property="c_id", type="integer"),
     *     @OA\Property(property="cs_id", type="integer"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     *     @OA\Property(property="department", ref="#/components/schemas/ClassSchedule"),
     * )
     */
    public function __construct(CourseClassScheduleService $courseClassScheduleService)
    {
        $this->courseClassScheduleService = $courseClassScheduleService;
    }

    /**
     * @OA\Get(
     *     path="/api/courseClassSchedule/getCourseByClassScheduleId/{id}",
     *     summary="根據ID取得所有關於班次的課程資訊",
     *     description="根據提供的ID取得所有關於班次的課程資訊",
     *     tags={"CourseClassSchedules"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ClassSchedule ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有關於班次的課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/CourseClassScheduleByClassSchedule"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋所有關於班次的課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/courseClassSchedule/getClassScheduleByCourseId/{id}",
     *     summary="根據ID取得所有關於課程的班次資訊",
     *     description="根據ID取得所有關於課程的班次的資訊",
     *     tags={"CourseClassSchedules"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Course ID",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有關於課程的班次資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/CourseClassScheduleByCourse")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋所有關於課程的班次發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function getCourseByClassScheduleId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->courseClassScheduleService->getCourseByClassScheduleId($id);
        return response()->json($result);
    }

    public function getClassScheduleByCourseId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->courseClassScheduleService->getClassScheduleByCourseId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/courseClassSchedule/createClassScheduleAndCourse",
     *     summary="創建班次課程",
     *     description="創建班次課程",
     *     tags={"CourseClassSchedules"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建班次課程",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"classScheduleId"},
     *                 @OA\Property(property="classScheduleId", type="integer", example="1"),
     *                 @OA\Property(property="courseId", type="array",
     *                   @OA\Items(type="integer",example=1),
     *                ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="CourseClassSchedule updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/CourseClassSchedule"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該班次課程已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="CourseClassSchedule with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function createClassScheduleAndCourse(Request $request)
    {
        $validateData = $request->validate([
            'classScheduleId' => 'required|integer',
            'courseId.*' => 'required|integer',
        ]);
        $result = $this->courseClassScheduleService->createClassScheduleAndCourse($validateData['classScheduleId'], $validateData['courseId']);
        return response()->json($result);
    }


    /**
     * @OA\Delete(
     *     path="/api/courseClassSchedule/deleteClassScheduleAndCourse",
     *     summary="根據ID刪除班次課程",
     *     description="根據提供的ID刪除班次課程",
     *     tags={"CourseClassSchedules"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="classScheduleId",
     *         in="query",
     *         description="班次ID（選填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="courseId",
     *         in="query",
     *         description="課程ID（選填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="CourseClassSchedule delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="CourseClassSchedule ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function deleteClassScheduleAndCourse(Request $request)
    {
        $classScheduleId = $request->query('classScheduleId') ?? null;
        $courseId = $request->query('courseId') ?? null;
        $result = $this->courseClassScheduleService->deleteClassScheduleAndCourse($classScheduleId, $courseId);
        return response()->json($result);
    }
}
