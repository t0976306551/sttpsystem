<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TrainingUnitService;

class TrainingUnitController extends Controller
{
    protected $trainingUnitService;

    /**
     * @OA\Schema(
     *     schema="TrainingUnit",
     *     @OA\Property(property="tu_id", type="integer"),
     *     @OA\Property(property="tu_name", type="string"),
     *     @OA\Property(property="tu_location", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(TrainingUnitService $trainingUnitService)
    {
        $this->trainingUnitService = $trainingUnitService;
    }

    /**
     * @OA\Get(
     *     path="/api/trainingUnit/{id}",
     *     summary="根據ID取得訓練單位資訊",
     *     description="根據提供的ID取得訓練單位資訊",
     *     tags={"TrainingUnits"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練單位ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得訓練單位資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingUnit"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋訓練單位發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/trainingUnit",
     *     summary="返回所有訓練單位資訊",
     *     description="返回所有訓練單位的資訊",
     *     tags={"TrainingUnits"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有訓練單位資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/TrainingUnit")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋訓練單位發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->trainingUnitService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/trainingUnit",
     *     summary="創建訓練單位",
     *     description="創建訓練單位",
     *     tags={"TrainingUnits"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建訓練單位",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"tu_name", "tu_location"},
     *                 @OA\Property(property="tu_name", type="string", example="tu_name"),
     *                 @OA\Property(property="tu_location", type="string", example="tu_location"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingUnit updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingUnit"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該訓練單位已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingUnit with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/trainingUnit/{id}",
     *     summary="修改訓練單位",
     *     description="修改訓練單位",
     *     tags={"TrainingUnits"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練單位ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改訓練單位資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *            @OA\Schema(
     *                 required={"tu_name", "tu_location"},
     *                 @OA\Property(property="tu_name", type="string", example="tu_name"),
     *                 @OA\Property(property="tu_location", type="string", example="tu_location"),
     *            )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingUnit updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingUnit"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該訓練單位已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingUnit with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingUnit not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function set(Request $request)
    {
        $id = $request->route('id');
        $validateData = $request->validate([
            'tu_name' => 'required|string|max:50',
            'tu_location' => 'required|string|max:50',
        ]);
        $result = $this->trainingUnitService->set($validateData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/trainingUnit/{id}",
     *     summary="根據ID刪除主辦單位",
     *     description="根據提供的ID刪除主辦單位",
     *     tags={"TrainingUnits"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="主辦單位ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingUnit delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingUnit ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->trainingUnitService->delete($id);
        return response()->json($result);
    }
}
