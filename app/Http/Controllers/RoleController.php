<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\RoleService;

class RoleController extends Controller
{
    protected $roleService;

    /**
     * @OA\Schema(
     *     schema="Role",
     *     @OA\Property(property="r_id", type="integer"),
     *     @OA\Property(property="r_name", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * @OA\Get(
     *     path="/api/role/{id}",
     *     summary="根據ID取得角色資訊",
     *     description="根據提供的ID取得角色資訊",
     *     tags={"Roles"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="角色ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得角色資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋角色發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/role",
     *     summary="返回所有角色資訊",
     *     description="返回所有角色的資訊",
     *     tags={"Roles"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有角色資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/Role")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋角色發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->roleService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/role",
     *     summary="創建角色",
     *     description="創建角色",
     *     tags={"Roles"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建角色",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"r_name"},
     *                 @OA\Property(property="r_name", type="string", example="r_name")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Role updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該角色已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Role with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     *  * @OA\PUT(
     *     path="/api/role/{id}",
     *     summary="修改角色",
     *     description="修改角色",
     *     tags={"Roles"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="角色的ID（必填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改角色資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"r_name"},
     *                 @OA\Property(property="r_name", type="string", example="r_name")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Role updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Role"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該角色已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Role with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Role not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function set(Request $request)
    {
        $id = $request->route('id');
        $validatedData = $request->validate([
            'r_name' => 'required|string|max:10',
        ]);
        $result = $this->roleService->set($validatedData, $id);

        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/role/{id}",
     *     summary="根據ID刪除角色",
     *     description="根據提供的ID刪除角色",
     *     tags={"Roles"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="角色的ID（選填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Role delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Role ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->roleService->delete($id);
        return response()->json($result);
    }
}
