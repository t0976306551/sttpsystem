<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FlatToneService;

class FlatToneController extends Controller
{
    protected $flatToneService;
    /**
     * @OA\Schema(
     *     schema="FlatTone",
     *     @OA\Property(property="ft_id", type="integer"),
     *     @OA\Property(property="sttp_id", type="string"),
     *     @OA\Property(property="ft_sttp", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */
    public function __construct(FlatToneService $flatToneService)
    {
        $this->flatToneService = $flatToneService;
    }

    /**
     * @OA\Get(
     *     path="/api/flatTone/getFlatToneBySttpId/{id}",
     *     summary="根據ID取得所有關於平調Sttp資訊",
     *     description="根據ID取得所有關於平調Sttp的資訊",
     *     tags={"FlatTones"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp ID",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有關於平調Sttp資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/FlatTone")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋所有關於平調Sttp資訊發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function getFlatToneBySttpId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->flatToneService->getFlatToneBySttpId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/flatTone/createFlatTone",
     *     summary="創建平調Sttp",
     *     description="創建平調Sttp",
     *     tags={"FlatTones"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建平調Sttp",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"sttp_id"},
     *                 @OA\Property(property="sttp_id", type="string", example="DESDFE"),
     *                 @OA\Property(property="ft_sttp", type="array",
     *                   @OA\Items(type="string",example="MFMPAM"),
     *                ),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="FlatTone updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/FlatTone"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該平調Sttp已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="FlatTone with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function createFlatTone(Request $request)
    {
        $validateData = $request->validate([
            'sttp_id' => 'required|string|max:10',
            'ft_sttp.*' => 'required|string'
        ]);
        $result = $this->flatToneService->createFlatTone($validateData);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/flatTone/deleteFlatTone/{id}",
     *     summary="根據ID刪除平調Sttp",
     *     description="根據提供的ID刪除平調Sttp",
     *     tags={"FlatTones"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="平調Sttp ID（選填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="sttp_id",
     *         in="query",
     *         description="Sttp ID（選填）",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *    @OA\Parameter(
     *         name="ft_sttp",
     *         in="query",
     *         description="ft_sttp（選填）",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="FlatTone delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="FlatTone ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function deleteFlatTone(Request $request)
    {
        $id = $request->route('id');
        $sttp_id = $request->query('sttp_id') ?? null;
        $ft_sttp = $request->query('ft_sttp') ?? null;
        $data = [
            'sttp_id' => $sttp_id,
            'ft_sttp' => $ft_sttp
        ];
        $result = $this->flatToneService->deleteFlatTone($id, $data);
        return response()->json($result);
    }
}
