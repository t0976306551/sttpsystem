<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SignUpService;

class SignUpController extends Controller
{
    protected $signUpService;

    /**
     * @OA\Schema(
     *     schema="SignUp",
     *     @OA\Property(property="su_id", type="integer"),
     *     @OA\Property(property="cs_id", type="integer"),
     *     @OA\Property(property="s_id", type="integer"),
     * )
     */

    public function __construct(SignUpService $signUpService)
    {
        $this->signUpService = $signUpService;
    }

    /**
     * @OA\Get(
     *     path="/api/signUp/{id}",
     *     summary="根據ID取得報名資訊",
     *     description="根據提供的ID取得報名資訊",
     *     tags={"SignUps"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="報名ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得報名資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/SignUp"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋報名發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/signUp",
     *     summary="返回所有報名資訊",
     *     description="返回所有報名的資訊",
     *     tags={"SignUps"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有報名資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/SignUp")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋報名發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->signUpService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/signUp/getSignUpByClassesId/{id}",
     *     summary="根據班次ID取得報名資訊",
     *     description="根據提供的班次ID取得報名資訊",
     *     tags={"SignUps"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班次ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得報名資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/SignUp"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋報名發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function getSignUpByClassesId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->signUpService->getSignUpByClassesId($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/signUp/getSignUpByStaffId/{id}",
     *     summary="根據職員ID取得報名資訊",
     *     description="根據提供的職員ID取得報名資訊",
     *     tags={"SignUps"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="職員ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得報名資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/SignUp"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋報名發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function getSignUpByStaffId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->signUpService->getSignUpByStaffId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/signUp",
     *     summary="創建報名資訊",
     *     description="創建報名資訊",
     *     tags={"SignUps"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建報名資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"c_id", "s_id"},
     *                 @OA\Property(property="c_id", type="integer", example="1"),
     *                 @OA\Property(property="s_id", type="integer", example="1"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="SignUp created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/SignUp"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該報名資訊已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="SignUp with the same data already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'c_id' => 'required|integer',
            's_id' => 'required|string',
        ]);
        $result = $this->signUpService->createSignUp($validatedData);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/signUp",
     *     summary="根據ID 或者 班別ID 或 職員ID 刪除報名資訊",
     *     description="根據提供的ID 或者 班別ID 或 職員ID刪除課程",
     *     tags={"SignUps"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="報名ID（選填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="c_id",
     *         in="query",
     *         description="班別ID（選填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="s_id",
     *         in="query",
     *         description="職員ID（選填）",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="SignUp delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="SignUp ID or Classes ID or Staff ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $c_id = $request->query('c_id') ?? null;
        $s_id = $request->query('s_id') ?? null;
        $data = [
            'c_id' => $c_id,
            's_id' => $s_id
        ];

        if (!is_null($id)) {
            if (!is_null($c_id) || !is_null($s_id)) {
                return response()->json([
                    'success' => false,
                    'message' => "With id, you can't bring in other parameters",
                    'status' => 404,
                ]);
            }
        }

        if (is_null($id)) {
            if (!is_null($c_id) && !is_null($s_id)) {
                return response()->json([
                    'success' => false,
                    'message' => "Only one cs_id and one s_id can be entered.",
                    'status' => 404,
                ]);
            }
        }
        $result = $this->signUpService->delete($id, $data);
        return response()->json($result);
    }
}
