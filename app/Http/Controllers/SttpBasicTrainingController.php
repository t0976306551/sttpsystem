<?php

namespace App\Http\Controllers;

use App\Services\SttpBasicTrainingService;
use Illuminate\Http\Request;

class SttpBasicTrainingController extends Controller
{
    protected $sttpBasicTrainingService;

    /**
     * @OA\Schema(
     *     schema="SttpBasicTraining",
     *     @OA\Property(property="sbt_id", type="integer"),
     *     @OA\Property(property="sttp_id", type="string"),
     *     @OA\Property(property="c_id", type="integer"),
     *     @OA\Property(property="elective_type", type="string"),
     *     @OA\Property(property="credit_conditions", type="string"),
     *     @OA\Property(property="classes_type", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(SttpBasicTrainingService $sttpBasicTrainingService)
    {
        $this->sttpBasicTrainingService = $sttpBasicTrainingService;
    }

    /**
     * @OA\Get(
     *     path="/api/sttpBasicTraining/{id}",
     *     summary="根據ID取得Sttp基本訓練資訊",
     *     description="根據提供的ID取得Sttp基本訓練資訊",
     *     tags={"SttpBasicTrainings"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp基本訓練ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得Sttp基本訓練資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/SttpBasicTraining"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋Sttp基本訓練發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/sttpBasicTraining",
     *     summary="返回所有Sttp基本訓練資訊",
     *     description="返回所有Sttp基本訓練的資訊",
     *     tags={"SttpBasicTrainings"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有Sttp基本訓練資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/SttpBasicTraining")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋Sttp基本訓練發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->sttpBasicTrainingService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/sttpBasicTraining/getBySttpId/{id}",
     *     summary="根據Sttp ID取得Sttp基本訓練資訊",
     *     description="根據提供的Sttp ID取得Sttp基本訓練資訊",
     *     tags={"SttpBasicTrainings"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得Sttp基本訓練資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/SttpBasicTraining"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋Sttp基本訓練發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function getBySttpId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->sttpBasicTrainingService->getBySttpId($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/sttpBasicTraining/getByClassesId/{id}",
     *     summary="根據班次ID取得Sttp基本訓練資訊",
     *     description="根據提供的班次ID取得Sttp基本訓練資訊",
     *     tags={"SttpBasicTrainings"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班次 ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得Sttp基本訓練資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/SttpBasicTraining"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋Sttp基本訓練發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function getByClassesId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->sttpBasicTrainingService->getByClassesId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/sttpBasicTraining",
     *     summary="創建Sttp基本訓練資訊",
     *     description="創建Sttp基本訓練資訊",
     *     tags={"SttpBasicTrainings"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建Sttp基本訓練資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"sttp_id", "c_id", "elective_type", "credit_conditions", "classes_type"},
     *                 @OA\Property(property="sttp_id", type="string", example="DESDFE"),
     *                 @OA\Property(property="c_id", type="integer", example="1"),
     *                 @OA\Property(property="elective_type", type="integer", example="必修"),
     *                 @OA\Property(property="credit_conditions", type="integer", example="研習過相關技術課程"),
     *                 @OA\Property(property="classes_type", type="integer", example="訓練"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="SttpBasicTraining created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/SttpBasicTraining"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該Sttp基本訓練資訊已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="SttpBasicTraining with the same data already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/sttpBasicTraining/{id}",
     *     summary="修改Sttp基本訓練",
     *     description="修改Sttp基本訓練",
     *     tags={"SttpBasicTrainings"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp基本訓練的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改Sttp基本訓練資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"sttp_id", "c_id", "elective_type", "credit_conditions", "classes_type"},
     *                 @OA\Property(property="sttp_id", type="string", example="DESDFE"),
     *                 @OA\Property(property="c_id", type="integer", example="1"),
     *                 @OA\Property(property="elective_type", type="integer", example="必修"),
     *                 @OA\Property(property="credit_conditions", type="integer", example="研習過相關技術課程"),
     *                 @OA\Property(property="classes_type", type="integer", example="訓練"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="SttpBasicTraining updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/SttpBasicTraining"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該Sttp基本訓練已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="SttpBasicTraining with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="SttpBasicTraining not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function set(Request $request)
    {
        $id = $request->route('id');
        $validatedData = $request->validate([
            'sttp_id' => 'required|string',
            'c_id' => 'required|integer',
            'elective_type' => 'required|string|max:10',
            'credit_conditions' => 'required|string',
            'classes_type' => 'required|string|max:10',
        ]);
        $result = $this->sttpBasicTrainingService->set($validatedData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/sttpBasicTraining/{id}",
     *     summary="根據ID刪除Sttp基本訓練",
     *     description="根據提供的ID刪除Sttp基本訓練",
     *     tags={"SttpBasicTrainings"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp基本訓練的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="SttpBasicTraining delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="SttpBasicTraining ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->sttpBasicTrainingService->delete($id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/sttpBasicTraining/deleteBySttpId/{id}",
     *     summary="根據Sttp ID刪除Sttp基本訓練",
     *     description="根據提供的根據Sttp ID刪除Sttp基本訓練",
     *     tags={"SttpBasicTrainings"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp 的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="SttpBasicTraining delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Sttp ID is required to delete a SttpBasicTraining resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function deleteBySttpId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->sttpBasicTrainingService->deleteBySttpId($id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/sttpBasicTraining/deleteByClassesId/{id}",
     *     summary="根據班別ID刪除Sttp基本訓練",
     *     description="根據提供的班別ID刪除Sttp基本訓練",
     *     tags={"SttpBasicTrainings"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Sttp 的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="SttpBasicTraining delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Classes ID is required to delete a SttpBasicTraining resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function deleteByClassesId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->sttpBasicTrainingService->deleteByClassesId($id);
        return response()->json($result);
    }
}
