<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClassesTrainingCourseService;

class ClassesTrainingCourseController extends Controller
{
    protected $classTrainingCourseSerivce;

    /**
     * @OA\Schema(
     *     schema="ClassesTrainingCourse",
     *     @OA\Property(property="ctc_id", type="integer"),
     *     @OA\Property(property="c_id", type="integer"),
     *     @OA\Property(property="tc_id", type="integer"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */
    public function __construct(ClassesTrainingCourseService $classesTrainingCourseService)
    {
        $this->classTrainingCourseSerivce = $classesTrainingCourseService;
    }

    /**
     * @OA\Get(
     *     path="/api/classesTrainingCourse/{id}",
     *     summary="根據ID取得班別訓練課程資訊",
     *     description="根據提供的ID取得班別訓練課程資訊",
     *     tags={"ClassesTrainingCourses"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別訓練課程ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得班別訓練課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/ClassesTrainingCourse"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班別訓練課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/classesTrainingCourse",
     *     summary="返回所有班別訓練課程資訊",
     *     description="返回所有班別訓練課程的資訊",
     *     tags={"ClassesTrainingCourses"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有班別訓練課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/ClassesTrainingCourse")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班別訓練課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classTrainingCourseSerivce->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/classesTrainingCourse/getByClassesId/{id}",
     *     summary="根據班別ID取得班別訓練課程資訊",
     *     description="根據提供的班別ID取得班別訓練課程資訊",
     *     tags={"ClassesTrainingCourses"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得班別訓練課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/ClassesTrainingCourse"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班別訓練課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function getByClassesId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classTrainingCourseSerivce->getByClassesId($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/classesTrainingCourse/getByTrainingCourseId/{id}",
     *     summary="根據訓練課程別ID取得班別訓練課程資訊",
     *     description="根據提供的訓練課程別I取得班別訓練課程資訊",
     *     tags={"ClassesTrainingCourses"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練課程別ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得班別訓練課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/ClassesTrainingCourse"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班別訓練課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     */

    public function getByTrainingCourseId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classTrainingCourseSerivce->getByTrainingCourseId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/classesTrainingCourse",
     *     summary="創建班別訓練課程",
     *     description="創建班別訓練課程",
     *     tags={"ClassesTrainingCourses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建班別訓練課程",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"c_id", "tc_id"},
     *                 @OA\Property(property="c_id", type="integer", example=1),
     *                 @OA\Property(property="tc_id", type="integer", example=1),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="ClassesTrainingCourse updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/ClassesTrainingCourse"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該班別訓練課程已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassesTrainingCourse with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/classesTrainingCourse/{id}",
     *     summary="修改班別訓練課程",
     *     description="修改班別訓練課程",
     *     tags={"ClassesTrainingCourses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別訓練課程ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改班別訓練課程資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                required={"c_id", "tc_id"},
     *                @OA\Property(property="c_id", type="integer", example=1),
     *                @OA\Property(property="tc_id", type="integer", example=1),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="ClassesTrainingCourse updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/ClassesTrainingCourse"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該班別訓練課程已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassesTrainingCourse with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassesTrainingCourse not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function set(Request $request)
    {
        $id = $request->route('id');
        $validatedData = $request->validate([
            'c_id' => 'required|integer',
            'tc_id' => 'required|integer'
        ]);
        $result = $this->classTrainingCourseSerivce->set($validatedData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/classesTrainingCourse/{id}",
     *     summary="根據ID刪除班別訓練課程",
     *     description="根據提供的ID刪除班別訓練課程",
     *     tags={"ClassesTrainingCourses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別訓練課程ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="ClassesTrainingCourse delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassesTrainingCourse ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classTrainingCourseSerivce->delete($id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/classesTrainingCourse/deleteByClassesId/{id}",
     *     summary="根據班次ID刪除班別訓練課程",
     *     description="根據提供的班次ID刪除班別訓練課程",
     *     tags={"ClassesTrainingCourses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="ClassesTrainingCourse delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassesTrainingCourse ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function deleteByClassesId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classTrainingCourseSerivce->deleteByClassesId($id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/classesTrainingCourse/deleteByTrainingCourseId/{id}",
     *     summary="根據訓練課程ID刪除班別訓練課程",
     *     description="根據提供的訓練課程ID刪除班別訓練課程",
     *     tags={"ClassesTrainingCourses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練課程ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="ClassesTrainingCourse delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassesTrainingCourse ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function deleteByTrainingCourseId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classTrainingCourseSerivce->deleteByTrainingCourseId($id);
        return response()->json($result);
    }
}
