<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CourseTypeService;

class CourseTypeController extends Controller
{
    protected $courseTypeService;

    /**
     * @OA\Schema(
     *     schema="CourseType",
     *     @OA\Property(property="ct_id", type="integer"),
     *     @OA\Property(property="type", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */
    public function __construct(CourseTypeService $courseTypeService)
    {
        $this->courseTypeService = $courseTypeService;
    }

    /**
     * @OA\Get(
     *     path="/api/courseType/{id}",
     *     summary="根據ID取得課程類別資訊",
     *     description="根據提供的ID取得課程類別資訊",
     *     tags={"CourseTypes"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="課程類別ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得課程類別資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/CourseType"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋課程類別發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/courseType",
     *     summary="返回所有課程類別資訊",
     *     description="返回所有課程類別的資訊",
     *     tags={"CourseTypes"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有課程類別資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/CourseType")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋課程類別發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->courseTypeService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/courseType",
     *     summary="創建課程類別",
     *     description="創建課程類別",
     *     tags={"CourseTypes"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建課程類別",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"type"},
     *                 @OA\Property(property="type", type="string", example="type"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="CourseType updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/CourseType"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該課程類別已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="CourseType with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/courseType/{id}",
     *     summary="修改課程類別",
     *     description="修改課程類別",
     *     tags={"CourseTypes"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="課程類別ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改課程類別資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"type"},
     *                 @OA\Property(property="type", type="string", example="type"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="CourseType updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/CourseType"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該課程類別已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="CourseType with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="CourseType not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function set(Request $request)
    {
        $id = $request->route('id');
        $validateData = $request->validate([
            'type' => 'required|string|max:50',
        ]);
        $result = $this->courseTypeService->set($validateData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/courseType/{id}",
     *     summary="根據ID刪除課程類別",
     *     description="根據提供的ID刪除課程類別",
     *     tags={"CourseTypes"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="課程類別ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="CourseType delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="CourseType ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->courseTypeService->delete($id);
        return response()->json($result);
    }
}
