<?php

/*
                       _ooOoo_
                      o8888888o
                      88" . "88
                      (| -_- |)
                      O\  =  /O
                   ____/`---'\____
                 .'  \\|     |//  `.
                /  \\|||  :  |||//  \
               /  _||||| -:- |||||-  \
               |   | \\\  -  /// |   |
               | \_|  ''\---/''  |   |
               \  .-\__  `-`  ___/-. /
             ___`. .'  /--.--\  `. . __
          ."" '<  `.___\_<|>_/___.'  >'"".
         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
         \  \ `-.   \_ __\ /__ _/   .-` /  /
    ======`-.____`-.___\_____/___.-`____.-'======
                       `=---='
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                佛祖保佑       永無BUG
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AuthService;
use Illuminate\Validation\ValidationException;

/**
 * @OA\OpenApi(
 *  @OA\Info(
 *      title="Sttp System API",
 *      version="1.0.0",
 *      description="人力資源發展系統 API",
 *      @OA\Contact(
 *          email="your-email@gmail.com"
 *      )
 *  ),
 *  @OA\Server(
 *      description="Swagger-doc App API",
 *      url="http://127.0.0.1:8000"
 *  ),
 *  @OA\Server(
 *      description="Swagger-doc App API 8080",
 *      url="http://127.0.0.1:8080"
 *  ),
 *
 *  @OA\PathItem(
 *      path="/"
 *  )
 * )
 *
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     in="header",
 *     securityScheme="bearerAuth",
 *     name="Authorization",
 *     description="Bearer JWT Token 以下是範例(Bearer eyJ0eXAiOiJKV1QiLCJhbGc.....)",
 * )
 */

class AuthController extends Controller
{
    /**
     * @OA\Schema(
     *     schema="User",
     *     @OA\Property(property="id", type="integer"),
     *     @OA\Property(property="username", type="string"),
     *     @OA\Property(property="name", type="string"),
     *     @OA\Property(property="r_id", type="integer"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     *
     * @OA\Schema(
     *     schema="Logout",
     *     @OA\Property(property="success", type="boolean"),
     *     @OA\Property(property="message", type="string"),
     * )
     *
     * @OA\Schema(
     *     schema="Login",
     *     @OA\Property(property="success", type="boolean"),
     *     @OA\Property(property="message", type="string"),
     *     @OA\Property(property="token", type="string"),
     *     @OA\Property(
     *         property="user",
     *         @OA\Property(property="id", type="integer"),
     *         @OA\Property(property="username", type="string"),
     *         @OA\Property(property="name", type="string"),
     *         @OA\Property(property="r_id", type="integer"),
     *         @OA\Property(property="created_at", type="datetime"),
     *         @OA\Property(property="updated_at", type="datetime"),
     *     ),
     * )
     */

    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @OA\Post(
     *     path="/api/login",
     *     summary="使用者登入",
     *     description="使用 username 和 password 登入",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="使用者登入資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"username","password"},
     *                 @OA\Property(property="username", type="string", example="username"),
     *                 @OA\Property(property="password", type="string", example="password")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="登入成功",
     *         @OA\JsonContent(
     *            ref="#/components/schemas/Login"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="登入失敗"
     *     )
     * )
     */

    public function login(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'username' => 'required|string|max:20',
                'password' => 'required|string|max:20',
            ]);
            $result = $this->authService->login($validatedData['username'], $validatedData['password']);
            return response()->json($result, $result['status']);
        } catch (ValidationException $e) {
            return response()->json(['success' => false, 'errors' => $e->validator->errors()], 422);
        }
    }

    /**
     * @OA\Post(
     *     path="/api/auth/logout",
     *     summary="使用者登出",
     *     description="登出使用者",
     *     tags={"Auth"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="登出成功",
     *         @OA\JsonContent(
     *            ref="#/components/schemas/Logout"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="登出失敗",
     *          @OA\JsonContent(
     *            ref="#/components/schemas/Logout"
     *         )
     *     )
     * )
     */

    public function logout(Request $request)
    {
        $result = $this->authService->logout();
        return response()->json($result, $result['status']);
    }
}
