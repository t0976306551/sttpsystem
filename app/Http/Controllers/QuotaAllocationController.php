<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\QuotaAllocationService;

class QuotaAllocationController extends Controller
{
    protected $quotaAllocationService;

    /**
     * @OA\Schema(
     *     schema="QuotaAllocation",
     *     @OA\Property(property="qa_id", type="integer"),
     *     @OA\Property(property="u_id", type="string"),
     *     @OA\Property(property="cs_id", type="integer"),
     *     @OA\Property(property="quota", type="integer"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */
    public function __construct(QuotaAllocationService $quotaAllocationService)
    {
        $this->quotaAllocationService = $quotaAllocationService;
    }

    /**
     * @OA\Get(
     *     path="/api/quotaAllocation/{id}",
     *     summary="根據ID取得名額分配資訊",
     *     description="根據提供的ID取得名額分配資訊",
     *     tags={"QuotaAllocations"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="名額分配ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得名額分配資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/QuotaAllocation"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋名額分配發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/quotaAllocation",
     *     summary="返回所有名額分配資訊",
     *     description="返回所有名額分配的資訊",
     *     tags={"QuotaAllocations"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有名額分配資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/QuotaAllocation")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋名額分配發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->quotaAllocationService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/quotaAllocation/getByUnitId/{id}",
     *     summary="根據單位ID取得名額分配資訊",
     *     description="根據提供的單位ID取得名額分配資訊",
     *     tags={"QuotaAllocations"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="單位ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得名額分配資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *               @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/QuotaAllocation")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋名額分配發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     */

    public function getByUnitId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->quotaAllocationService->getByUnitId($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/quotaAllocation/getByClassScheduleId/{id}",
     *     summary="根據班次ID取得名額分配資訊",
     *     description="根據提供的班別ID取得名額分配資訊",
     *     tags={"QuotaAllocations"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班次ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得名額分配資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *               @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/QuotaAllocation")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋名額分配發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     */
    public function getByClassScheduleId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->quotaAllocationService->getByClassScheduleId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/quotaAllocation",
     *     summary="創建名額分配",
     *     description="創建名額分配",
     *     tags={"QuotaAllocations"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建名額分配",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"u_id", "cs_id", "quota"},
     *                 @OA\Property(property="u_id", type="string", example="DS"),
     *                 @OA\Property(property="cs_id", type="integer", example=1),
     *                 @OA\Property(property="quota", type="integer", example=10),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="QuotaAllocation updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/QuotaAllocation"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該名額分配已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="QuotaAllocation with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/quotaAllocation/{id}",
     *     summary="修改名額分配",
     *     description="修改名額分配",
     *     tags={"QuotaAllocations"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="名額分配ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改名額分配資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"u_id", "cs_id", "quota"},
     *                 @OA\Property(property="u_id", type="string", example="DS"),
     *                 @OA\Property(property="cs_id", type="integer", example=1),
     *                 @OA\Property(property="quota", type="integer", example=10),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="QuotaAllocation updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/QuotaAllocation"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該名額分配已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="QuotaAllocation with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="QuotaAllocation not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function set(Request $request)
    {
        $id = $request->route('id');
        $validateData = $request->validate([
            'u_id' => 'required|string',
            'cs_id' => 'required|integer',
            "quota" => 'required|integer|max:1000'
        ]);
        $result = $this->quotaAllocationService->set($validateData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/quotaAllocation/{id}",
     *     summary="根據ID刪除名額分配",
     *     description="根據提供的ID刪除名額分配",
     *     tags={"QuotaAllocations"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="名額分配ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="QuotaAllocation delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="QuotaAllocation ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->quotaAllocationService->delete($id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/quotaAllocation/deleteByUnitId/{id}",
     *     summary="根據單位ID刪除名額分配",
     *     description="根據提供的單位ID刪除名額分配",
     *     tags={"QuotaAllocations"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="單位ID（選填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="QuotaAllocation delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="QuotaAllocation ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function deleteByUnitId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->quotaAllocationService->deleteByUnitId($id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/quotaAllocation/deleteByClassScheduleId/{id}",
     *     summary="根據班次ID刪除名額分配",
     *     description="根據提供的班次ID刪除名額分配",
     *     tags={"QuotaAllocations"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班次ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="QuotaAllocation delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="QuotaAllocation ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function deleteByClassScheduleId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->quotaAllocationService->deleteByClassScheduleId($id);
        return response()->json($result);
    }
}
