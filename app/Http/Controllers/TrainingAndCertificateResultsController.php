<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TrainingAndCertificateResultsService;

class TrainingAndCertificateResultsController extends Controller
{
    protected $trainingAndCertificateResultsService;

    /**
     * @OA\Schema(
     *     schema="TrainingAndCertificateResult",
     *     @OA\Property(property="tac_id", type="integer"),
     *     @OA\Property(property="c_id", type="string"),
     *     @OA\Property(property="s_id", type="string"),
     *     @OA\Property(property="academic_score", type="integer"),
     *     @OA\Property(property="practical_score", type="integer"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */
    public function __construct(TrainingAndCertificateResultsService $trainingAndCertificateResultsService)
    {
        $this->trainingAndCertificateResultsService = $trainingAndCertificateResultsService;
    }

    /**
     * @OA\Get(
     *     path="/api/trainingAndCertificateResults/{id}",
     *     summary="根據ID取得訓練及證照成績資訊",
     *     description="根據提供的ID取得訓練及證照成績資訊",
     *     tags={"TrainingAndCertificateResults"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練及證照成績ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得訓練及證照成績資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingAndCertificateResult"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋訓練及證照成績發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/trainingAndCertificateResults",
     *     summary="返回所有訓練及證照成績資訊",
     *     description="返回所有訓練及證照成績的資訊",
     *     tags={"TrainingAndCertificateResults"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有訓練及證照成績資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/TrainingAndCertificateResult")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋訓練及證照成績發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->trainingAndCertificateResultsService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/trainingAndCertificateResults/getTrainingAndCertificateResultsByStaffId/{id}",
     *     summary="根據職員ID取得訓練及證照成績資訊",
     *     description="根據提供的職員ID取得訓練及證照成績資訊",
     *     tags={"TrainingAndCertificateResults"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="職員ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得訓練及證照成績資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *               @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/TrainingAndCertificateResult")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋訓練及證照成績發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     */
    public function getTrainingAndCertificateResultsByStaffId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->trainingAndCertificateResultsService->getTrainingAndCertificateResultsByStaffId($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/trainingAndCertificateResults/getTrainingAndCertificateResultsByClassesId/{id}",
     *     summary="根據班別ID取得訓練及證照成績資訊",
     *     description="根據提供的班別ID取得訓練及證照成績資訊",
     *     tags={"TrainingAndCertificateResults"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班別ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得訓練及證照成績資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *               @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/TrainingAndCertificateResult")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋訓練及證照成績發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     */
    public function getTrainingAndCertificateResultsByClassesId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->trainingAndCertificateResultsService->getTrainingAndCertificateResultsByClassesId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/trainingAndCertificateResults",
     *     summary="創建訓練及證照成績",
     *     description="創建訓練及證照成績",
     *     tags={"TrainingAndCertificateResults"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建訓練及證照成績",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"c_id", "s_id", "academic_score", "practical_score"},
     *                 @OA\Property(property="c_id", type="integer", example=1),
     *                 @OA\Property(property="s_id", type="string", example="Y2023-1001"),
     *                 @OA\Property(property="academic_score", type="integer", example=80),
     *                 @OA\Property(property="practical_score", type="integer", example=80),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingAndCertificateResult updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingAndCertificateResult"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該訓練及證照成績已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingAndCertificateResult with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/trainingAndCertificateResults/{id}",
     *     summary="修改訓練及證照成績",
     *     description="修改訓練及證照成績",
     *     tags={"TrainingAndCertificateResults"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練及證照成績ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改訓練及證照成績資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"c_id", "s_id", "academic_score", "practical_score"},
     *                 @OA\Property(property="c_id", type="integer", example=1),
     *                 @OA\Property(property="s_id", type="string", example="Y2023-1001"),
     *                 @OA\Property(property="academic_score", type="integer", example=80),
     *                 @OA\Property(property="practical_score", type="integer", example=80),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingAndCertificateResult updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingAndCertificateResult"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該訓練及證照成績已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingAndCertificateResult with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingAndCertificateResult not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function set(Request $request)
    {
        $id = $request->route('id');
        $validatedData = $request->validate([
            'c_id' => 'required|integer',
            's_id' => 'required|string',
            'academic_score' => 'required|integer',
            'practical_score' => 'required|integer',
        ]);
        $result = $this->trainingAndCertificateResultsService->set($validatedData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/trainingAndCertificateResults{id}",
     *     summary="根據ID刪除訓練及證照成績",
     *     description="根據提供的ID刪除訓練及證照成績",
     *     tags={"TrainingAndCertificateResults"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練及證照成績ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingAndCertificateResult delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingAndCertificateResult ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->trainingAndCertificateResultsService->delete($id);
        return response()->json($result);
    }
}
