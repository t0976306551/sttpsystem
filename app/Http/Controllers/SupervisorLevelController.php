<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SupervisorLevelService;

class SupervisorLevelController extends Controller
{
    protected $supervisorLevelService;

    public function __construct(SupervisorLevelService $supervisorLevelService)
    {
        $this->supervisorLevelService = $supervisorLevelService;
    }

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->supervisorLevelService->get($id);
        return response()->json($result);
    }

    public function set(Request $request)
    {
        $id = $request->route('id');
        $validatedData = $request->validate([
            'sl_id' => 'required|string|max:10',
            'sl_name' => 'required|string|max:50',
        ]);
        $result = $this->supervisorLevelService->set($validatedData, $id);
        return response()->json($result);
    }

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->supervisorLevelService->delete($id);
        return response()->json($result);
    }
}
