<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClassScheduleService;

class ClassScheduleController extends Controller
{
    protected $classScheduleService;

    /**
     * @OA\Schema(
     *     schema="ClassSchedule",
     *     @OA\Property(property="cs_id", type="integer"),
     *     @OA\Property(property="training_year", type="string"),
     *     @OA\Property(property="term", type="integer"),
     *     @OA\Property(property="ch_name", type="string"),
     *     @OA\Property(property="contacts_name", type="string"),
     *     @OA\Property(property="contacts_phone", type="string"),
     *     @OA\Property(property="start_date", type="date"),
     *     @OA\Property(property="end_date", type="date"),
     *     @OA\Property(property="plan_trainees_number", type="integer"),
     *     @OA\Property(property="deadline_date", type="date"),
     *     @OA\Property(property="budget", type="integer"),
     *     @OA\Property(property="remarks", type="string"),
     *     @OA\Property(property="o_id", type="integer"),
     *     @OA\Property(property="tu_id", type="integer"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(ClassScheduleService $classScheduleService)
    {
        $this->classScheduleService = $classScheduleService;
    }

    /**
     * @OA\Get(
     *     path="/api/classSchedule/{id}",
     *     summary="根據ID取得班次資訊",
     *     description="根據提供的ID取得班次資訊",
     *     tags={"ClassSchedules"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班次ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得班次資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/ClassSchedule"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班次發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/classSchedule",
     *     summary="返回所有班次資訊",
     *     description="返回所有班次的資訊",
     *     tags={"ClassSchedules"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有班次資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/ClassSchedule")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋班次發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classScheduleService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/classSchedule",
     *     summary="創建班次",
     *     description="創建班次",
     *     tags={"ClassSchedules"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建班次",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"training_year", "term", "ch_name", "contacts_name", "contacts_phone", "start_date", "end_date", "plan_trainees_number", "deadline_date", "budget", "remarks", "o_id", "tu_id"},
     *                 @OA\Property(property="training_year", type="string", example="2023-11-11"),
     *                 @OA\Property(property="term", type="integer", example=1),
     *                 @OA\Property(property="ch_name", type="string", example="王XX"),
     *                 @OA\Property(property="contacts_name", type="string", example="王XX"),
     *                 @OA\Property(property="contacts_phone", type="string", example="0900000000"),
     *                 @OA\Property(property="start_date", type="date", example="2023-11-01"),
     *                 @OA\Property(property="end_date", type="date", example="2023-12-31"),
     *                 @OA\Property(property="plan_trainees_number", type="integer", example=50),
     *                 @OA\Property(property="deadline_date", type="date", example="2023-10-15"),
     *                 @OA\Property(property="budget", type="integer", example=100000),
     *                 @OA\Property(property="remarks", type="string", example="無備註"),
     *                 @OA\Property(property="o_id", type="integer", example="o_id"),
     *                 @OA\Property(property="tu_id", type="integer", example="tu_id"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="ClassSchedule updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/ClassSchedule"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該班次已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassSchedule with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/classSchedule/{id}",
     *     summary="修改班次",
     *     description="修改班次",
     *     tags={"ClassSchedules"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班次ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改班次資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"training_year", "term", "ch_name", "contacts_name", "contacts_phone", "start_date", "end_date", "plan_trainees_number", "deadline_date", "budget", "remarks", "o_id", "tu_id"},
     *                 @OA\Property(property="training_year", type="string", example="2023-11-11"),
     *                 @OA\Property(property="term", type="integer", example=1),
     *                 @OA\Property(property="ch_name", type="string", example="王XX"),
     *                 @OA\Property(property="contacts_name", type="string", example="王XX"),
     *                 @OA\Property(property="contacts_phone", type="string", example="0900000000"),
     *                 @OA\Property(property="start_date", type="date", example="2023-11-01"),
     *                 @OA\Property(property="end_date", type="date", example="2023-12-31"),
     *                 @OA\Property(property="plan_trainees_number", type="integer", example=50),
     *                 @OA\Property(property="deadline_date", type="date", example="2023-10-15"),
     *                 @OA\Property(property="budget", type="integer", example=100000),
     *                 @OA\Property(property="remarks", type="string", example="無備註"),
     *                 @OA\Property(property="o_id", type="integer", example="o_id"),
     *                 @OA\Property(property="tu_id", type="integer", example="tu_id"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="ClassSchedule updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/ClassSchedule"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該班次已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassSchedule with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassSchedule not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function set(Request $request)
    {
        $id = $request->route('id');
        $validateData = $request->validate([
            'training_year' => 'required|string|max:50',
            'term' => 'required|integer',
            'ch_name' => 'required|string|max:50',
            'contacts_name' => 'required|string|max:50',
            'contacts_phone' => 'required|string|max:50',
            'start_date' => 'required|date|max:50',
            'end_date' => 'required|date|max:50',
            'plan_trainees_number' => 'required|integer',
            'deadline_date' => 'required|date|max:50',
            'budget' => 'required|integer',
            'remarks' => 'required|string|max:255',
            'o_id' => 'required|integer',
            'tu_id' => 'required|integer',
        ]);
        $result = $this->classScheduleService->set($validateData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/classSchedule/{id}",
     *     summary="根據ID刪除班次",
     *     description="根據提供的ID刪除班次",
     *     tags={"ClassSchedules"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="班次ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="ClassSchedule delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="ClassSchedule ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->classScheduleService->delete($id);
        return response()->json($result);
    }
}
