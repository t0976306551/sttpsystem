<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\StaffForeignLanguageScoresService;

class StaffForeignLanguageScoresController extends Controller
{
    protected $staffForeignLanguageScoresService;

    /**
     * @OA\Schema(
     *     schema="StaffForeignLanguageScore",
     *     @OA\Property(property="sfls_id", type="integer"),
     *     @OA\Property(property="s_id", type="string"),
     *     @OA\Property(property="fl_id", type="integer"),
     *     @OA\Property(property="test_date", type="date"),
     *     @OA\Property(property="pass_score", type="integer"),
     *     @OA\Property(property="listening_score", type="integer"),
     *     @OA\Property(property="grammar_score", type="integer"),
     *     @OA\Property(property="vocabulary_score", type="integer"),
     *     @OA\Property(property="session_score", type="integer"),
     *     @OA\Property(property="toefl_score", type="integer"),
     *     @OA\Property(property="essay_score", type="integer"),
     *     @OA\Property(property="total_score", type="integer"),
     *     @OA\Property(property="is_passed", type="boolean"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */
    public function __construct(StaffForeignLanguageScoresService $staffForeignLanguageScoresService)
    {
        $this->staffForeignLanguageScoresService = $staffForeignLanguageScoresService;
    }

    /**
     * @OA\Get(
     *     path="/api/staffForeignLanguageScore/{id}",
     *     summary="根據ID取得員工外語成績資訊",
     *     description="根據提供的ID取得員工外語成績資訊",
     *     tags={"StaffForeignLanguageScores"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="員工外語成績ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得員工外語成績資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/StaffForeignLanguageScore"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋員工外語成績發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/staffForeignLanguageScore",
     *     summary="返回所有員工外語成績資訊",
     *     description="返回所有員工外語成績的資訊",
     *     tags={"StaffForeignLanguageScores"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有員工外語成績資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/StaffForeignLanguageScore")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋員工外語成績發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->staffForeignLanguageScoresService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/staffForeignLanguageScore/getByStaffId/{id}",
     *     summary="根據職員ID取得員工外語成績資訊",
     *     description="根據提供的職員ID取得員工外語成績資訊",
     *     tags={"StaffForeignLanguageScores"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="職員ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得員工外語成績資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *               @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/StaffForeignLanguageScore")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋員工外語成績發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     */
    public function getByStaffId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->staffForeignLanguageScoresService->getByStaffId($id);
        return response()->json($result);
    }

    /**
     * @OA\Get(
     *     path="/api/staffForeignLanguageScore/getByForeignLanguageId/{id}",
     *     summary="根據外語ID取得員工外語成績資訊",
     *     description="根據提供的外語ID取得員工外語成績資訊",
     *     tags={"StaffForeignLanguageScores"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="外語ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得員工外語成績資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *               @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/StaffForeignLanguageScore")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋員工外語成績發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     */
    public function getByForeignLanguageId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->staffForeignLanguageScoresService->getByForeignLanguageId($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/staffForeignLanguageScore",
     *     summary="創建員工外語成績",
     *     description="創建員工外語成績",
     *     tags={"StaffForeignLanguageScores"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建員工外語成績",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"s_id", "fl_id", "test_date", "pass_score", "listening_score", "grammar_score", "vocabulary_score", "session_score", "toefl_score", "essay_score"},
     *                 @OA\Property(property="s_id", type="string", example="Y2023-1001"),
     *                 @OA\Property(property="fl_id", type="integer", example=1),
     *                 @OA\Property(property="test_date", type="date", example="2024-01-01"),
     *                 @OA\Property(property="pass_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="listening_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="grammar_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="vocabulary_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="session_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="toefl_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="essay_score", type="integer", example=100, nullable=true),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="StaffForeignLanguageScore updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/StaffForeignLanguageScore"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該員工外語成績已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="StaffForeignLanguageScore with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/staffForeignLanguageScore/{id}",
     *     summary="修改員工外語成績",
     *     description="修改員工外語成績",
     *     tags={"StaffForeignLanguageScores"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="員工外語成績ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改員工外語成績資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"s_id", "fl_id", "test_date", "pass_score", "listening_score", "grammar_score", "vocabulary_score", "session_score", "toefl_score", "essay_score"},
     *                 @OA\Property(property="s_id", type="string", example="Y2023-1001"),
     *                 @OA\Property(property="fl_id", type="integer", example=1),
     *                 @OA\Property(property="test_date", type="date", example="2024-01-01"),
     *                 @OA\Property(property="pass_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="listening_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="grammar_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="vocabulary_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="session_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="toefl_score", type="integer", example=100, nullable=true),
     *                 @OA\Property(property="essay_score", type="integer", example=100, nullable=true),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="StaffForeignLanguageScore updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/StaffForeignLanguageScore"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該員工外語成績已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="StaffForeignLanguageScore with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="StaffForeignLanguageScore not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function set(Request $request)
    {
        $id = $request->route('id');
        $validateData = $request->validate([
            's_id' => 'required|string',
            'fl_id' => 'required|integer',
            "test_date" => 'required|date',
            'pass_score' => 'required|integer|between:0,1000',
            'listening_score' => 'nullable|integer|between:0,1000',
            'grammar_score' => 'nullable|integer|between:0,1000',
            'vocabulary_score' => 'nullable|integer|between:0,1000',
            'session_score' => 'nullable|integer|between:0,1000',
            'toefl_score' => 'nullable|integer|between:0,1000',
            'essay_score' => 'nullable|integer|between:0,1000',
        ]);
        $result = $this->staffForeignLanguageScoresService->set($validateData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/staffForeignLanguageScore/{id}",
     *     summary="根據ID刪除員工外語成績",
     *     description="根據提供的ID刪除員工外語成績",
     *     tags={"StaffForeignLanguageScores"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="員工外語成績ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="StaffForeignLanguageScore delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="StaffForeignLanguageScore ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->staffForeignLanguageScoresService->delete($id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/staffForeignLanguageScore/deleteByStaffId/{id}",
     *     summary="根據職員ID刪除員工外語成績",
     *     description="根據提供的職員ID刪除員工外語成績",
     *     tags={"StaffForeignLanguageScores"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="職員ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="StaffForeignLanguageScore delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="StaffForeignLanguageScore ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function deleteByStaffId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->staffForeignLanguageScoresService->deleteByStaffId($id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/staffForeignLanguageScore/deleteByForeignLanguageId/{id}",
     *     summary="根據外語ID刪除員工外語成績",
     *     description="根據提供的外語ID刪除員工外語成績",
     *     tags={"StaffForeignLanguageScores"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="外語ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="StaffForeignLanguageScore delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="StaffForeignLanguageScore ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function deleteByForeignLanguageId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->staffForeignLanguageScoresService->deleteByForeignLanguageId($id);
        return response()->json($result);
    }
}
