<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UnitService;
use Illuminate\Validation\ValidationException;

class UnitController extends Controller
{
    protected $unitService;

    /**
     * @OA\Schema(
     *     schema="Unit",
     *     @OA\Property(property="u_id", type="string"),
     *     @OA\Property(property="u_name", type="string"),
     *     @OA\Property(property="d_id", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(UnitService $unitService)
    {
        $this->unitService = $unitService;
    }

    /**
     * @OA\Get(
     *     path="/api/unit/{id}",
     *     summary="根據ID取得單位資訊",
     *     description="根據提供的ID取得單位資訊",
     *     tags={"Units"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="單位ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得單位資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Unit"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋單位發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/unit",
     *     summary="返回所有單位資訊",
     *     description="返回所有單位的資訊",
     *     tags={"Units"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有單位資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/Unit")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋單位發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->unitService->get($id);
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Get(
     *     path="/api/unit/department/{id}",
     *     summary="根據部門ID取得單位資訊",
     *     description="根據提供的部門ID取得單位資訊",
     *     tags={"Units"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="部門ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得單位資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/Unit"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋單位發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function getUnitByDepartmentId(Request $request)
    {
        $id = $request->route('id');
        $result = $this->unitService->getUnitByDepartmentId($id);
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Post(
     *     path="/api/unit",
     *     summary="創建單位",
     *     description="創建單位",
     *     tags={"Units"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建單位",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"u_id", "u_name", "d_id"},
     *                 @OA\Property(property="u_id", type="string", example="u_id(限制兩個字英文字)"),
     *                 @OA\Property(property="u_name", type="string", example="u_name"),
     *                 @OA\Property(property="d_id", type="string", example="d_id(部門代號)"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Unit updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Unit"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該單位已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Unit with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     *  * @OA\PUT(
     *     path="/api/unit/{id}",
     *     summary="修改單位",
     *     description="修改單位",
     *     tags={"Units"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="單位的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改單位資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"u_name", "d_id"},
     *                 @OA\Property(property="u_name", type="string", example="u_name"),
     *                 @OA\Property(property="d_id", type="string", example="d_id(部門代號)"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Unit updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/Unit"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該單位已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Unit with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Unit not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function set(Request $request)
    {
        try {
            $id = $request->route('id');
            $validatedData = $request->validate([
                'u_id' => 'required|string|max:2',
                'u_name' => 'required|string|max:100',
                'd_id' => 'required|string|max:2'
            ]);
            $result = $this->unitService->set($validatedData, $id);
            return response()->json($result, $result['status']);
        } catch (ValidationException $e) {
            return response()->json(['success' => false, 'errors' => $e->validator->errors()], 422);
        }
    }

    /**
     * @OA\Delete(
     *     path="/api/unit/{id}",
     *     summary="根據ID刪除單位",
     *     description="根據提供的ID刪除單位",
     *     tags={"Units"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="單位的ID（必填）",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="Unit delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="Unit ID is required to delete a CourseShare resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->unitService->delete($id);
        return response()->json($result, $result['status']);
    }
}
