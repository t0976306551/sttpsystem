<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TrainingCourseService;

class TrainingCourseController extends Controller
{
    protected $trainingCourseService;

    /**
     * @OA\Schema(
     *     schema="TrainingCourse",
     *     @OA\Property(property="tc_id", type="integer"),
     *     @OA\Property(property="tc_name", type="string"),
     *     @OA\Property(property="hours", type="integer"),
     *     @OA\Property(property="t_name", type="string"),
     *     @OA\Property(property="t_unit", type="string"),
     *     @OA\Property(property="t_position", type="string"),
     *     @OA\Property(property="created_at", type="datetime"),
     *     @OA\Property(property="updated_at", type="datetime"),
     * )
     */

    public function __construct(TrainingCourseService $trainingCourseService)
    {
        $this->trainingCourseService = $trainingCourseService;
    }

    /**
     * @OA\Get(
     *     path="/api/trainingCourse/{id}",
     *     summary="根據ID取得訓練課程資訊",
     *     description="根據提供的ID取得訓練課程資訊",
     *     tags={"TrainingCourses"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練課程ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="成功取得訓練課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingCourse"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋訓練課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\Get(
     *     path="/api/trainingCourse",
     *     summary="返回所有訓練課程資訊",
     *     description="返回所有訓練課程的資訊",
     *     tags={"TrainingCourses"},
     *     @OA\Response(
     *         response=200,
     *         description="成功取得所有訓練課程資訊",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/TrainingCourse")),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="找尋訓練課程發生錯誤",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=false),
     *              @OA\Property(property="message", type="string", example="string"),
     *              @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */
    public function get(Request $request)
    {
        $id = $request->route('id');
        $result = $this->trainingCourseService->get($id);
        return response()->json($result);
    }

    /**
     * @OA\Post(
     *     path="/api/trainingCourse",
     *     summary="創建訓練課程",
     *     description="創建訓練課程",
     *     tags={"TrainingCourses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="創建訓練課程",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"c_name", "hours", "t_name", "t_unit"},
     *                 @OA\Property(property="c_name", type="string", example="c_name"),
     *                 @OA\Property(property="hours", type="integer", example="hours"),
     *                 @OA\Property(property="t_name", type="string", example="t_name"),
     *                 @OA\Property(property="t_unit", type="string", example="t_unit"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="創建成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingCourse updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingCourse"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="創建失敗(該訓練課程已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingCourse with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="創建發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     *
     * @OA\PUT(
     *     path="/api/trainingCourse/{id}",
     *     summary="修改訓練課程",
     *     description="修改訓練課程",
     *     tags={"TrainingCourses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練課程ID（必填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="修改訓練課程資訊",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"tc_name", "hours", "t_name", "t_unit", "t_position"},
     *                 @OA\Property(property="tc_name", type="string", example="c_name"),
     *                 @OA\Property(property="hours", type="integer", example="hours"),
     *                 @OA\Property(property="t_name", type="string", example="t_name"),
     *                 @OA\Property(property="t_unit", type="string", example="t_unit"),
     *                 @OA\Property(property="t_position", type="string", example="t_unit"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="修改成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingCourse updated or created successfully"),
     *              @OA\Property(property="data", ref="#/components/schemas/TrainingCourse"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="修改失敗(該訓練課程已存在)",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingCourse with the same name already exists"),
     *             @OA\Property(property="status", type="integer", example=409),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingCourse not found with ID: {id}"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function set(Request $request)
    {
        $id = $request->route('id');
        $validateData = $request->validate([
            'tc_name' => 'required|string|max:50',
            'hours' => 'required|integer|max:10',
            't_name' => 'required|string|max:50',
            't_unit' => 'required|string|max:50',
            't_position' => 'required|string|max:50'
        ]);
        $result = $this->trainingCourseService->set($validateData, $id);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *     path="/api/trainingCourse/{id}",
     *     summary="根據ID刪除訓練課程",
     *     description="根據提供的ID刪除訓練課程",
     *     tags={"TrainingCourses"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="訓練課程ID（選填）",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="刪除成功",
     *         @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example=true),
     *              @OA\Property(property="message", type="string", example="TrainingCourse delete successfully"),
     *              @OA\Property(property="status", type="integer", example=200),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="找不到對應的資源{id}",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="TrainingCourse ID is required to delete resource"),
     *             @OA\Property(property="status", type="integer", example=404),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="修改發生錯誤",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", example=false),
     *             @OA\Property(property="message", type="string", example="error message"),
     *             @OA\Property(property="status", type="integer", example=500),
     *         )
     *     )
     * )
     */

    public function delete(Request $request)
    {
        $id = $request->route('id');
        $result = $this->trainingCourseService->delete($id);
        return response()->json($result);
    }
}
