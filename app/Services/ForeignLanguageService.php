<?php

namespace App\Services;

use App\Models\ForeignLanguage;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ForeignLanguageService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $foreignLanguage = ForeignLanguage::find($id);
                if (!$foreignLanguage) {
                    throw new ModelNotFoundException("ForeignLanguage not found with ID: $id");
                }
                return BaseService::responseSet(true, 'ForeignLanguage get successfully', 200, $foreignLanguage);
            }
            $foreignLanguages = ForeignLanguage::all();
            return BaseService::responseSet(true, 'ForeignLanguages get successfully', 200, $foreignLanguages);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while ForeignLanguage',
                'message' => $e->getMessage(),
                'status' => 500,
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $foreignLanguage = ForeignLanguage::find($id);
                if (!$foreignLanguage) {
                    throw new ModelNotFoundException("ForeignLanguage not found with ID: $id");
                };
            }

            $checkForeignLanguageName = ForeignLanguage::where('fl_name', $data['fl_name'])->first();
            if ($checkForeignLanguageName) {
                return BaseService::responseSet(false, 'ForeignLanguage with the same name already exists', 409);
            }

            $setForeignLanguage = ForeignLanguage::updateOrCreate(
                ['fl_id' => $id],
                [
                    'fl_name' => $data['fl_name']
                ]
            );
            return BaseService::responseSet(true, 'ForeignLanguage updated or created successfully', 200, $setForeignLanguage);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating ForeignLanguages',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("ForeignLanguage ID is required to delete a User resource");
            }
            $foreignLanguage = ForeignLanguage::find($id);
            if (!$foreignLanguage) {
                throw new ModelNotFoundException("ForeignLanguage not found with ID: $id");
            }
            $foreignLanguage->delete();
            return BaseService::responseSet(true, 'ForeignLanguage delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting ForeignLanguage',
                'message' => $e->getMessage(),
            ];
        }
    }
}
