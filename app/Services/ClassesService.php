<?php

namespace App\Services;

use App\Models\Classes;
use App\Models\ClassSchedule;
use App\Models\Certificate;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClassesService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $classes = Classes::find($id);
                if (!$classes) {
                    throw new ModelNotFoundException("Classes not found with ID: $id");
                }
                return BaseService::responseSet(true, 'Classes get successfully', 200, $classes);
            }
            $classes = Classes::all();
            return BaseService::responseSet(true, 'Classes get successfully', 200, $classes);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Classes',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getClassesByClassScheduleId($id = null)
    {
        try {
            if (!is_null($id)) {
                $classes = Classes::where('cs_id', $id)->get();
                if (!$classes) {
                    throw new ModelNotFoundException("Classes not found with classSchedule ID: $id");
                }
                return BaseService::responseSet(true, 'Classes get successfully', 200, $classes);
            }
            throw new ModelNotFoundException("Classes classScheduleId is required to get a Classes resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Classes',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getClassesByCertificateId($id = null)
    {
        try {
            if (!is_null($id)) {
                $classes = Classes::where('cer_id', $id)->get();
                if (!$classes) {
                    throw new ModelNotFoundException("Classes not found with certificate ID: $id");
                }
                return BaseService::responseSet(true, 'Classes get successfully', 200, $classes);
            }
            throw new ModelNotFoundException("Classes certificateId is required to get a Classes resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Classes',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $classes = Classes::find($id);
                if (!$classes) {
                    throw new ModelNotFoundException("Classes not found with ID: $id");
                }
            }
            $checkClassSchedule = ClassSchedule::find($data['cs_id']);
            if (!$checkClassSchedule) {
                throw new ModelNotFoundException("ClassSchedule not found with ID: " . $data['cs_id']);
            }
            $checkCertificate =  Certificate::find($data['cer_id']);
            if (!$checkCertificate) {
                throw new ModelNotFoundException("Certificate not found with ID: " . $data['cer_id']);
            }
            $checkClasses = Classes::where('cs_id', $data['cs_id'])->where('cer_id', $data['cer_id'])->get();
            if ($checkClasses) {
                return BaseService::responseSet(false, 'Classes with the cs_id and cer_id already exists', 409);
            }
            $setClasses = Classes::updateOrCreate(
                ['c_id', $id],
                [
                    'cs_id' => $data['cs_id'],
                    'cer_id' => $data['cer_id'],
                    'c_name' => $data['c_name'],
                    'training_day' => $data['training_day'],
                    'training_hours' => $data['training_hours'],
                    'class_number' => $data['class_number'],
                    'academic_standards' => $data['academic_standards'],
                    'practical_standards' => $data['practical_standards'],
                    'traing_active_years' => $data['traing_active_years'],
                    'remarks' => $data['remarks']
                ]
            );
            return BaseService::responseSet(true, 'Classes updated or created successfully', 200, $setClasses);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while Classes',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($data, $id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Classes ID is required to delete a User resource");
            }
            $classes = Classes::find($id);
            if (!$classes) {
                throw new ModelNotFoundException("Classes not found with ID: $id");
            }
            $classes->delete();
            return BaseService::responseSet(true, 'Classes delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Classes',
                'message' => $e->getMessage(),
            ];
        }
    }
}
