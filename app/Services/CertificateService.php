<?php

namespace App\Services;

use App\Models\Certificate;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CertificateService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $certificate = Certificate::find($id);
                if (!$certificate) {
                    throw new ModelNotFoundException("Certificate not found with ID: $id");
                }
                return BaseService::responseSet(true, 'Certificate get successfully', 200, $certificate);
            }
            $certificates = Certificate::all();
            return BaseService::responseSet(true, 'Certificate get successfully', 200, $certificates);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Certificate',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                if (!Certificate::find($id)) {
                    throw new ModelNotFoundException("Course not found with ID: $id");
                }
            }
            $checkCertificateName = Certificate::where('cer_name', $data['cer_name'])->first();
            if ($checkCertificateName) {
                return BaseService::responseSet(false, 'Certificate with the same name already exists', 409);
            }
            $setCertificate = Certificate::updateOrCreate(
                ['cer_id' => $id],
                [
                    'cer_name' => $data['cer_name'],
                    'effective_years' => $data['effective_years'],
                    'cer_authority' => $data['cer_authority'],
                ]
            );
            return BaseService::responseSet(true, 'Certificate updated or created successfully', 200, $setCertificate);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Certificate',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Certificate ID is required to delete a resource");
            }
            $certificate = Certificate::find($id);
            if (!$certificate) {
                throw new ModelNotFoundException("Certificate not found with ID: $id");
            }
            $certificate->delete();
            return BaseService::responseSet(true, 'Certificate delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Certificate',
                'message' => $e->getMessage(),
            ];
        }
    }
}
