<?php

namespace App\Services;

use App\Models\SttpLearnAndExperience;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SttpLearnAndExperienceService
{
    public static function get($id = null, $data)
    {
        try {
            if (!is_null($id)) {
                $sle = SttpLearnAndExperience::with('sttp')->find($id);
                if (!$sle) {
                    throw new ModelNotFoundException("Sttp learn and experience not found with ID: $id");
                }
                return [
                    'success' => true,
                    'message' => 'Sttp learn and experience get successfully',
                    'data' => $sle,
                ];
            }
            $where = [];
            if (!is_null($data['sttp_id']) && $data['sttp_id'] != '') {
                $where['sttp_id'] = $data['sttp_id'];
            }
            $sles = SttpLearnAndExperience::with('sttp')->where($where)->get();
            return [
                'success' => true,
                'message' => 'Sttp learn and experience get successfully',
                'data' => $sles,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Sttp learn and experience',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $sle = SttpLearnAndExperience::find($id);
                if (!$sle) {
                    throw new ModelNotFoundException("Sttp learn and experience not found with ID: $id");
                }
            }
            if (is_null($id)) {
                $checksle = SttpLearnAndExperience::where('sttp_id', $data['sttp_id'])->first();
                if ($checksle) {
                    return [
                        'success' => false,
                        'message' => 'Sttp learn and experience with the same name already exists',
                    ];
                }
            }
            $setSle = SttpLearnAndExperience::updateOrCreate(
                ['sle_id' => $id, 'sttp_id' => $data['sttp_id']],
                [
                    'sttp_id' => $data['sttp_id'],
                    'education' => $data['education'],
                    'experience' => $data['experience'],
                ]
            );
            return [
                'success' => true,
                'message' => 'Sttp learn and experience updated or created successfully',
                'data' => $setSle,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while setting Sttp learn and experience',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Sttp learn and experience ID is required to delete a Sttp resource");
            }
            $sle = SttpLearnAndExperience::find($id);
            if (!$sle) {
                throw new ModelNotFoundException("Sttp learn and experience not found with ID: $id");
            }
            $sle->delete();
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Sttp learn and experience',
                'message' => $e->getMessage(),
            ];
        }
    }
}
