<?php

namespace App\Services;

use App\Models\Promotion;
use App\Models\Sttp;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PromotionService
{
    public static function getPromotionBySttpId($sttp_id = null)
    {
        try {
            if (!is_null($sttp_id)) {
                $promotion = Promotion::where('sttp_id', $sttp_id)->get();
                if (!$promotion) {
                    throw new ModelNotFoundException("Promotion not found with Sttp ID: $sttp_id");
                }
                return BaseService::responseSet(true, 'Promotion get successfully', 200, $promotion);
            }
            throw new ModelNotFoundException("Promotion Sttp ID is required to get a resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Promotion',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function createPromotion($data = [])
    {
        try {
            if (!isset($data['sttp_id']) || $data['sttp_id'] = null) {
                throw new ModelNotFoundException("Promotion Sttp ID is required to get a resource");
            }

            if (!isset($data['pr_sttp']) || empty($data['pr_sttp'])) {
                throw new ModelNotFoundException("Promotion pr_sttp ID is required to get a resource");
            }

            if (!Sttp::find($data['sttp_id'])) {
                throw new ModelNotFoundException("Sttp not found with sttp_id");
            }

            foreach ($data['pr_sttp'] as $pr_sttp) {
                if ($data['sttp_id'] == $pr_sttp) {
                    return BaseService::responseSet(false, 'Sttp_id cannot be repeated with pr_sttp', 404);
                }
                if (!Sttp::find($pr_sttp)) {
                    throw new ModelNotFoundException("Sttp not found with pr_sttp: $pr_sttp");
                }
            }
            $promotionArray = [];
            foreach ($data['pr_sttp'] as $pr_sttp) {
                $result = Promotion::create(
                    [
                        'sttp_id' => $data['sttp_id'],
                        'pr_sttp' => $pr_sttp
                    ]
                );
                $promotionArray[] = $result;
            }
            return BaseService::responseSet(true, 'Promotion created successfully', 200, $promotionArray);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while creating Promotion',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deletePromotion($id = null, $data)
    {
        try {
            if (!is_null($id)) {
                $promotion = Promotion::find($id);
                if ($promotion) {
                    $promotion->delete();
                    return BaseService::responseSet(true, 'Promotion delete successfully', 200);
                }
                throw new ModelNotFoundException("Promotion not found with ID: $id");
            }

            if (!is_null($data['sttp_id'])) {
                if (!is_null($data['pr_sttp'])) {
                    $promotion = Promotion::where('sttp_id', $data['sttp_id'])
                        ->where('pr_sttp', $data['sttp_id'])->first();
                    if ($promotion) {
                        $promotion->delete();
                        return BaseService::responseSet(true, 'Promotion delete successfully', 200);
                    }
                    throw new ModelNotFoundException("Promotion not found with sttp_id and pr_sttp");
                }
                Promotion::where('sttp_id', $data['sttp_id'])->delete();
                return BaseService::responseSet(true, 'Promotion delete successfully by sttp_id', 200);
            }

            if (!is_null($data['pr_sttp'])) {
                Promotion::where('pr_sttp', $data['pr_sttp'])->delete();
                return BaseService::responseSet(true, 'Promotion delete successfully by pr_sttp', 200);
            }

            throw new ModelNotFoundException("Promotion id or sttp_id or pr_sttp need one of them to delete a Promotion resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Promotion',
                'message' => $e->getMessage(),
            ];
        }
    }
}
