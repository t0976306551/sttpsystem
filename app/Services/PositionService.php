<?php

namespace App\Services;

use App\Models\Position;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\CourseShare;

class PositionService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $position = Position::find($id);
                if (!$position) {
                    throw new ModelNotFoundException("Position not found with ID: $id");
                }
                return [
                    'success' => true,
                    'message' => 'Position get successfully',
                    'data' => $position,
                    'status' => 200
                ];
            }
            $position = Position::all();
            return [
                'success' => true,
                'message' => 'Position get successfully',
                'data' => $position,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Position',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $position = Position::find($id);
                if (!$position) {
                    throw new ModelNotFoundException("Position not found with ID: $id");
                }

                $data["p_id"] = $id;
            }
            if (is_null($id)) {
                if (!isset($data['p_id'])) {
                    return [
                        'success' => false,
                        'message' => 'Position id cannot be empty',
                        'status' => 404
                    ];
                }

                $id = $data["p_id"];
            }

            $checksetPositionName = Position::where('p_name', $data['p_name'])->first();
            if ($checksetPositionName) {
                return [
                    'success' => false,
                    'message' => 'Position with the same name already exists',
                    'status' => 409
                ];
            }
            $setPosition = Position::updateOrCreate(
                ['p_id' => $id],
                [
                    'p_id' => $data["p_id"],
                    'p_name' => $data['p_name'],
                ]
            );
            return [
                'success' => true,
                'message' => 'Position updated or created successfully',
                'data' => $setPosition,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Position',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("CourseShare ID is required to delete a CourseShare resource");
            }
            $position = Position::find($id);
            if (!$position) {
                throw new ModelNotFoundException("Position not found with ID: $id");
            }
            $position->delete();
            return [
                'success' => true,
                'message' => 'Position delete successfully',
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Position',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }
}
