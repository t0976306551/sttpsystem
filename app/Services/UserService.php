<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public static function get($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $user = User::with('role.rolePermission.permission')->find($id);
                if (!$user) {
                    throw new ModelNotFoundException("User not found with ID: $id");
                }
                return BaseService::responseSet(true, 'User get successfully', 200, $user);
            }
            $where = [];
            if (!is_null($data['r_id']) && $data['r_id'] != '') {
                $where['r_id'] = $data['r_id'];
            }
            $users = User::with('role.rolePermission.permission')->where($where)->get();
            return BaseService::responseSet(true, 'User get successfully', 200, $users);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting User',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $user = User::find($id);
                if (!$user) {
                    throw new ModelNotFoundException("User not found with ID: $id");
                };
            }

            if (is_null($id)) {
                $checkUsername = User::where('username', $data['username'])->first();
                if ($checkUsername) {
                    return BaseService::responseSet(false, 'User with the same username already exists', 409);
                }
            }

            $setUser = User::updateOrCreate(
                ['id' => $id, 'username' => $data['username']],
                [
                    'username' => $data['username'],
                    'password' => Hash::make($data['password']),
                    'name' => $data['name'],
                    'r_id' => $data['r_id']
                ]
            );
            return BaseService::responseSet(true, 'User updated or created successfully', 200, $setUser);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating User',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function updatePassword($data, $id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("User ID is required to update password a User resource");
            }
            $user = User::find($id);
            if (!$user) {
                throw new ModelNotFoundException("User not found with ID: $id");
            }
            if (!is_null($data['newPassword']) && $data['newPassword'] != '') {
                if (Hash::check($data['oldPassword'], $user->password)) {
                    $user->update(['password' => Hash::make($data['newPassword'])]);
                    return BaseService::responseSet(true, 'User password update successfully', 200);
                }
                return BaseService::responseSet(false, 'User Incorrect Password', 422);
            }
            return BaseService::responseSet(false, 'New password cannot be empty', 422);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating User password',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("User ID is required to delete a User resource");
            }
            $user = User::find($id);
            if (!$user) {
                throw new ModelNotFoundException("User not found with ID: $id");
            }
            $user->delete();
            return BaseService::responseSet(true, 'User delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting User',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }
}
