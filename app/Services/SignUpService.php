<?php

namespace App\Services;

use App\Models\SignUp;
use App\Models\Classes;
use App\Models\Staff;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SignUpService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $signUp = SignUp::find($id);
                if (!$signUp) {
                    throw new ModelNotFoundException("Sign up not found with cd_id: $id");
                }
                return BaseService::responseSet(true, 'Sign up get successfully', 200, $signUp);
            }
            $signUps = SignUp::all();
            return BaseService::responseSet(true, 'Sign up get successfully', 200, $signUps);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Sign up',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getSignUpByClassesId($id = null)
    {
        try {
            if (!is_null($id)) {
                $signUp = SignUp::where('c_id', $id)->get();
                if (!$signUp) {
                    throw new ModelNotFoundException("Sign up not found with c_id: $id");
                }
                return BaseService::responseSet(true, 'Sign up get successfully', 200, $signUp);
            }
            throw new ModelNotFoundException("Sign up ClassesId is required to delete a Sign up resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Sign up by ClassesId',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getSignUpByStaffId($id = null)
    {
        try {
            if (!is_null($id)) {
                $signUp = SignUp::where('s_id', $id)->get();
                if (!$signUp) {
                    throw new ModelNotFoundException("Sign up not found with StaffId: $id");
                }
                return BaseService::responseSet(true, 'Sign up get successfully', 200, $signUp);
            }
            throw new ModelNotFoundException("Sign up StaffId is required to get a Sign up resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Sign up by StaffId',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function createSignUp($data)
    {
        try {
            if (!Staff::find($data['s_id'])) {
                return  throw new ModelNotFoundException("Staff not found with ID: " . $data['s_id']);
            }
            if (!Classes::find($data['c_id'])) {
                return  throw new ModelNotFoundException("Classes not found with ID: " . $data['c_id']);
            }
            $checkSignUp = SignUp::where('c_id', $data['c_id'])->where('s_id', $data['s_id'])->get();
            if ($checkSignUp) {
                return BaseService::responseSet(false, 'Sign up with the c_id and s_id already exists', 409);
            }
            $createSignUp = SignUp::create($data);
            return BaseService::responseSet(true, 'SignUp created successfully', 200, $createSignUp);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while SignUp',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null, $data)
    {
        try {
            if (!is_null($id)) {
                $signUp = SignUp::find($id);
                if (!$signUp) {
                    throw new ModelNotFoundException("Sign up not found with ID: $signUp");
                }
                $signUp->delete();
            }
            if (!is_null($data['c_id'])) {
                $signUps = SignUp::where('c_id', $data['c_id']);
                if ($signUps) {
                    $signUps->delete();
                }
            }
            if (!is_null($data['s_id'])) {
                $signUps = SignUp::where('c_id', $data['c_id']);
                if ($signUps) {
                    $signUps->delete();
                }
            }
            return BaseService::responseSet(true, 'Sign up delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Sign up',
                'message' => $e->getMessage(),
            ];
        }
    }
}
