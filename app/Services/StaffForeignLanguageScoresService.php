<?php

namespace App\Services;

use App\Models\StaffForeignLanguageScores;
use App\Models\Staff;
use App\Models\ForeignLanguage;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StaffForeignLanguageScoresService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $staffForeignLanguageScore = StaffForeignLanguageScores::find($id);
                if (!$staffForeignLanguageScore) {
                    throw new ModelNotFoundException("StaffForeignLanguageScores not found with cd_id: $id");
                }
                return BaseService::responseSet(true, 'StaffForeignLanguageScores get successfully', 200, $staffForeignLanguageScore);
            }
            $staffForeignLanguageScore = StaffForeignLanguageScores::all();
            return BaseService::responseSet(true, 'StaffForeignLanguageScores get successfully', 200, $staffForeignLanguageScore);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting StaffForeignLanguageScores',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getByStaffId($id = null)
    {
        try {
            if (!is_null($id)) {
                $staffForeignLanguageScore = StaffForeignLanguageScores::where('s_id', $id)->get();
                if (!$staffForeignLanguageScore) {
                    throw new ModelNotFoundException("StaffForeignLanguageScores not found with Staff id: $id");
                }
                return BaseService::responseSet(true, 'StaffForeignLanguageScores get successfully', 200, $staffForeignLanguageScore);
            }
            $staffForeignLanguageScore = StaffForeignLanguageScores::all();
            return BaseService::responseSet(true, 'StaffForeignLanguageScores get successfully', 200, $staffForeignLanguageScore);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting StaffForeignLanguageScores',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getByForeignLanguageId($id = null)
    {
        try {
            if (!is_null($id)) {
                $staffForeignLanguageScore = StaffForeignLanguageScores::where('fl_id', $id)->get();
                if (!$staffForeignLanguageScore) {
                    throw new ModelNotFoundException("StaffForeignLanguageScores not found with ForeignLanguage id: $id");
                }
                return BaseService::responseSet(true, 'StaffForeignLanguageScores get successfully', 200, $staffForeignLanguageScore);
            }
            $staffForeignLanguageScore = StaffForeignLanguageScores::all();
            return BaseService::responseSet(true, 'StaffForeignLanguageScores get successfully', 200, $staffForeignLanguageScore);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting StaffForeignLanguageScores',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $staffForeignLanguageScore = StaffForeignLanguageScores::find($id);
                if (!$staffForeignLanguageScore) {
                    throw new ModelNotFoundException("StaffForeignLanguageScores not found with ID: $id");
                }

                $checkStaff = Staff::find($data['s_id']);
                $checkForeignLanguage = ForeignLanguage::find($data['fl_id']);

                if (!$checkStaff || !$checkForeignLanguage) {
                    throw new ModelNotFoundException("Staff or ForeignLanguage not found with ID: " . $data['s_id'] . ' or ' . $data['fl_id']);
                }

                $checkDuplicate = StaffForeignLanguageScores::where('s_id', $data['s_id'])->where('fl_id', $data['fl_id'])->get();
                if ($checkDuplicate) {
                    return BaseService::responseSet(false, 'StaffForeignLanguageScores with the same ForeignLanguage and staff already exists', 409);
                }

                $data['total_score'] = 0;
                $data['is_passed'] = false;
                $scoreArray = ['listening_score', 'grammar_score', 'vocabulary_score', 'session_score', 'toefl_score', 'essay_score'];

                foreach ($scoreArray as $score) {
                    if (is_null($data[$score]) && is_int($data[$score])) {
                        $data['total_score'] += $data[$score];
                    }
                }

                $data['is_passed'] = $data['total_score'] > $data['pass_score'] ? true : false;

                $setStaffForeignLanguageScore = StaffForeignLanguageScores::updateOrCreate(
                    ['sfls_id' => $id],
                    [
                        's_id' => $data['s_id'],
                        'fl_id' => $data['fl_id'],
                        'test_date' => $data['test_date'],
                        'pass_score' => $data['pass_score'],
                        'listening_score' => $data['listening_score'],
                        'grammar_score' => $data['grammar_score'],
                        'vocabulary_score' => $data['vocabulary_score'],
                        'session_score' => $data['session_score'],
                        'toefl_score' => $data['toefl_score'],
                        'essay_score' => $data['essay_score'],
                        'total_score' => $data['total_score'],
                        'is_passed' => $data['is_passed'],
                    ]
                );
                return BaseService::responseSet(true, 'StaffForeignLanguageScores updated or created successfully', 200, $setStaffForeignLanguageScore);
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while StaffForeignLanguageScores',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("StaffForeignLanguageScores ID is required to delete a StaffForeignLanguageScores resource");
            }
            $staffForeignLanguageScore = StaffForeignLanguageScores::find($id);
            if (!$staffForeignLanguageScore) {
                throw new ModelNotFoundException("StaffForeignLanguageScores not found with ID: $id");
            }
            $staffForeignLanguageScore->delete();
            return BaseService::responseSet(true, 'StaffForeignLanguageScores delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting StaffForeignLanguageScores',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteByStaffId($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Staff ID is required to delete a StaffForeignLanguageScores resource");
            }
            $staffForeignLanguageScore = StaffForeignLanguageScores::where('s_id', $id)->get();
            if (!$staffForeignLanguageScore) {
                throw new ModelNotFoundException("StaffForeignLanguageScores not found with Staff ID: $id");
            }
            $staffForeignLanguageScore->delete();
            return BaseService::responseSet(true, 'StaffForeignLanguageScores delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting StaffForeignLanguageScores',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteByForeignLanguageId($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("ForeignLanguage ID is required to delete a StaffForeignLanguageScores resource");
            }
            $staffForeignLanguageScore = StaffForeignLanguageScores::where('fl_id', $id)->get();
            if (!$staffForeignLanguageScore) {
                throw new ModelNotFoundException("StaffForeignLanguageScores not found with ForeignLanguage ID: $id");
            }
            $staffForeignLanguageScore->delete();
            return BaseService::responseSet(true, 'StaffForeignLanguageScores delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting StaffForeignLanguageScores',
                'message' => $e->getMessage(),
            ];
        }
    }
}
