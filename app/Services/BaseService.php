<?php

namespace App\Services;

class BaseService
{
    public static function responseSet($success, $message, $status, $data = [])
    {
        $responseData = [
            'success' => $success,
            'message' => $message,
            'status' => $status,

        ];
        if (!empty($data)) {
            $responseData['data'] = $data;
        }
        return $responseData;
    }
}
