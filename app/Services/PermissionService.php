<?php

namespace App\Services;

use App\Models\Permissions;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PermissionService
{
    public static function get($id = null){
        try{
            if(!is_null($id)){
                $permission = Permissions::find($id);
                if(!$permission){
                    throw new ModelNotFoundException("Permission not found with ID: $id");
                }
                return BaseService::responseSet(true, 'Permission get successfully', 200, $permission);
            }
            $permissions = Permissions::all();
            return BaseService::responseSet(true, 'Permissions get successfully', 200, $permissions);
        }
        catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Permission',
                'message' => $e->getMessage(),
            ];
        }
    }
}
