<?php

namespace App\Services;

use App\Models\ClassSchedule;
use App\Models\Course;
use App\Models\CourseClassSchedule;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CourseClassScheduleService
{
    public static function getCourseByClassScheduleId($classScheduleId = null)
    {
        try {
            if (!is_null($classScheduleId)) {
                $course = CourseClassSchedule::where('cs_id', $classScheduleId)->with('course')->get();
                if (!$course) {
                    throw new ModelNotFoundException("Course not found with ID: $classScheduleId");
                }
                return BaseService::responseSet(true, 'Course get successfully', 200, $course);
            }
            throw new ModelNotFoundException("ClassSchedule ID is required to delete a User resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting  Course',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getClassScheduleByCourseId($courseId = null)
    {
        try {
            if (!is_null($courseId)) {
                $classSchedule = CourseClassSchedule::where('c_id', $courseId)->with('classSchedule')->get();
                if (!$classSchedule) {
                    throw new ModelNotFoundException("ClassSchedule not found with ID: $courseId");
                }
                return BaseService::responseSet(true, 'ClassSchedule get successfully', 200, $classSchedule);
            }
            throw new ModelNotFoundException("Course ID is required to delete a ClassScheduleAndCourse resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting  ClassSchedule',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function createClassScheduleAndCourse($classScheduleId = null, $courseId = [])
    {
        try {
            if (!is_null($classScheduleId)) {
                $classSchedule = ClassSchedule::find($classScheduleId);
                if ($classSchedule) {
                    $courseClassScheduleArray = [];
                    if (!empty($courseId)) {
                        foreach ($courseId as $c_id) {
                            if (!Course::find($c_id)) {
                                throw new ModelNotFoundException("Course not found with ID: $c_id");
                            }
                            if (CourseClassSchedule::where('cs_id', $classScheduleId)->where('c_id', $c_id)->first()) {
                                return BaseService::responseSet(false, "CourseClassSchedule with Course $c_id and ClassSchedule $classScheduleId already exists", 409);
                            }
                        }
                        foreach ($courseId as $c_id) {
                            $result = CourseClassSchedule::create([
                                'c_id' => $c_id,
                                'cs_id' => $classScheduleId
                            ]);

                            $courseClassScheduleArray[] = $result;
                        }
                        return BaseService::responseSet(true, 'ClassSchedule created successfully', 200, $courseClassScheduleArray);
                    }
                }
                throw new ModelNotFoundException("ClassSchedule not found with ID: $classScheduleId");
            }
            throw new ModelNotFoundException("classScheduleId ID is required to create a ClassScheduleAndCourse resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while creating ClassSchedule and Course',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteClassScheduleAndCourse($classScheduleId = null, $courseId = null)
    {
        try {
            if (!is_null($courseId) && !is_null($classScheduleId)) {
                $course = Course::find($courseId);
                $classSchedule = ClassSchedule::find($classScheduleId);
                if (!$course) {
                    throw new ModelNotFoundException("Course not found with ID: $courseId");
                }
                if (!$classSchedule) {
                    throw new ModelNotFoundException("ClassSchedule not found with ID: $classScheduleId");
                }
                CourseClassSchedule::where('cs_id', $classScheduleId)->where('c_id', $courseId)->delete();
                return BaseService::responseSet(true, 'ClassSchedule and Course delete successfully', 200);
            }

            if (!is_null($courseId) && is_null($classScheduleId)) {
                $course = Course::find($courseId);
                if (!$course) {
                    throw new ModelNotFoundException("Course not found with ID: $courseId");
                }
                CourseClassSchedule::where('c_id', $courseId)->delete();
                return BaseService::responseSet(true, 'ClassSchedule and Course delete successfully', 200);
            }

            if (!is_null($classScheduleId) && is_null($courseId)) {
                $classSchedule = ClassSchedule::find($classScheduleId);
                if (!$classSchedule) {
                    throw new ModelNotFoundException("ClassSchedule not found with ID: $classScheduleId");
                }
                CourseClassSchedule::where('cs_id', $classScheduleId)->delete();
                return BaseService::responseSet(true, 'ClassSchedule and Course delete successfully', 200);
            }
            throw new ModelNotFoundException("classScheduleId or course ID is required to delete a ClassScheduleAndCourse resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting ClassSchedule or Course',
                'message' => $e->getMessage(),
            ];
        }
    }
}
