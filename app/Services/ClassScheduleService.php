<?php

namespace App\Services;

use App\Models\ClassSchedule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\CourseClassSchedule;
use App\Models\Classes;

class ClassScheduleService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $classSchedule = ClassSchedule::find($id);
                if (!$classSchedule) {
                    throw new ModelNotFoundException("ClassSchedule not found with ID: $id");
                }
                return BaseService::responseSet(true, 'ClassSchedule get successfully', 200, $classSchedule);
            }
            $classSchedules = ClassSchedule::all();
            return BaseService::responseSet(true, 'ClassSchedule get successfully', 200, $classSchedules);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting ClassSchedule',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                if (!ClassSchedule::find($id)) {
                    throw new ModelNotFoundException("ClassSchedule not found with ID: $id");
                }
            }
            $chName = ClassSchedule::where('ch_name', $data['ch_name'])->first();
            $trainingYear = ClassSchedule::where('training_year', $data['training_year'])->first();

            if ($chName) {
                if ($trainingYear) {
                    return BaseService::responseSet(false, 'ClassSchedule with the same ch_name and training_year already exists', 400);
                }
            }

            if (strtotime($data['start_date']) > strtotime($data['end_date'])) {
                return BaseService::responseSet(false, 'The start date cannot be greater than the end date', 400);
            }

            if (strtotime($data['deadline_date']) > strtotime($data['start_date'])) {
                return BaseService::responseSet(false, 'The deadline date cannot be greater than the start date', 400);
            }

            if ($data['plan_trainees_number'] <= 0) {
                return BaseService::responseSet(false, 'The number of trainees cannot be less than or equal to 0', 400);
            }

            $setClassSchedule = ClassSchedule::updateOrCreate(
                ['cs_id' => $id],
                [
                    'training_year' => $data['training_year'],
                    'term' => $data['term'],
                    'ch_name' => $data['ch_name'],
                    'contacts_name' => $data['contacts_name'],
                    'contacts_phone' => $data['contacts_phone'],
                    'start_date' => $data['start_date'],
                    'end_date' => $data['end_date'],
                    'plan_trainees_number' => $data['plan_trainees_number'],
                    'deadline_date' => $data['deadline_date'],
                    'budget' => $data['budget'],
                    'remarks' => $data['remarks'],
                    'o_id' => $data['o_id'],
                    'tu_id' => $data['tu_id']
                ]
            );
            return BaseService::responseSet(true, 'ClassSchedule updated or created successfully', 200, $setClassSchedule);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating ClassSchedule',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("ClassSchedule ID is required to delete a User resource");
            }
            $classSchedules = ClassSchedule::find($id);
            if (!$classSchedules) {
                throw new ModelNotFoundException("ClassSchedule not found with ID: $id");
            }

            if (CourseClassSchedule::where('cs_id', $id)->get()) {
                return BaseService::responseSet(false, 'Please delete the data inside the courseClassSchedule table where the ID exists', 400);
            }

            if (Classes::where('cs_id', $id)->get()) {
                return BaseService::responseSet(false, 'Please delete the data inside the classes table where the ID exists', 400);
            }

            $classSchedules->delete();
            return BaseService::responseSet(true, 'ClassSchedule delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting ClassSchedule',
                'message' => $e->getMessage(),
            ];
        }
    }
}
