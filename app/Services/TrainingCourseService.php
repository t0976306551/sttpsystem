<?php

namespace App\Services;

use App\Models\TrainingCourses;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TrainingCourseService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $trainingCourse = TrainingCourses::find($id);
                if (!$trainingCourse) {
                    throw new ModelNotFoundException("TrainingCourses not found with ID: $id");
                }
                return BaseService::responseSet(true, 'TrainingCourses get successfully', 200, $trainingCourse);
            }
            $trainingCourses = TrainingCourses::all();
            return BaseService::responseSet(true, 'TrainingCourses get successfully', 200, $trainingCourses);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting TrainingCourses',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                if (!TrainingCourses::find($id)) {
                    throw new ModelNotFoundException("TrainingCourses not found with ID: $id");
                }
            }
            $checkTrainingCourseName = TrainingCourses::where('tc_name', $data['tc_name'])->first();
            if ($checkTrainingCourseName) {
                return BaseService::responseSet(false, 'TrainingCourses with the same name already exists', 409);
            }
            $setTrainingCourse = TrainingCourses::updateOrCreate(
                ['tc_id' => $id],
                [
                    'tc_name' => $data['tc_name'],
                    'hours' => $data['hours'],
                    't_name' => $data['t_name'],
                    't_unit' => $data['t_unit'],
                    't_position' => $data['t_position']
                ]
            );
            return BaseService::responseSet(true, 'TrainingCourses updated or created successfully', 200, $setTrainingCourse);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating TrainingCourses',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("TrainingCourses ID is required to delete a User resource");
            }
            $trainingCourse = TrainingCourses::find($id);
            if (!$trainingCourse) {
                throw new ModelNotFoundException("TrainingCourses not found with ID: $id");
            }
            $trainingCourse->delete();
            return BaseService::responseSet(true, 'TrainingCourses delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting TrainingCourses',
                'message' => $e->getMessage(),
            ];
        }
    }
}
