<?php

namespace App\Services;

use App\Models\SupervisorLevel;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SupervisorLevelService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $supervisorLevel = SupervisorLevel::find($id);
                if (!$supervisorLevel) {
                    throw new ModelNotFoundException("SupervisorLevel not found with ID: $id");
                }
                return [
                    'success' => true,
                    'message' => 'SupervisorLevel get successfully',
                    'data' => $supervisorLevel,
                ];
            }

            $supervisorLevels = SupervisorLevel::all();
            return [
                'success' => true,
                'message' => 'SupervisorLevel get successfully',
                'data' => $supervisorLevels,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting SupervisorLevel',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $supervisorLevel = SupervisorLevel::find($id);
                if (!$supervisorLevel) {
                    throw new ModelNotFoundException("SupervisorLevel not found with ID: $id");
                }
                $data['sl_id'] = $id;
            }
            if (SupervisorLevel::where('sl_name', $data['sl_name'])->first()) {
                return [
                    'success' => false,
                    'message' => 'SupervisorLevel with the same sl_name already exists',
                    'status' => 409
                ];
            }

            $setSupervisorLevel = SupervisorLevel::updateOrCreate(
                ['sl_id', $data['sl_id']],
                [
                    'sl_id' => $data['sl_id'],
                    'sl_name' => $data['sl_name']
                ]
            );
            return [
                'success' => true,
                'message' => 'SupervisorLevel updated or created successfully',
                'data' => $setSupervisorLevel,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating SupervisorLevel',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("SupervisorLevel ID is required to delete a Unit resource");
            }
            $supervisorLevel = SupervisorLevel::find($id);
            if (!$supervisorLevel) {
                throw new ModelNotFoundException("SupervisorLevel not found with ID: $id");
            }
            $supervisorLevel->delete();
            return [
                'success' => true,
                'message' => 'SupervisorLevel delete successfully',
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting SupervisorLevel',
                'message' => $e->getMessage(),
            ];
        }
    }
}
