<?php

namespace App\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\ClassesTrainingCourse;
use App\Models\Classes;
use App\Models\TrainingCourses;


class ClassesTrainingCourseService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $classesTrainingCourse = ClassesTrainingCourse::find($id);
                if (!$classesTrainingCourse) {
                    throw new ModelNotFoundException("ClassesTrainingCourse not found with id: $id");
                }
                return BaseService::responseSet(true, 'ClassesTrainingCourse get successfully', 200, $classesTrainingCourse);
            }
            $classesTrainingCourse = ClassesTrainingCourse::all();
            return BaseService::responseSet(true, 'ClassesTrainingCourse get successfully', 200, $classesTrainingCourse);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting ClassesTrainingCourse',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getByClassesId($id = null)
    {
        try {
            if (!is_null($id)) {
                $classesTrainingCourse = ClassesTrainingCourse::where('c_id', $id)->get();
                if (!$classesTrainingCourse) {
                    throw new ModelNotFoundException("ClassesTrainingCourse not found with Classes id: $id");
                }
                return BaseService::responseSet(true, 'ClassesTrainingCourse get successfully', 200, $classesTrainingCourse);
            }
            throw new ModelNotFoundException("Classes ID is required to get a ClassesTrainingCourse resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting ClassesTrainingCourse',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getByTrainingCourseId($id = null)
    {
        try {
            if (!is_null($id)) {
                $classesTrainingCourse = ClassesTrainingCourse::where('tc_id', $id)->get();
                if (!$classesTrainingCourse) {
                    throw new ModelNotFoundException("ClassesTrainingCourse not found with TrainingCourse id: $id");
                }
                return BaseService::responseSet(true, 'ClassesTrainingCourse get successfully', 200, $classesTrainingCourse);
            }
            throw new ModelNotFoundException("TrainingCourse ID is required to get a ClassesTrainingCourse resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting ClassesTrainingCourse',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $classesTrainingCourse = ClassesTrainingCourse::find($id);
                if (!$classesTrainingCourse) {
                    throw new ModelNotFoundException("ClassesTrainingCourse not found with ID: $id");
                }

                $checkClasses = Classes::find($data['c_id']);
                $checkTrainingCourses = TrainingCourses::find($data['tc_id']);

                if (!$checkClasses || !$checkTrainingCourses) {
                    throw new ModelNotFoundException("Classes or TrainingCourse not found with ID: " . $data['c_id'] . ' or ' . $data['tc_id']);
                }

                $checkDuplicate = ClassesTrainingCourse::where('u_id', $data['u_id'])->where('cs_id', $data['cs_id'])->get();
                if ($checkDuplicate) {
                    return BaseService::responseSet(false, 'ClassesTrainingCourse with the same Classes and TrainingCourse already exists', 409);
                }

                $setClassesTrainingCourse = ClassesTrainingCourse::updateOrCreate(
                    ['ctc_id' => $id],
                    [
                        'c_id' => $data['c_id'],
                        'tc_id' => $data['tc_id']
                    ]
                );
                return BaseService::responseSet(true, 'ClassesTrainingCourse updated or created successfully', 200, $setClassesTrainingCourse);
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating ClassesTrainingCourse',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("ClassesTrainingCourse ID is required to delete a StaffForeignLanguageScores resource");
            }
            $classesTrainingCourse = ClassesTrainingCourse::find($id);
            if (!$classesTrainingCourse) {
                throw new ModelNotFoundException("ClassesTrainingCourse not found with ID: $id");
            }
            $classesTrainingCourse->delete();
            return BaseService::responseSet(true, 'ClassesTrainingCourse delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting ClassesTrainingCourse',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteByClassesId($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Classes ID is required to delete a ClassesTrainingCourse resource");
            }
            $classesTrainingCourse = ClassesTrainingCourse::where('c_id', $id)->get();
            if (!$classesTrainingCourse) {
                throw new ModelNotFoundException("ClassesTrainingCourse not found with Classes ID: $id");
            }
            $classesTrainingCourse->delete();
            return BaseService::responseSet(true, 'ClassesTrainingCourse delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting ClassesTrainingCourse',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteByTrainingCourseId($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("TrainingCourse ID is required to delete a ClassesTrainingCourse resource");
            }
            $classesTrainingCourse = ClassesTrainingCourse::where('tc_id', $id)->get();
            if (!$classesTrainingCourse) {
                throw new ModelNotFoundException("ClassesTrainingCourse not found with TrainingCourse ID: $id");
            }
            $classesTrainingCourse->delete();
            return BaseService::responseSet(true, 'ClassesTrainingCourse delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting ClassesTrainingCourse',
                'message' => $e->getMessage(),
            ];
        }
    }
}
