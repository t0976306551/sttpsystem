<?php

namespace App\Services;

use App\Models\TrainingAndCertificateResults;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Classes;
use App\Models\Staff;

class TrainingAndCertificateResultsService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $trainingAndCertificateResult = TrainingAndCertificateResults::find($id);
                if (!$trainingAndCertificateResult) {
                    throw new ModelNotFoundException("TrainingAndCertificateResults not found with ID: $id");
                }
                return BaseService::responseSet(true, 'TrainingAndCertificateResults get successfully', 200, $trainingAndCertificateResult);
            }

            $trainingAndCertificateResult = TrainingAndCertificateResults::all();
            return BaseService::responseSet(true, 'TrainingAndCertificateResults get successfully', 200, $trainingAndCertificateResult);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting TrainingAndCertificateResults',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getTrainingAndCertificateResultsByStaffId($id = null)
    {
        try {
            if (!is_null($id)) {
                $trainingAndCertificateResult = TrainingAndCertificateResults::where('s_id', $id)->get();
                if (!$trainingAndCertificateResult) {
                    throw new ModelNotFoundException("TrainingAndCertificateResults not found with ID: $id");
                }
                return BaseService::responseSet(true, 'TrainingAndCertificateResults get successfully', 200, $trainingAndCertificateResult);
            }

            throw new ModelNotFoundException("Staff ID is required to get a TrainingAndCertificateResults resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting TrainingAndCertificateResults',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getTrainingAndCertificateResultsByClassesId($id = null)
    {
        try {
            if (!is_null($id)) {
                $trainingAndCertificateResult = TrainingAndCertificateResults::where('c_id', $id)->get();
                if (!$trainingAndCertificateResult) {
                    throw new ModelNotFoundException("TrainingAndCertificateResults not found with ID: $id");
                }
                return BaseService::responseSet(true, 'TrainingAndCertificateResults get successfully', 200, $trainingAndCertificateResult);
            }

            throw new ModelNotFoundException("Classes ID is required to get a TrainingAndCertificateResults resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting TrainingAndCertificateResults',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $trainingAndCertificateResult = TrainingAndCertificateResults::find($id);
                if (!$trainingAndCertificateResult) {
                    throw new ModelNotFoundException("TrainingAndCertificateResults not found with ID: $id");
                }
            }

            $checkClasses = Classes::find($data['c_id']);
            $checkStaff = Staff::find($data['s_id']);

            if (!$checkClasses || !$checkStaff) {
                throw new ModelNotFoundException("Classes or Staff not found with ID: " . $data['c_id'] . ' or ' . $data['s_id']);
            }

            $checkDuplicate = TrainingAndCertificateResults::where('c_id', $data['c_id'])->where('s_id', $data['s_id'])->get();
            if ($checkDuplicate) {
                return BaseService::responseSet(false, 'TrainingAndCertificateResults with the same classes and staff already exists', 409);
            }

            $setTrainingAndCertificateResults = TrainingAndCertificateResults::updateOrCreate(
                ['tac_id' => $id],
                [
                    'c_id' => $data['c_id'],
                    's_id' => $data['s_id'],
                    'academic_score' => $data['academic_score'],
                    'practical_score' => $data['practical_score']
                ]
            );
            return BaseService::responseSet(true, 'TrainingAndCertificateResults updated or created successfully', 200, $setTrainingAndCertificateResults);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while creating TrainingAndCertificateResults',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("TrainingAndCertificateResults ID is required to delete a User resource");
            }
            $trainingAndCertificateResult = TrainingAndCertificateResults::find($id);
            if (!$trainingAndCertificateResult) {
                throw new ModelNotFoundException("TrainingAndCertificateResults not found with ID: $id");
            }
            $trainingAndCertificateResult->delete();
            return BaseService::responseSet(true, 'TrainingAndCertificateResults delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting TrainingAndCertificateResults',
                'message' => $e->getMessage(),
            ];
        }
    }
}
