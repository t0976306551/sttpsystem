<?php

namespace App\Services;

use App\Models\FlatTone;
use App\Models\Sttp;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FlatToneService
{
    public static function getFlatToneBySttpId($sttp_id = null)
    {
        try {
            if (!is_null($sttp_id)) {
                $flatTone = FlatTone::where('sttp_id', $sttp_id)->get();
                if (!$flatTone) {
                    throw new ModelNotFoundException("FlatTone not found with Sttp ID: $sttp_id");
                }
                return BaseService::responseSet(true, 'FlatTone get successfully', 200, $flatTone);
            }
            throw new ModelNotFoundException("FlatTone Sttp ID is required to get a resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting FlatTone',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function createFlatTone($data = [])
    {
        try {
            if (!isset($data['sttp_id']) || $data['sttp_id'] = null) {
                throw new ModelNotFoundException("FlatTone Sttp ID is required to get a resource");
            }

            if (!isset($data['ft_sttp']) || empty($data['ft_sttp'])) {
                throw new ModelNotFoundException("FlatTone ft_sttp ID is required to get a resource");
            }

            if (!Sttp::find($data['sttp_id'])) {
                throw new ModelNotFoundException("Sttp not found with sttp_id");
            }

            foreach ($data['ft_sttp'] as $ft_sttp) {
                if ($data['fttp_id'] == $ft_sttp) {
                    return BaseService::responseSet(false, 'Sttp_id cannot be repeated with ft_sttp', 404);
                }
                if (!Sttp::find($ft_sttp)) {
                    throw new ModelNotFoundException("Sttp not found with ID: $ft_sttp");
                }
            }
            $flatToneArray = [];
            foreach ($data['ft_sttp'] as $ft_sttp) {
                $result = FlatTone::create(
                    [
                        'sttp_id' => $data['sttp_id'],
                        'ft_sttp' => $ft_sttp
                    ]
                );
                $flatToneArray[] = $result;
            }
            return BaseService::responseSet(true, 'FlatTone created successfully', 200, $flatToneArray);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while creating FlatTone',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteFlatTone($id = null, $data)
    {
        try {
            if (!is_null($id)) {
                $flatTone = FlatTone::find($id);
                if ($flatTone) {
                    $flatTone->delete();
                    return BaseService::responseSet(true, 'FlatTone delete successfully', 200);
                }
                throw new ModelNotFoundException("FlatTone not found with ID: $id");
            }

            if (!is_null($data['sttp_id'])) {
                if (!is_null($data['ft_sttp'])) {
                    $flatTone = FlatTone::where('sttp_id', $data['sttp_id'])
                        ->where('ft_sttp', $data['sttp_id'])->first();
                    if ($flatTone) {
                        $flatTone->delete();
                        return BaseService::responseSet(true, 'FlatTone delete successfully', 200);
                    }
                    throw new ModelNotFoundException("FlatTone not found with sttp_id and ft_sttp");
                }
                FlatTone::where('sttp_id', $data['sttp_id'])->delete();
                return BaseService::responseSet(true, 'FlatTone delete successfully by sttp_id', 200);
            }

            if (!is_null($data['ft_sttp'])) {
                FlatTone::where('ft_sttp', $data['ft_sttp'])->delete();
                return BaseService::responseSet(true, 'FlatTone delete successfully by ft_sttp', 200);
            }

            throw new ModelNotFoundException("FlatTone id or sttp_id or ft_sttp need one of them to delete a FlatTone resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting FlatTone',
                'message' => $e->getMessage(),
            ];
        }
    }
}
