<?php

namespace App\Services;

use App\Models\Department;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DepartmentService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $department = Department::find($id);
                if (!$department) {
                    throw new ModelNotFoundException("Department not found with ID: $id");
                }
                return [
                    'success' => true,
                    'message' => 'Department get successfully',
                    'data' => $department,
                    'status' => 200
                ];
            }
            $department = Department::all();
            return [
                'success' => true,
                'message' => 'Department get successfully',
                'data' => $department,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Department',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $department = Department::find($id);
                if (!$department) {
                    throw new ModelNotFoundException("Department not found with ID: $id");
                }
                $data["d_id"] = $id;
            }

            if (is_null($id)) {
                if (!isset($data['d_id'])) {
                    return [
                        'success' => false,
                        'message' => 'Department id cannot be empty',
                        'status' => 404
                    ];
                }

                $id = $data["d_id"];
            }

            $checkDepartmentName = Department::where('d_name', $data['d_name'])->first();
            if ($checkDepartmentName) {
                return [
                    'success' => false,
                    'message' => 'Department with the same name already exists',
                    'status' => 409
                ];
            }

            $setDepartment = Department::updateOrCreate(
                ['d_id' => $id],
                [
                    'd_id' => $data["d_id"],
                    'd_name' => $data['d_name']
                ]
            );

            return [
                'success' => true,
                'message' => 'Department updated or created successfully',
                'data' => $setDepartment,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Department',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Department ID is required to delete a Department resource");
            }
            $department = Department::find($id);
            if (!$department) {
                throw new ModelNotFoundException("Department not found with ID: $id");
            }
            $department->delete();
            return [
                'success' => true,
                'message' => 'Department delete successfully',
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Department',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }
}
