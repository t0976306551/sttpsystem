<?php

namespace App\Services;

use App\Models\TrainingUnit;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TrainingUnitService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $trainingUnit = TrainingUnit::find($id);
                if (!$trainingUnit) {
                    throw new ModelNotFoundException("TrainingUnit not found with ID: $id");
                }
                return BaseService::responseSet(true, 'TrainingUnit get successfully', 200, $trainingUnit);
            }
            $trainingUnits = TrainingUnit::all();
            return BaseService::responseSet(true, 'TrainingUnit get successfully', 200, $trainingUnits);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting TrainingUnit',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                if (!TrainingUnit::find($id)) {
                    throw new ModelNotFoundException("TrainingUnit not found with ID: $id");
                }
            }
            $checkTrainingUnitName = TrainingUnit::where('tu_name', $data['tu_name'])->first();
            if ($checkTrainingUnitName) {
                return BaseService::responseSet(false, 'TrainingUnit with the same name already exists', 409);
            }
            $setTrainingUnit = TrainingUnit::updateOrCreate(
                ['tu_id' => $id],
                [
                    'tu_name' => $data['tu_name'],
                    'tu_location' => $data['tu_location']
                ]
            );
            return BaseService::responseSet(true, 'TrainingUnit updated or created successfully', 200, $setTrainingUnit);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating TrainingUnit',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("TrainingUnit ID is required to delete a User resource");
            }
            $trainingUnit = TrainingUnit::find($id);
            if (!$trainingUnit) {
                throw new ModelNotFoundException("TrainingUnit not found with ID: $id");
            }
            $trainingUnit->delete();
            return BaseService::responseSet(true, 'TrainingUnit delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting TrainingUnit',
                'message' => $e->getMessage(),
            ];
        }
    }
}
