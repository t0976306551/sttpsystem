<?php

namespace App\Services;

use App\Models\CourseType;
use App\Models\Course;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CourseTypeService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $courseType = CourseType::find($id);
                if (!$courseType) {
                    throw new ModelNotFoundException("Course Type not found with ID: $id");
                }
                return BaseService::responseSet(true, 'Course Type get successfully', 200, $courseType);
            }

            $courseTypes = CourseType::all();
            return BaseService::responseSet(true, 'Course Type get successfully', 200, $courseTypes);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Course Type',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                if (!CourseType::find($id)) {
                    throw new ModelNotFoundException("Course Type not found with ID: $id");
                }
            }
            $checkCourseType = CourseType::where('type', $data['type'])->first();
            if ($checkCourseType) {
                return BaseService::responseSet(false, 'CourseType with the same type already exists', 409);
            }
            $setCourseType = CourseType::updateOrCreate(
                ['ct_id' => $id],
                [
                    'type' => $data['type']
                ]
            );
            return BaseService::responseSet(true, 'CourseType updated or created successfully', 200, $setCourseType);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating CourseType',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("CourseType ID is required to delete a User resource");
            }
            $courseType = CourseType::find($id);
            if (!$courseType) {
                throw new ModelNotFoundException("CourseType not found with ID: $id");
            }

            $checkCourse = Course::where('ct_id', $id)->get();
            if ($checkCourse) {
                return BaseService::responseSet(false, 'This type still exists in the course, please delete the course first', 404);
            }
            $courseType->delete();
            return BaseService::responseSet(true, 'CourseType delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting CourseType',
                'message' => $e->getMessage(),
            ];
        }
    }
}
