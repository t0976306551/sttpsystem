<?php

namespace App\Services;

use App\Models\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RoleService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $role = Role::find($id);
                if (!$role) {
                    throw new ModelNotFoundException("Role not found with ID: $id");
                }
                return BaseService::responseSet(true, 'Role get successfully', 200, $role);
            }
            $roles = Role::all();
            return BaseService::responseSet(true, 'Roles get successfully', 200, $roles);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Role',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $role = Role::find($id);
                if (!$role) {
                    throw new ModelNotFoundException("Role not found with ID: $id");
                };
            }

            $checkRole = Role::where('r_name', $data['r_name'])->first();
            if ($checkRole) {
                return BaseService::responseSet(false, 'Role with the same name already exists', 409);
            }

            $setRole = Role::updateOrCreate(
                ['r_id' => $id],
                [
                    'r_name' => $data['r_name']
                ]
            );
            return BaseService::responseSet(true, 'Role updated or created successfully', 200, $setRole);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Role',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Role ID is required to delete a User resource");
            }
            $role = Role::find($id);
            if (!$role) {
                throw new ModelNotFoundException("Role not found with ID: $id");
            }
            $role->delete();
            return BaseService::responseSet(true, 'Role delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Role',
                'message' => $e->getMessage(),
            ];
        }
    }
}
