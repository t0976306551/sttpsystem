<?php

namespace App\Services;

use App\Models\Organizer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrganizerService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $organizer = Organizer::find($id);
                if (!$organizer) {
                    throw new ModelNotFoundException("Organizer not found with ID: $id");
                }
                return BaseService::responseSet(true, 'Course get successfully', 200, $organizer);
            }
            $organizers = Organizer::all();
            return BaseService::responseSet(true, 'Organizer get successfully', 200, $organizers);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Organizer',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                if (!Organizer::find($id)) {
                    throw new ModelNotFoundException("Organizer not found with ID: $id");
                }
            }
            $checkOrganizerName = Organizer::where('o_name', $data['o_name'])->first();
            if ($checkOrganizerName) {
                return BaseService::responseSet(false, 'Organizer with the same name already exists', 409);
            }
            $setOrganizer = Organizer::updateOrCreate(
                ['o_id' => $id],
                [
                    'o_name' => $data['o_name']
                ]
            );
            return BaseService::responseSet(true, 'Organizer updated or created successfully', 200, $setOrganizer);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Organizer',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Organizer ID is required to delete a User resource");
            }
            $organizer = Organizer::find($id);
            if (!$organizer) {
                throw new ModelNotFoundException("Organizer not found with ID: $id");
            }
            $organizer->delete();
            return BaseService::responseSet(true, 'Organizer delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Organizer',
                'message' => $e->getMessage(),
            ];
        }
    }
}
