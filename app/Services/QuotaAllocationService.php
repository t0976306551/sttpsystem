<?php

namespace App\Services;

use App\Models\QuotaAllocation;
use App\Models\Unit;
use App\Models\ClassSchedule;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class QuotaAllocationService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $quotaAllocation = QuotaAllocation::find($id);
                if (!$quotaAllocation) {
                    throw new ModelNotFoundException("QuotaAllocation not found with id: $id");
                }
                return BaseService::responseSet(true, 'QuotaAllocation get successfully', 200, $quotaAllocation);
            }
            $quotaAllocation = QuotaAllocation::all();
            return BaseService::responseSet(true, 'QuotaAllocation get successfully', 200, $quotaAllocation);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting QuotaAllocation',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getByUnitId($id = null)
    {
        try {
            if (!is_null($id)) {
                $quotaAllocation = QuotaAllocation::where('u_id', $id)->get();
                if (!$quotaAllocation) {
                    throw new ModelNotFoundException("QuotaAllocation not found with Unit id: $id");
                }
                return BaseService::responseSet(true, 'QuotaAllocation get successfully', 200, $quotaAllocation);
            }
            $quotaAllocation = QuotaAllocation::all();
            return BaseService::responseSet(true, 'QuotaAllocation get successfully', 200, $quotaAllocation);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting QuotaAllocation',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getByClassScheduleId($id = null)
    {
        try {
            if (!is_null($id)) {
                $quotaAllocation = QuotaAllocation::where('cs_id', $id)->get();
                if (!$quotaAllocation) {
                    throw new ModelNotFoundException("QuotaAllocation not found with ClassSchedule id: $id");
                }
                return BaseService::responseSet(true, 'QuotaAllocation get successfully', 200, $quotaAllocation);
            }
            $quotaAllocation = QuotaAllocation::all();
            return BaseService::responseSet(true, 'QuotaAllocation get successfully', 200, $quotaAllocation);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting QuotaAllocation',
                'message' => $e->getMessage(),
            ];
        }
    }


    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $quotaAllocation = QuotaAllocation::find($id);
                if (!$quotaAllocation) {
                    throw new ModelNotFoundException("QuotaAllocation not found with ID: $id");
                }

                $checkUnit = Unit::find($data['u_id']);
                $checkClassSchedule = ClassSchedule::find($data['cs_id']);

                if (!$checkUnit || !$checkClassSchedule) {
                    throw new ModelNotFoundException("Unit or ClassSchedule not found with ID: " . $data['u_id'] . ' or ' . $data['cs_id']);
                }

                $checkDuplicate = QuotaAllocation::where('u_id', $data['u_id'])->where('cs_id', $data['cs_id'])->get();
                if ($checkDuplicate) {
                    return BaseService::responseSet(false, 'QuotaAllocation with the same Unit and Classedule already exists', 409);
                }

                $setQuotaAllocation = QuotaAllocation::updateOrCreate(
                    ['qa_id' => $id],
                    [
                        'u_id' => $data['u_id'],
                        'cs_id' => $data['cs_id'],
                        'quota' => $data['quota']
                    ]
                );
                return BaseService::responseSet(true, 'QuotaAllocation updated or created successfully', 200, $setQuotaAllocation);
            }
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while creating or updating QuotaAllocation',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("QuotaAllocation ID is required to delete a StaffForeignLanguageScores resource");
            }
            $quotaAllocation = QuotaAllocation::find($id);
            if (!$quotaAllocation) {
                throw new ModelNotFoundException("QuotaAllocation not found with ID: $id");
            }
            $quotaAllocation->delete();
            return BaseService::responseSet(true, 'QuotaAllocation delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting QuotaAllocation',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteByUnitId($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Unit ID is required to delete a QuotaAllocation resource");
            }
            $quotaAllocation = QuotaAllocation::where('u_id', $id)->get();
            if (!$quotaAllocation) {
                throw new ModelNotFoundException("QuotaAllocation not found with Staff ID: $id");
            }
            $quotaAllocation->delete();
            return BaseService::responseSet(true, 'QuotaAllocation delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting QuotaAllocation',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteByClassScheduleId($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("ClassSchedule ID is required to delete a QuotaAllocation resource");
            }
            $quotaAllocation = QuotaAllocation::where('cs_id', $id)->get();
            if (!$quotaAllocation) {
                throw new ModelNotFoundException("QuotaAllocation not found with ClassSchedule ID: $id");
            }
            $quotaAllocation->delete();
            return BaseService::responseSet(true, 'QuotaAllocation delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting QuotaAllocation',
                'message' => $e->getMessage(),
            ];
        }
    }
}
