<?php

namespace App\Services;

use App\Models\Staff;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StaffService
{
    public static function get($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $staff = Staff::find($id);
                if (!$staff) {
                    throw new ModelNotFoundException("Staff not found with ID: $id");
                }
                return BaseService::responseSet(true, 'Staff get successfully', 200, $staff);
            }
            $where = [];
            $searchs = ['sttp_id', 'sl_id'];
            foreach ($searchs as $search) {
                if (!is_null($data[$search]) && $data[$search] != '') {
                    $where[$search] = $data[$search];
                }
            }
            $staffs = Staff::with('sttp', 'supervisorLevel')->where($where)->get();
            return BaseService::responseSet(true, 'Staff get successfully', 200, $staffs);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Staff',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function create($data)
    {
        try {
            if (Staff::where('s_phone', $data['s_phone'])->first()) {
                return BaseService::responseSet(false, 'Staff with the same phone already exists', 409);
            }
            $createStaff = Staff::create(
                [
                    's_id' => $data['s_id'],
                    's_name' => $data['s_name'],
                    's_phone' => $data['s_phone'],
                    's_gender' => $data['s_gender'],
                    'sl_id' => $data['sl_id'],
                    'sttp_id' => $data['sttp_id']
                ]
            );
            return BaseService::responseSet(true, 'Staff updated or created successfully', 200, $createStaff);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Staff',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public function update($data, $id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Staff ID is required to delete a Staff resource");
            }

            $staff = Staff::find($id);
            if (!$staff) {
                throw new ModelNotFoundException("Staff not found with ID: $id");
            }
            $updateStaff = $staff->update([
                's_id' => $data['s_id'],
                's_name' => $data['s_name'],
                's_phone' => $data['s_phone'],
                's_gender' => $data['s_gender'],
                'sl_id' => $data['sl_id'],
                'sttp_id' => $data['sttp_id']
            ]);

            return BaseService::responseSet(true, 'Staff updated or created successfully', 200, $updateStaff);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Staff',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Staff ID is required to delete a User resource");
            }
            $staff = Staff::find($id);
            if (!$staff) {
                throw new ModelNotFoundException("Staff not found with ID: $id");
            }
            $staff->delete();
            return BaseService::responseSet(true, 'Staff delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Staff',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }
}
