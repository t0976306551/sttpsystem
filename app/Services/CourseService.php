<?php

namespace App\Services;

use App\Models\Course;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\CourseClassSchedule;

class CourseService
{
    public static function get($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $course = Course::find($id);
                if (!$course) {
                    throw new ModelNotFoundException("Course not found with ID: $id");
                }
                return BaseService::responseSet(true, 'Course get successfully', 200, $course);
            }
            $courses = Course::all();
            return BaseService::responseSet(true, 'Course get successfully', 200, $courses);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Course',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getCourseByType($id = null)
    {
        try {
            if (!is_null($id)) {
                $course = Course::where('ct_id', $id)->get();
                if (!$course) {
                    throw new ModelNotFoundException("Course not found with type ID: $id");
                }
                return BaseService::responseSet(true, 'Course get successfully', 200, $course);
            }
            return BaseService::responseSet(false, 'Course type id is required', 409);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Course',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                if (!Course::find($id)) {
                    throw new ModelNotFoundException("Course not found with ID: $id");
                }
            }
            $checkCourseName = Course::where('c_name', $data['c_name'])->first();
            if ($checkCourseName) {
                return BaseService::responseSet(false, 'Course with the same name already exists', 409);
            }
            $setCourse = Course::updateOrCreate(
                ['c_id' => $id],
                [
                    'c_name' => $data['c_name'],
                    'ct_id' => $data['ct_id'],
                    'hours' => $data['hours'],
                    't_name' => $data['t_name'],
                    't_unit' => $data['t_unit'],
                    't_position' => $data['t_position']
                ]
            );
            return BaseService::responseSet(true, 'Course updated or created successfully', 200, $setCourse);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Course',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Course ID is required to delete a User resource");
            }
            $course = Course::find($id);
            if (!$course) {
                throw new ModelNotFoundException("Course not found with ID: $id");
            }

            if (CourseClassSchedule::where('c_id', $id)->get()) {
                return BaseService::responseSet(false, 'Please delete the data inside the courseClassSchedule table where the ID exists', 400);
            }

            $course->delete();
            return BaseService::responseSet(true, 'Course delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Course',
                'message' => $e->getMessage(),
            ];
        }
    }
}
