<?php

namespace App\Services;

use App\Models\Sttp;
use App\Models\Department;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SttpService
{
    public static function get($id = null, $data)
    {
        try {
            if (!is_null($id)) {
                $sttp = Sttp::with('department', 'unit', 'position')->find($id);
                if (!$sttp) {
                    throw new ModelNotFoundException("Sttp not found with ID: $id");
                }
                $sttp['fullTitle'] =
                    $sttp->department->d_name . '-' .
                    $sttp->unit->u_name . '-' .
                    $sttp->position->p_name;
                return [
                    'success' => true,
                    'message' => 'Sttp get successfully',
                    'data' => $sttp,
                    'status' => 200
                ];
            }
            //用來判斷where的資料
            $where = [];
            $searchs = ['d_id', 'u_id', 'p_id'];
            foreach ($searchs as $search) {
                if (!is_null($data[$search]) && $data[$search] != '') {
                    $where[$search] = $data[$search];
                }
            }
            $sttps = Sttp::with('department', 'unit', 'position')->where($where)->get();
            foreach ($sttps as $sttp) {
                $sttp['fullTitle'] = $sttp->department->d_name . '-';
                if (is_null($sttp->u_id)) {
                    $sttp['fullTitle'] = $sttp['fullTitle'] . '無' . '-';
                } else {
                    $sttp['fullTitle'] = $sttp['fullTitle'] . $sttp->unit->u_name . '-';
                }
                $sttp['fullTitle'] = $sttp['fullTitle'] . $sttp->position->p_name;
            }
            return [
                'success' => true,
                'message' => 'Sttp get successfully',
                'data' => $sttps,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Sttp',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function getAllDepartment()
    {
        try {
            $allData = Department::with('unit', 'unit.courseShare.position')->get();
            return [
                'success' => true,
                'message' => 'All Department get successfully',
                'data' => $allData,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting all Department',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $sttp = Sttp::find($id);
                if (!$sttp) {
                    throw new ModelNotFoundException("Sttp not found with ID: $id");
                }

                $data['sttp_id'] = $id;
            }

            if (!isset($data['u_id'])) {
                $data['u_id'] = null;
            }

            if (!isset($data['p_id'])) {
                $data['p_id'] = null;
            }

            $checkSttp = Sttp::where('d_id', $data['d_id'])
                ->where('u_id', $data['u_id'])
                ->where('p_id', $data['p_id'])
                ->first();
            if ($checkSttp) {
                return BaseService::responseSet(false, 'Sttp with the same name already exists', 409);
            }

            if (is_null($id)) {
                $data['sttp_id'] =   $data['d_id'] .
                    ($data['u_id'] ?? '') .
                    ($data['p_id'] ?? '');
            }

            $setSttp = Sttp::updateOrCreate(
                ['sttp_id' => $data['sttp_id']],
                [
                    'sttp_id' => $data['sttp_id'],
                    'd_id' => $data['d_id'],
                    'u_id' => $data['u_id'] ?? null,
                    'p_id' => $data['p_id'] ?? null
                ]
            );
            return [
                'success' => true,
                'message' => 'Sttp updated or created successfully',
                'data' => $setSttp,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Sttp',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Sttp ID is required to delete a Sttp resource");
            }
            $sttp = Sttp::find($id);
            if (!$sttp) {
                throw new ModelNotFoundException("Sttp not found with ID: $id");
            }
            $sttp->delete();
            return [
                'success' => true,
                'message' => 'Sttp delete successfully',
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Sttp',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }
}
