<?php

namespace App\Services;

use App\Models\SttpBasicTraining;
use App\Models\Sttp;
use App\Models\Classes;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SttpBasicTrainingService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $sttpBasicTraining = SttpBasicTraining::find($id);
                if (!$sttpBasicTraining) {
                    throw new ModelNotFoundException("SttpBasicTraining not found with ID: $id");
                }
                return BaseService::responseSet(true, 'SttpBasicTraining get successfully', 200, $sttpBasicTraining);
            }
            $sttpBasicTraining = SttpBasicTraining::all();
            return BaseService::responseSet(true, 'SttpBasicTraining get successfully', 200, $sttpBasicTraining);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting SttpBasicTraining',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getBySttpId($id = null)
    {
        try {
            if (!is_null($id)) {
                $sttpBasicTraining = SttpBasicTraining::where('sttp_id', $id)->get();
                if (!$sttpBasicTraining) {
                    throw new ModelNotFoundException("SttpBasicTraining not found with Sttp ID: $id");
                }
                return BaseService::responseSet(true, 'SttpBasicTraining get successfully', 200, $sttpBasicTraining);
            }
            throw new ModelNotFoundException("Sttp ID is required to get a SttpBasicTraining resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting SttpBasicTraining',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function getByClassesId($id = null)
    {
        try {
            if (!is_null($id)) {
                $sttpBasicTraining = SttpBasicTraining::where('c_id', $id)->get();
                if (!$sttpBasicTraining) {
                    throw new ModelNotFoundException("SttpBasicTraining not found with Classes ID: $id");
                }
                return BaseService::responseSet(true, 'SttpBasicTraining get successfully', 200, $sttpBasicTraining);
            }
            throw new ModelNotFoundException("Classes ID is required to get a SttpBasicTraining resource");
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting SttpBasicTraining',
                'message' => $e->getMessage(),
            ];
        }
    }


    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $sttpBasicTraining = SttpBasicTraining::find($id);
                if (!$sttpBasicTraining) {
                    throw new ModelNotFoundException("SttpBasicTraining not found with ID: $id");
                }
            }
            $checkDuplicate = SttpBasicTraining::where('sttp_id', $data['sttp_id'])->where('c_id', $data['c_id'])->get();
            if ($checkDuplicate) {
                return BaseService::responseSet(false, 'SttpBasicTraining with the same classes and sttp id already exists', 409);
            }

            $checkSttp = Sttp::find($data['sttp_id']);
            $checkClasses = Classes::find($data['c_id']);

            if (!$checkSttp || !$checkClasses) {
                throw new ModelNotFoundException("Classes or Sttp not found with ID: " . $data['c_id'] . ' or ' . $data['sttp_id']);
            }

            $setSttpBasicTraining = SttpBasicTraining::updateOrCreate(
                ['sbt_id' => $id],
                [
                    'sttp_id' => $data['sttp_id'],
                    'c_id' => $data['c_id'],
                    'elective_type' => $data['elective_type'],
                    'credit_conditions' => $data['credit_conditions'],
                    'classes_type' => $data['classes_type']
                ]
            );
            return BaseService::responseSet(true, 'SttpBasicTraining updated or created successfully', 200, $setSttpBasicTraining);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while creating or updating SttpBasicTraining',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("SttpBasicTraining ID is required to delete a SttpBasicTraining resource");
            }
            $sttpBasicTraining = SttpBasicTraining::find($id);
            if (!$sttpBasicTraining) {
                throw new ModelNotFoundException("SttpBasicTraining not found with ID: $id");
            }
            $sttpBasicTraining->delete();
            return BaseService::responseSet(true, 'SttpBasicTraining delete successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting SttpBasicTraining',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteBySttpId($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Sttp ID is required to delete a SttpBasicTraining resource");
            }
            $sttpBasicTraining = SttpBasicTraining::where('sttp_id', $id)->get();
            if (!$sttpBasicTraining) {
                throw new ModelNotFoundException("SttpBasicTraining not found with Sttp ID: $id");
            }
            $sttpBasicTraining->delete();
            return BaseService::responseSet(true, 'SttpBasicTraining delete By Sttp id successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting SttpBasicTraining',
                'message' => $e->getMessage(),
            ];
        }
    }

    public static function deleteByClassesId($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Sttp ID is required to delete a SttpBasicTraining resource");
            }
            $sttpBasicTraining = SttpBasicTraining::where('c_id', $id)->get();
            if (!$sttpBasicTraining) {
                throw new ModelNotFoundException("SttpBasicTraining not found with Classes ID: $id");
            }
            $sttpBasicTraining->delete();
            return BaseService::responseSet(true, 'SttpBasicTraining delete By Classes id successfully', 200);
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting SttpBasicTraining',
                'message' => $e->getMessage(),
            ];
        }
    }
}
