<?php

namespace App\Services;

use App\Models\Unit;
use App\Models\Department;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UnitService
{
    public static function get($id = null)
    {
        try {
            if (!is_null($id)) {
                $unit = Unit::find($id);
                if (!$unit) {
                    throw new ModelNotFoundException("Unit not found with ID: $id");
                }
                return [
                    'success' => true,
                    'message' => 'Unit get successfully',
                    'data' => $unit,
                    'status' => 200
                ];
            }
            $unit = Unit::all();
            return [
                'success' => true,
                'message' => 'Unit get successfully',
                'data' => $unit,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Unit',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function getUnitByDepartmentId($id = null)
    {
        try {
            if (!is_null($id)) {
                $unit = Unit::where(function ($query) use ($id) {
                    $query->where('d_id', $id)
                        ->orWhereNull('d_id');
                })->get();
                if (!$unit) {
                    throw new ModelNotFoundException("Unit not found with ID: $id");
                }
                return [
                    'success' => true,
                    'message' => 'Unit get successfully',
                    'data' => $unit,
                    'status' => 200
                ];
            }
            return [
                'success' => false,
                'message' => 'Department id cannot be null',
                'status' => 400
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while getting Unit by department id',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function set($data, $id = null)
    {
        try {
            if (!is_null($id)) {
                $unit = Unit::find($id);
                if (!$unit) {
                    throw new ModelNotFoundException("Unit not found with ID: $id");
                }
                $data["u_id"] = $id;
            }

            if (is_null($id)) {
                if (!isset($data['u_id'])) {
                    return [
                        'success' => false,
                        'message' => 'Unit id cannot be empty',
                        'status' => 404
                    ];
                }
                $id = $data["u_id"];
            }
            if (!Department::find($data['d_id'])) {
                throw new ModelNotFoundException("Department not found with ID: " . $data['d_id']);
            }
            $checkUnitName = Unit::where('u_name', $data['u_name'])->first();
            if ($checkUnitName) {
                return [
                    'success' => false,
                    'message' => 'Unit with the same name already exists',
                    'status' => 409
                ];
            }
            $setUnit = Unit::updateOrCreate(
                ['u_id' => $id],
                [
                    'u_id' => $data["u_id"],
                    'u_name' => $data['u_name'],
                    'd_id' => $data['d_id']
                ]
            );
            return [
                'success' => true,
                'message' => 'Unit updated or created successfully',
                'data' => $setUnit,
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while updating or creating Unit',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }

    public static function delete($id = null)
    {
        try {
            if (is_null($id)) {
                throw new ModelNotFoundException("Unit ID is required to delete a Unit resource");
            }
            $unit = Unit::find($id);
            if (!$unit) {
                throw new ModelNotFoundException("Unit not found with ID: $id");
            }
            $unit->delete();
            return [
                'success' => true,
                'message' => 'Unit delete successfully',
                'status' => 200
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while deleting Unit',
                'message' => $e->getMessage(),
                'status' => 500
            ];
        }
    }
}
