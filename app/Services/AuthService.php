<?php

namespace App\Services;

use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthService
{
    public static function login($username, $password)
    {
        try {
            if (!$token = JWTAuth::attempt(['username' => $username, 'password' => $password])) {
                return [
                    'success' => false,
                    'message' => 'invalid credentials',
                    'status' => 401,
                ];
            }
            $user = JWTAuth::user();
            return [
                'success' => true,
                'message' => 'Login successfully',
                'token' => $token,
                'user' => $user,
                'status' => 200,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'error' => 'An error occurred while Login',
                'message' => $e->getMessage(),
                'status' => 500,
            ];
        }
    }

    public static function logout()
    {
        try {
            auth()->logout();
            return [
                'success' => true,
                'message' => 'Logout successfully',
                'status' => 200,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'status' => 500,
            ];
        }
    }
}
