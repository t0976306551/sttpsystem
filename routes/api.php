<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\SttpController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\SttpLearnAndExperienceController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SupervisorLevelController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\OrganizerController;
use App\Http\Controllers\TrainingUnitController;
use App\Http\Controllers\ClassScheduleController;
use App\Http\Controllers\CourseClassScheduleController;
use App\Http\Controllers\CertificateController;
use App\Http\Controllers\FlatToneController;
use App\Http\Controllers\ForeignLanguageController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\SignUpController;
use App\Http\Controllers\CourseTypeController;
use App\Http\Controllers\SttpBasicTrainingController;
use App\Http\Controllers\ClassesTrainingCourseController;
use App\Http\Controllers\QuotaAllocationController;
use App\Http\Controllers\TrainingAndCertificateResultsController;
use App\Http\Controllers\TrainingCourseController;
use App\Http\Controllers\StaffForeignLanguageScoresController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

//登入
Route::post('login', [AuthController::class, 'login']);

//登出
Route::group(['middleware' => ['jwt.verify'], 'prefix' => 'auth'], function () {
    Route::post('logout', [AuthController::class, 'logout']);
});

//部門
Route::group(['prefix' => 'department'], function () {
    Route::get('/', [DepartmentController::class, 'get']);
    Route::get('/{id}', [DepartmentController::class, 'get']);
    Route::post('/', [DepartmentController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:CREATE_DEPARTMENT']);
    Route::put('/{id}', [DepartmentController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:UPDATE_DEPARTMENT']);
    Route::delete('/{id}', [DepartmentController::class, 'delete'])->middleware(['jwt.verify', 'checkPermission:DELETE_DEPARTMENT']);
});

//單位
Route::group(['prefix' => 'unit'], function () {
    Route::get('/', [UnitController::class, 'get']);
    Route::get('/{id}', [UnitController::class, 'get']);
    Route::get('/department/{id}', [UnitController::class, 'getUnitByDepartmentId']);
    Route::post('/', [UnitController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:CREATE_UNIT']);
    Route::put('/{id}', [UnitController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:UPDATE_UNIT']);
    Route::delete('/{id}', [UnitController::class, 'delete'])->middleware(['jwt.verify', 'checkPermission:DELETE_UNIT']);
});

//職位
Route::group(['prefix' => 'position'], function () {
    Route::get('/', [PositionController::class, 'get']);
    Route::get('/{id}', [PositionController::class, 'get']);
    Route::post('/', [PositionController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:CREATE_POSITION']);
    Route::put('/{id}', [PositionController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:UPDATE_POSITION']);
    Route::delete('/{id}', [PositionController::class, 'delete'])->middleware(['jwt.verify', 'checkPermission:DELETE_POSITION']);
});

//sttp
Route::group(['prefix' => 'sttp'], function () {
    Route::get('/', [SttpController::class, 'get']);
    Route::get('/{id}', [SttpController::class, 'get']);
    Route::post('/', [SttpController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:CREATE_STTP']);
    Route::put('/{id}', [SttpController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:UPDATE_STTP']);
    Route::delete('/{id}', [SttpController::class, 'delete'])->middleware(['jwt.verify', 'checkPermission:DELETE_STTP']);
});

//sttp學經歷
Route::group(['prefix' => 'sle'], function () {
    Route::get('/', [SttpLearnAndExperienceController::class, 'get']);
    Route::get('/{id}', [SttpLearnAndExperienceController::class, 'get']);
    Route::post('/', [SttpLearnAndExperienceController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:CREATE_STPPLEARN_EXPERIENCE']);
    Route::put('/{id}', [SttpLearnAndExperienceController::class, 'set'])->middleware(['jwt.verify', 'checkPermission:UPDATE_STPPLEARN_EXPERIENCE']);
    Route::delete('/{id}', [SttpLearnAndExperienceController::class, 'delete'])->middleware(['jwt.verify', 'checkPermission:DELETE_STPPLEARN_EXPERIENCE']);
});

//使用者
Route::group(['prefix' => 'user'], function () {
    Route::get('/', [UserController::class, 'get']);
    Route::get('/{id}', [UserController::class, 'get']);
    Route::post('/', [UserController::class, 'set'])->middleware(['jwt.verify']);
    Route::put('/{id}', [UserController::class, 'set'])->middleware(['jwt.verify']);
    Route::put('/updatePassword/{id}', [UserController::class, 'updatePassword'])->middleware(['jwt.verify']);
    Route::delete('/{id}', [UserController::class, 'delete'])->middleware(['jwt.verify']);
});

//角色
Route::group(['prefix' => 'role'], function () {
    Route::get('/', [RoleController::class, 'get']);
    Route::get('/{id}', [RoleController::class, 'get']);
    Route::post('/', [RoleController::class, 'set'])->middleware(['jwt.verify']);
    Route::put('/{id}', [RoleController::class, 'set'])->middleware(['jwt.verify']);
    Route::delete('/{id}', [RoleController::class, 'delete'])->middleware(['jwt.verify']);
});

//主管等級
Route::group(['prefix' => 'supervisorLevel'], function () {
    Route::get('/', [SupervisorLevelController::class, 'get']);
    Route::get('/{id}', [SupervisorLevelController::class, 'get']);
    Route::post('/', [SupervisorLevelController::class, 'set']);
    Route::put('/{id}', [SupervisorLevelController::class, 'set']);
    Route::delete('/{id}', [SupervisorLevelController::class, 'delete']);
});

//職員
Route::group(['prefix' => 'staff'], function () {
    Route::get('/', [StaffController::class, 'get']);
    Route::get('/{id}', [StaffController::class, 'get']);
    Route::post('/', [StaffController::class, 'create']);
    Route::put('/{id}', [StaffController::class, 'update']);
    Route::delete('/{id}', [StaffController::class, 'delete']);
});

//課程
Route::group(['prefix' => 'course'], function () {
    Route::get('/', [CourseController::class, 'get']);
    Route::get('/{id}', [CourseController::class, 'get']);
    Route::get('/getCourseByType/{id}', [CourseController::class, 'getCourseByType']);
    Route::post('/', [CourseController::class, 'set']);
    Route::put('/{id}', [CourseController::class, 'set']);
    Route::delete('/{id}', [CourseController::class, 'delete']);
});

//主辦單位
Route::group(['prefix' => 'organizer'], function () {
    Route::get('/', [OrganizerController::class, 'get']);
    Route::get('/{id}', [OrganizerController::class, 'get']);
    Route::post('/', [OrganizerController::class, 'set']);
    Route::put('/{id}', [OrganizerController::class, 'set']);
    Route::delete('/{id}', [OrganizerController::class, 'delete']);
});

//訓練單位
Route::group(['prefix' => 'trainingUnit'], function () {
    Route::get('/', [TrainingUnitController::class, 'get']);
    Route::get('/{id}', [TrainingUnitController::class, 'get']);
    Route::post('/', [TrainingUnitController::class, 'set']);
    Route::put('/{id}', [TrainingUnitController::class, 'set']);
    Route::delete('/{id}', [TrainingUnitController::class, 'delete']);
});

//班次
Route::group(['prefix' => 'classSchedule'], function () {
    Route::get('/', [ClassScheduleController::class, 'get']);
    Route::get('/{id}', [ClassScheduleController::class, 'get']);
    Route::post('/', [ClassScheduleController::class, 'set']);
    Route::put('/{id}', [ClassScheduleController::class, 'set']);
    Route::delete('/{id}', [ClassScheduleController::class, 'delete']);
});

//課程班次多對多
Route::group(['prefix' => 'courseClassSchedule'], function () {
    Route::get('/getCourseByClassScheduleId/{id}', [CourseClassScheduleController::class, 'getCourseByClassScheduleId']);
    Route::get('/getClassScheduleByCourseId/{id}', [CourseClassScheduleController::class, 'getClassScheduleByCourseId']);
    Route::post('/createClassScheduleAndCourse', [CourseClassScheduleController::class, 'createClassScheduleAndCourse']);
    Route::delete('/deleteClassScheduleAndCourse', [CourseClassScheduleController::class, 'deleteClassScheduleAndCourse']);
});

//證照對照表
Route::group(['prefix' => 'certificate'], function () {
    Route::get('/', [CertificateController::class, 'get']);
    Route::get('/{id}', [CertificateController::class, 'get']);
    Route::post('/', [CertificateController::class, 'set']);
    Route::put('/{id}', [CertificateController::class, 'set']);
    Route::delete('/{id}', [CertificateController::class, 'delete']);
});

//平調對照表
Route::group(['prefix' => 'flatTone'], function () {
    Route::get('/getFlatToneBySttpId/{id}', [FlatToneController::class, 'getFlatToneBySttpId']);
    Route::post('/createFlatTone', [FlatToneController::class, 'createFlatTone']);
    Route::delete('/deleteFlatTone/{id?}', [FlatToneController::class, 'deleteFlatTone']);
});

//升遷對照表
Route::group(['prefix' => 'promotion'], function () {
    Route::get('/getPromotionBySttpId/{id}', [PromotionController::class, 'getPromotionBySttpId']);
    Route::post('/createPromotion', [PromotionController::class, 'createPromotion']);
    Route::delete('/deletePromotion/{id?}', [PromotionController::class, 'deletePromotion']);
});

//報名對照表
Route::group(['prefix' => 'promotion'], function () {
    Route::get('/', [SignUpController::class, 'get']);
    Route::get('/{id}', [SignUpController::class, 'get']);
    Route::get('/getSignUpByClassScheduleId/{id}', [SignUpController::class, 'getSignUpByClassScheduleId']);
    Route::get('/getSignUpByStaffId/{id}', [SignUpController::class, 'create']);
    Route::post('/', [SignUpController::class, 'create']);
    Route::delete('/{id?}', [SignUpController::class, 'delete']);
});

//課程類別
Route::group(['prefix' => 'courseType'], function () {
    Route::get('/', [CourseTypeController::class, 'get']);
    Route::get('/{id}', [CourseTypeController::class, 'get']);
    Route::post('/', [CourseTypeController::class, 'set']);
    Route::put('/{id}', [CourseTypeController::class, 'set']);
    Route::delete('/{id}', [CourseTypeController::class, 'delete']);
});

//外語對照表
Route::group(['prefix' => 'foreignLanguage'], function () {
    Route::get('/', [ForeignLanguageController::class, 'get']);
    Route::get('/{id}', [ForeignLanguageController::class, 'get']);
    Route::post('/', [ForeignLanguageController::class, 'set']);
    Route::put('/{id}', [ForeignLanguageController::class, 'set']);
    Route::delete('/{id}', [ForeignLanguageController::class, 'delete']);
});

//Sttp基本訓練對照表
Route::group(['prefix' => 'sttpBasicTraining'], function () {
    Route::get('/', [SttpBasicTrainingController::class, 'get']);
    Route::get('/{id}', [SttpBasicTrainingController::class, 'get']);
    Route::get('/getBySttpId/{id}', [SttpBasicTrainingController::class, 'getBySttpId']);
    Route::get('/getByClassesId/{id}', [SttpBasicTrainingController::class, 'getByClassesId']);
    Route::post('/', [SttpBasicTrainingController::class, 'set']);
    Route::put('/{id}', [SttpBasicTrainingController::class, 'set']);
    Route::delete('/{id}', [SttpBasicTrainingController::class, 'delete']);
    Route::delete('/deleteBySttpId/{id}', [SttpBasicTrainingController::class, 'deleteBySttpId']);
    Route::delete('/deleteByClassesId/{id}', [SttpBasicTrainingController::class, 'deleteByClassesId']);
});

//班別訓練課程對照表
Route::group(['prefix' => 'classesTrainingCourse'], function () {
    Route::get('/', [ClassesTrainingCourseController::class, 'get']);
    Route::get('/{id}', [ClassesTrainingCourseController::class, 'get']);
    Route::get('/getByClassesId/{id}', [ClassesTrainingCourseController::class, 'getByClassesId']);
    Route::get('/getByTrainingCourseId/{id}', [ClassesTrainingCourseController::class, 'getByTrainingCourseId']);
    Route::post('/', [ClassesTrainingCourseController::class, 'set']);
    Route::put('/{id}', [ClassesTrainingCourseController::class, 'set']);
    Route::delete('/{id}', [ClassesTrainingCourseController::class, 'delete']);
    Route::delete('/deleteByClassesId/{id}', [ClassesTrainingCourseController::class, 'deleteByClassesId']);
    Route::delete('/deleteByTrainingCourseId/{id}', [ClassesTrainingCourseController::class, 'deleteByTrainingCourseId']);
});

//名額分配對照表
Route::group(['prefix' => 'quotaAllocation'], function () {
    Route::get('/', [QuotaAllocationController::class, 'get']);
    Route::get('/{id}', [QuotaAllocationController::class, 'get']);
    Route::get('/getByUnitId/{id}', [QuotaAllocationController::class, 'getByUnitId']);
    Route::get('/getByClassScheduleId/{id}', [QuotaAllocationController::class, 'getByClassScheduleId']);
    Route::post('/', [QuotaAllocationController::class, 'set']);
    Route::put('/{id}', [QuotaAllocationController::class, 'set']);
    Route::delete('/{id}', [QuotaAllocationController::class, 'delete']);
    Route::delete('/deleteByUnitId/{id}', [QuotaAllocationController::class, 'deleteByUnitId']);
    Route::delete('/deleteByClassScheduleId/{id}', [QuotaAllocationController::class, 'deleteByClassScheduleId']);
});

// 訓練and證照成績
Route::group(['prefix' => 'trainingAndCertificateResults'], function () {
    Route::get('/', [TrainingAndCertificateResultsController::class, 'get']);
    Route::get('/{id}', [TrainingAndCertificateResultsController::class, 'get']);
    Route::get('/getTrainingAndCertificateResultsByStaffId/{id}', [TrainingAndCertificateResultsController::class, 'getTrainingAndCertificateResultsByStaffId']);
    Route::get('/getTrainingAndCertificateResultsByClassesId/{id}', [TrainingAndCertificateResultsController::class, 'getTrainingAndCertificateResultsByClassesId']);
    Route::post('/', [TrainingAndCertificateResultsController::class, 'set']);
    Route::put('/{id}', [TrainingAndCertificateResultsController::class, 'set']);
    Route::delete('/{id}', [TrainingAndCertificateResultsController::class, 'delete']);
});

// 訓練課程對照表
Route::group(['prefix' => 'trainingCourse'], function () {
    Route::get('/', [TrainingCourseController::class, 'get']);
    Route::get('/{id}', [TrainingCourseController::class, 'get']);
    Route::post('/', [TrainingAndCertificateResultsController::class, 'set']);
    Route::put('/{id}', [TrainingAndCertificateResultsController::class, 'set']);
    Route::delete('/{id}', [TrainingAndCertificateResultsController::class, 'delete']);
});


// 員工外語成績對照表
Route::group(['prefix' => 'staffForeignLanguageScore'], function () {
    Route::get('/', [StaffForeignLanguageScoresController::class, 'get']);
    Route::get('/{id}', [StaffForeignLanguageScoresController::class, 'get']);
    Route::get('/getByStaffId/{id}', [StaffForeignLanguageScoresController::class, 'getByStaffId']);
    Route::get('/getByForeignLanguageId/{id}', [StaffForeignLanguageScoresController::class, 'getByForeignLanguageId']);
    Route::post('/', [StaffForeignLanguageScoresController::class, 'set']);
    Route::put('/{id}', [StaffForeignLanguageScoresController::class, 'set']);
    Route::delete('/{id}', [StaffForeignLanguageScoresController::class, 'delete']);
    Route::delete('/deleteByStaffId/{id}', [StaffForeignLanguageScoresController::class, 'deleteByStaffId']);
    Route::delete('/deleteByForeignLanguageId/{id}', [StaffForeignLanguageScoresController::class, 'deleteByForeignLanguageId']);
});
