FROM php:8.1-fpm

RUN apt-get update && \
    apt-get install -y \
        zip \
        unzip \
        git \
        && \
    rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql

WORKDIR /var/www/html

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . .

RUN composer install

CMD ["php-fpm"]
